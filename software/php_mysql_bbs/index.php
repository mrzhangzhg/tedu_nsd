<?php
$con = mysql_connect("localhost","root","NSD2021@tedu.cn");
if (!$con) {
    die('Could not connect: ' . mysql_error());
}
if ($_SERVER['REQUEST_METHOD']=="POST") {
    mysql_select_db("mybbs");
    $title=$_POST["title"];
    $content=$_POST["content"];
    $sql="INSERT INTO posts (title, pub_date, content) VALUES('{$title}', NOW(), '{$content}')";
    mysql_query("SET NAMES 'utf8mb4'");
    mysql_query($sql);
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>NSD2021留言板</title>
    <link rel="stylesheet" href="static/css/bootstrap.min.css">
</head>
<body>
<div class="container">
    <div class="row">
        <div id="linux-carousel" class="carousel slide">
            <ol class="carousel-indicators">
                <li class="active" data-target="#linux-carousel" data-slide-to="0"></li>
                <li data-target="#linux-carousel" data-slide-to="1"></li>
                <li data-target="#linux-carousel" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="item active">
                    <img src="static/imgs/first.jpg" alt="Linux云计算学院">
                </div>
                <div class="item">
                    <img src="static/imgs/second.jpg" alt="云计算课程">
                </div>
                <div class="item">
                    <img src="static/imgs/third.jpg" alt="红帽认证">
                </div>
            </div>
            <a href="#linux-carousel" data-slide="prev" class="carousel-control left">
                <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
            <a href="#linux-carousel" data-slide="next" class="carousel-control right">
                <span class="glyphicon glyphicon-chevron-right"></span>
            </a>
        </div>
    </div>
    <?php
        $sql2="SELECT title, pub_date, content from mybbs.posts";
        mysql_query("SET NAMES 'utf8mb4'");
        $result=mysql_query($sql2) or die("Error in query: $sql2. ".mysql_error());
        while($row=mysql_fetch_assoc($result)){
            echo "<div class=\"row\">";
                echo "<h3>".$row["title"]."</h3>";
                echo "<div class=\"text-right\"><em>".$row["pub_date"]."</em></div>";
                echo "<div><pre style=\"white-space: pre-wrap;word-wrap: break-word;\">".$row["content"]."</pre></div>";
            echo "</div>";
        }
    ?>
    <div class="row" style="padding: 10px 0">
        <hr>
        <form action="/index.php" method="post">
            <div class="form-group">
                <label>标题：</label>
                <input class="form-control" type="text" name="title">
            </div>
            <div class="form-group">
                <textarea class="form-control" name="content" rows="10"></textarea>
            </div>
            <div class="form-group text-center">
                <input class="btn btn-primary" type="submit" value="提 交">
                <input class="btn btn-primary" type="reset" value="重 置">
            </div>
        </form>
    </div>
    <div class="row text-center">
        <p>张志刚制作 <a href="http://linux.tedu.cn">达内云计算学院</a></p>
        <p>客服电话：400-111-8989 邮箱：cloud_computing@tedu.cn</p>
    </div>
</div>

<script src="static/js/jquery.min.js"></script>
<script src="static/js/bootstrap.min.js"></script>
<script type="text/javascript">
    $('#linux-carousel').carousel({
        interval : 3000
    });
</script>
</body>
</html>