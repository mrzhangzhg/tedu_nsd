# php_mysql_bbs

<p align="right">
  By <a href="https://www.jianshu.com/u/665c84e77f9c">Mr.张志刚</a>
</p>

[toc]

## 创建数据库

```mysql
# 授权root用户可以通过任何地址访问
mysql> grant all on *.* to 'root'@'%' identified by 'NSD2021@tedu.cn';
# 创建名为mybbs的数据库
mysql> create database mybbs default charset utf8mb4;
mysql> use mybbs ;
# 创建名为posts的表，有四个字段，用于存储留言
mysql> create table posts( id int primary key auto_increment, title varchar(50), pub_date datetime,  content text);
```

## 配置nginx服务器

```shell
# 安装编译器
[root@nginx1 ~]# yum install -y gcc pcre-devel zlib-devel

# 编译安装nginx
[root@nginx1 ~]# tar xf nginx-1.12.2.tar.gz 
[root@nginx1 ~]# cd nginx-1.12.2
[root@nginx1 nginx-1.12.2]# ./configure 
[root@nginx1 nginx-1.12.2]# make && make install

# 安装并启动php-fpm
[root@nginx1 ~]# yum install -y php-fpm php-mysql
[root@nginx1 ~]# systemctl start php-fpm
[root@nginx1 ~]# systemctl enable php-fpm

# 修改配置文件
[root@nginx1 ~]# vim +65 /usr/local/nginx/conf/nginx.conf
        location ~ \.php$ {
            root           html;
            fastcgi_pass   127.0.0.1:9000;
            fastcgi_index  index.php;
        #    fastcgi_param  SCRIPT_FILENAME  /scripts$fastcgi_script_name;
            include        fastcgi.conf;
        }

# 启动nginx服务
[root@nginx1 ~]# /usr/local/nginx/sbin/nginx -t  # 语法检查
nginx: the configuration file /usr/local/nginx/conf/nginx.conf syntax is ok
nginx: configuration file /usr/local/nginx/conf/nginx.conf test is successful

[root@nginx1 ~]# /usr/local/nginx/sbin/nginx
```

## 修改php首页

```shell
# 拷贝php_mysql_bbs目录下所有内容到nginx的文档目录
[root@nginx1 ~]# cp -r tedu_nsd/software/php_mysql_bbs/* /usr/local/nginx/html/

# 修改php页面，使其可以连接到数据库
[root@nginx1 ~]# cd /usr/local/nginx/html/
[root@nginx1 html]# vim index.php  # 只修改第2行
... ...
//以下函数的三个参数分别为：服务器地址、用户名、密码
$con = mysql_connect("localhost","root","NSD2021@tedu.cn");
... ...

# 使用浏览器访问http://web服务器地址/index.php
```

