## 安装方法
- 解压
```shell
[root@zzgrhel8 小小输入法]# tar xf yong.tar.gz 
```
- 安装
```shell
[root@zzgrhel8 小小输入法]# cd yong/
[root@zzgrhel8 yong]# ./yong-tool.sh --install64
```
- 重启生效
```shell
[root@zzgrhel8 yong]# reboot
```
