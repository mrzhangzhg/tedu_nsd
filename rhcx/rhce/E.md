# 题目

## 14、编写剧本为受管机批量创建用户，要求使用保险库中的密码

​    从以下 URL 下载用户列表，保存到/home/alice/ansible 目录下：
​    http://study.lab0.example.com/materials/name_list.yml
​    创建剧本 /home/alice/ansible/users.yml 的 playbook，满足下列要求:
​        1）使用之前题目中的 passdb.yml 保险库文件
​        2）职位描述为 dev 的用户应在 test01、test02 主机组的受管机上创建，从 pw_dev 变量分配密码，是补充组 devops 的成员
​        3）职位描述为 man 的用户应在 web 主机组的受管机上创建,从 pw_man 变量分配密码,是补充组 opsmgr 的成员
​        4）该 playbook 可以使用之前题目创建的 secret.txt 密码文件运行

# 答案

## 解题思路

本题用到的模块有 group、user、使用了过滤器 password_hash

```shell
[alice@control ~]$ cd ~alice/ansible/
[alice@control ansible]$ wget  http://study.lab0.example.com/materials/name_list.yml
[alice@control ansible]$ vim users.yml 
---
- name: create users in test01,test02
  hosts: test01,test02
  vars_files:
    - passdb.yml
    - name_list.yml
  tasks:
    - name: create devops group
      group:
        name: devops
    - name: create users in devops group
      user:
        name: "{{item.name}}"
        password: "{{pw_dev|password_hash('sha512')}}"
        groups: devops
      when: "item.job == 'dev'"
      loop: "{{users}}"
- name: create users in web
  hosts: web
  vars_files:
    - passdb.yml
    - name_list.yml
  tasks:
    - name: create opsmgr group
      group:
        name: opsmgr
    - name: create users in opsmgr group
      user:
        name: "{{item.name}}"
        password: "{{pw_man|password_hash('sha512')}}"
        groups: opsmgr
      when: "item.job == 'man'"
      loop: "{{users}}"

[alice@control ansible]$ ansible-playbook users.yml --vault-id=secret.txt 

验证结果：failed=0，使用新添加的用户登录即可

​```shell
[alice@control ansible]$ ssh -l tom node1
tom@node1's password: 
[tom@node1 ~]$ logout
Connection to node1 closed.
[alice@control ansible]$ ssh -l tom node2
tom@node2's password: 
[tom@node2 ~]$ logout
Connection to node2 closed.
[alice@control ansible]$ ssh -l jerry node3
jerry@node3's password: 
[jerry@node3 ~]$ logout
Connection to node3 closed.
[alice@control ansible]$ ssh -l jerry node4
jerry@node4's password: 
[jerry@node4 ~]$ logout
Connection to node4 closed.
[alice@control ansible]$ 
```

## [返回](0.md)

