# 题目

## 12、编写剧本为受管机生成硬件报告

创建名为/home/alice/ansible/hardware.yml 的 playbook，满足下列要求：
    1）使所有受管理节点从以下 URL 下载文件：
          http://study.lab0.example.com/materials/hardware.empty
    2）并用来生成以下硬件报告信息，存储在各自的/root/hardware.txt 文件中

```shell
清单主机名称
以 MB 表示的总内存大小
BIOS 版本
硬盘 vda 的大小
硬盘 vdb 的大小
```


其中，文件的每一行含有一个 key=value 对，如果项目不存在，则显示 NONE

# 答案

## 解题思路

远程下载文件使用 get_url ，修改文件使用 replace ，该题主要考察 facts 变量值的获取，可以使用循环简化文件

该题目用到的知识 loop 循环，get_url、replace 模块

```shell
[alice@control ~]$ cd ~alice/ansible/
[alice@control ansible]$ vim hardware.yml
---
- hosts: all
  tasks:
    - get_url:
        url: http://study.lab0.example.com/materials/hardware.empty
        dest: /root/hardware.txt
        force: yes
    - replace:
        path: /root/hardware.txt
        regexp: inventoryhostname
        replace: "{{inventory_hostname}}"
    - replace:
        path: /root/hardware.txt
        regexp: memory_in_MB
        replace: "{{ansible_memtotal_mb}}"
    - replace:
        path: /root/hardware.txt
        regexp: BIOS_version
        replace: "{{ansible_bios_version}}"
    - replace:
        path: /root/hardware.txt
        regexp: disk_vda_size
        replace: "{{ansible_devices.vda.size}}"
    - replace:
        path: /root/hardware.txt
        regexp: disk_vdb_size
        replace: "{{ansible_devices.vdb.size if ansible_devices.vdb.size is defined else 'NONE'}}"


[alice@control ansible]$ ansible-playbook  hardware.yml
```

验证题目：结果正常 failed=0，查看生成的文件

```shell
[alice@control ansible]$ ansible all -m shell -a 'cat /root/hardware.txt'
node3 | CHANGED | rc=0 >>
hostname=node3
mem=979
bios=1.11.1-4.module+el8.1.0+4066+0f1aadab
vdasize=50.00 GB
vdbsize=NONE

node1 | CHANGED | rc=0 >>
hostname=node1
mem=979
bios=1.11.1-4.module+el8.1.0+4066+0f1aadab
vdasize=50.00 GB
vdbsize=NONE

node4 | CHANGED | rc=0 >>
hostname=node4
mem=979
bios=1.11.1-4.module+el8.1.0+4066+0f1aadab
vdasize=50.00 GB
vdbsize=2.00 GB

node2 | CHANGED | rc=0 >>
hostname=node2
mem=979
bios=1.11.1-4.module+el8.1.0+4066+0f1aadab
vdasize=50.00 GB
vdbsize=1.00 GB

node5 | CHANGED | rc=0 >>
hostname=node5
mem=979
bios=1.11.1-4.module+el8.1.0+4066+0f1aadab
vdasize=50.00 GB
vdbsize=5.00 GB

[alice@control ansible]$
```

## [返回](0.md)
