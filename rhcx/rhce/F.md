# 题目

## 15、重设保险库密码

​    1）从以下 URL 下载保险库文件到/home/alice/ansible 目录：
​           http://study.lab0.example.com/materials/topsec.yml
​    2）当前的库密码是 banana，新密码是 big_banana，请更新该库密码

# 答案

## 解题思路

白送分的题，没啥可说的

```shell
[alice@control ~]$ cd ~alice/ansible/
[alice@control ansible]$ curl -sO http://study.lab0.example.com/materials/topsec.yml
[alice@control ansible]$ ansible-vault rekey topsec.yml 
Vault password:                    # 输入原密码
New Vault password:                # 输入新密码
Confirm New Vault password:        # 在次输入新密码确认
Rekey successful
[alice@control ansible]$ 
```

验证方法：用新密码查看文件

```shell
[alice@control ansible]$ ansible-vault view topsec.yml 
Vault password: 
dachui: banana
[alice@control ansible]$ 
```

## [返回](0.md)
