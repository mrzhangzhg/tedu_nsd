# 题目

## 10、编写剧本修改远程文件内容

创建剧本 /home/alice/ansible/newissue.yml，满足下列要求：
    1）在所有清单主机上运行，替换/etc/issue 的内容
    2）对于 test01 主机组中的主机，/etc/issue 文件内容为 test01
    3）对于 test02 主机组中的主机，/etc/issue 文件内容为 test02
    4）对于 web 主机组中的主机，/etc/issue 文件内容为 Webserver

# 答案

## 解题思路

在所有主机上运行，不同主机写入不同的数据，这里考察的是 判断 语法的使用

在主机上运行应该判断当前主机所在的组可以通过 主机名称  in groups.组名称判断

本题只用到了 copy 模块

```shell
[alice@control ~]$ cd ~alice/ansible/
[alice@control ansible]$ vim newissue.yml
---
- hosts: all
  tasks:
    - copy:
        content: test01
        dest: /etc/issue
      when: '"test01" in group_names'
    - copy:
        content: test02
        dest: /etc/issue
      when: '"test02" in group_names'
    - copy:
        content: Webserver
        dest: /etc/issue
      when: '"web" in group_names'

[root@control ansible]# ansible-playbook newissue.yml
```

验证题目： failed=0 没有报错，到目标结点查看 issue 的文件内容

```shell
[alice@control ansible]$ ansible all -m shell -a 'cat /etc/issue'
node2 | CHANGED | rc=0 >>
test02

node5 | CHANGED | rc=0 >>
\S
Kernel \r on an \m

node4 | CHANGED | rc=0 >>
Webserver

node3 | CHANGED | rc=0 >>
Webserver

node1 | CHANGED | rc=0 >>
test01

[alice@control ansible]$
```

## [返回](0.md)
