# 题目

## 03、编写剧本远程安装软件

创建名为/home/alice/ansible/tools.yml 的 playbook，能够实现以下目的：
    1）将 php 和 tftp 软件包安装到 test01、test02 和 web 主机组中的主机上
    2）将 RPM Development Tools 软件包组安装到 test01 主机组中的主机上
    3）将 test01 主机组中的主机上所有软件包升级到最新版本

# 答案

## 解题思路

题目要求在不同的分组中安装不同的软件包，需要书写多个 play

安装升级软件包需要使用 yum 模块（使用 ansible-doc yum 查看案例）

```shell
[alice@control ansible]$ vim tools.yml 
---
- name: 安装软件包
  hosts: test01,test02,web
  tasks:
    - name: 安装PHP和tftp
      yum:
        name: php,tftp
        state: present
    
- name: 安装软件包组并升级所有软件包
  hosts: test01
  tasks:
    - name: 安装软件包组 RPM Development Tools 
      yum:
        name: "@RPM Development tools"
        state: present
    - name: 升级所有软件包
      yum:
        name: '*'
        state: latest
[alice@control ansible]$ ansible-playbook tools.yml
```

验证结果： 所有执行结果中 failed=0 

## [返回](0.md)

