# 题目

## 13、创建保险库文件

​    1）创建 ansible 保险库 /home/alice/ansible/passdb.yml，其中有 2 个变量：
​          pw_dev，值为 ab1234
​          pw_man，值为 cd5678
​    2) 加密和解密该库的密码是 pwd@1234 ,密码存在/home/alice/ansible/secret.txt 中

# 答案

## 解题思路

ansilbe加密使用 ansible-vault 命令

```shell
[alice@control ~]$ cd ~alice/ansible/
[alice@control ansible]$ vim passdb.yml 
---
pw_dev: ab1234
pw_man: cd5678
[alice@control ansible]$ echo 'pwd@1234' >secret.txt 
[alice@control ansible]$ chmod 0600 secret.txt 
[alice@control ansible]$ ansible-vault encrypt --vault-id=secret.txt passdb.yml 
Encryption successful
[alice@control ansible]$
```

验证方法： 查看文件已经被加密了

```shell
[alice@control ansible]$ cat passdb.yml 
$ANSIBLE_VAULT;1.1;AES256
65393235666466326163623463353239666439326465613161306133303734323635386231613434
6662613533373937313733343034323635336432616633630a386663613639646133373664636531
35616463303532353663306163313238663663663533323534626561663733636330623534666233
3434323161656136310a313139666231656531636531343431373162383462616230333838643437
63326235653239323864303237353230353632396539306163316161396162323530323635623131
3633316362363262396530336631633366333166653030653830
[alice@control ansible]$ 
```

## [返回](0.md)
