# 题目

## 11、编写剧本部署远程 Web 目录

创建剧本 /home/alice/ansible/webdev.yml，满足下列要求：
    1）在 test01 主机组运行
    2）创建目录/webdev，属于 webdev 组，常规权限为 rwxrwxr-x，具有 SetGID 特殊权限
    3）使用符号链接/var/www/html/webdev 链接到/webdev 目录
    4）创建文件/webdev/index.html，内容是 It's works!
    5）查看 test01 主机组的 web 页面 http://node1/webdev/ 将显示 It's works!

# 答案

## 解题思路

创建文件、目录、链接使用 file 模块完成

该题目隐含信息较多，坑比较多，需要我们自己启动服务、设置防火墙、设置 selinux

用到的模块 group、file、copy、service、firewalld、shell

```shell
[alice@control ~]$ cd ~alice/ansible/
[alice@control ansible]$ vim /home/alice/ansible/webdev.yml
---
- name: 远程部署web目录
  hosts: test01
  tasks:
    - name: 添加webdev组
      group:
        name: webdev
        state: present
    - name: 创建目录
      file:
        path: /webdev
        state: directory
        owner: apache
        group: webdev
        mode: '2775'
    - name: 创建链接文件
      file:
        src: /webdev
        dest: /var/www/html/webdev
        state: link
    - name: 创建网页文件
      copy:
        content: |
          It's works!
        dest: /webdev/index.html
    - name: 启动httpd服务
      service:
        name: httpd
        state: started
        enabled: yes
    - name: 开启防火墙允许服务通过
      firewalld:
        service: http
        permanent: yes
        immediate: yes
        state: enabled
    - name: 设置selinux安全上下文
      shell:
        chcon -R -t httpd_sys_content_t /webdev
[alice@control ansible]$ ansible-playbook webdev.yml
```

验证题目：安装正常 failed=0，访问 node1 查看页面

```shell
[alice@control ansible]$ curl http://node1/webdev/
It's works!
[alice@control ansible]$
```

## [返回](0.md)
