```mysql

```

```mysql
数据库：nsd2021
departments: 全局表
employees: 枚举法
salary: 求模法
```

```shell
[root@mycat1 ~]# vim /usr/local/mycat/conf/server.xml 
... ... 
                <property name="password">123456</property>
                <property name="schemas">TESTDB,nsd2021</property>
... ...
[root@mycat1 ~]# vim /usr/local/mycat/conf/schema.xml 
... ...
        <schema name="nsd2021" checkSQLschema="false" sqlMaxLimit="100">
                <table name="departments" primaryKey="ID" type="global" dataNode="dn1,dn2,dn3" />
                <table name="employees" primaryKey="ID" dataNode="dn1,dn2,dn3"
                           rule="sharding-by-intfile" />
                <table name="salary" primaryKey="ID" autoIncrement="true" dataNode="dn1,dn2,dn3"
                           rule="mod-long" />
        </schema>
.... ...
[root@mycat1 ~]# mycat restart



[root@zzgrhel8 ~]# mysql -uroot -p123456 -h192.168.1.15 -P8066
MySQL [(none)]> use nsd2021;
MySQL [nsd2021]> create table departments(
    -> id int primary key auto_increment,
    -> dept_name varchar(10) unique);
MySQL [nsd2021]> insert into departments(dept_name) values
    -> ('人事部'),('财务部'),('运维部'),('开发部');
MySQL [nsd2021]> create table employees( sharding_id int, employee_id int primary key auto_increment, name varchar(10), birth_date date, hire_date date, email varchar(50), phone_number char(11), dept_id int);
MySQL [nsd2021]> create table salary( id int primary key auto_increment, date date, employee_id int, basic int, bonus int);
MySQL [nsd2021]> insert into employees(sharding_id, name, dept_id) values (10000, 'tom', 1),(10010, 'jerry', 2),(10020,'bob', 3);
MySQL [nsd2021]> insert into salary(id, date, employee_id, basic, bonus)
    -> values (1, '2020-01-10', 1, 10000, 2000);

```













