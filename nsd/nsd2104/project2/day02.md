# db_project_day02

## 升级web服务器

### 清除原有配置

```shell
[root@web33 ~]# /usr/local/tomcat/bin/shutdown.sh 
[root@web33 ~]# vim /etc/rc.d/rc.local 
# /usr/local/tomcat/bin/startup.sh   # 注释此行

[root@web33 ~]# umount /usr/local/tomcat/webapps/ROOT/
[root@web33 ~]# vim /etc/fstab 
# 192.168.1.30:/sitedir /usr/local/tomcat/webapps/ROOT/ nfs  defaults        0 0     # 注释此行
```

### 部署lnmp环境

```shell
[root@web33 ~]# yum install -y gcc pcre-devel zlib-devel 
[root@zzgrhel8 ~]# scp /linux-soft/4/redis/lnmp/nginx-1.12.2.tar.gz 192.168.1.33:/root
[root@web33 ~]# tar xf nginx-1.12.2.tar.gz 
[root@web33 ~]# cd nginx-1.12.2/
[root@web33 nginx-1.12.2]# ./configure 
[root@web33 nginx-1.12.2]# make && make install

[root@web33 ~]# yum install -y php-fpm
[root@web33 ~]# systemctl enable php-fpm
[root@web33 ~]# systemctl start php-fpm

[root@web33 ~]# vim +65 /usr/local/nginx/conf/nginx.conf 
        location ~ \.php$ {
            root           html;
            fastcgi_pass   127.0.0.1:9000;
            fastcgi_index  index.php;
        #    fastcgi_param  SCRIPT_FILENAME  /scripts$fastcgi_script_name;
            include        fastcgi.conf;
        }
[root@web33 ~]# /usr/local/nginx/sbin/nginx -t
[root@web33 ~]# /usr/local/nginx/sbin/nginx

[root@web33 ~]# yum install -y php-mysql
[root@web33 ~]# systemctl restart php-fpm

[root@web33 ~]# ss -tlnp | grep :80
LISTEN     0      128          *:80                       *:*                   users:(("nginx",pid=28661,fd=6),("nginx",pid=28660,fd=6))
[root@web33 ~]# kill 28660

[root@web33 ~]# vim /etc/fstab 
192.168.1.30:/sitedir   /usr/local/nginx/html   nfs     defaults        0 0
[root@web33 ~]# mount -a
[root@web33 ~]# /usr/local/nginx/sbin/nginx

```

## 配置redis

- 初始化redis服务器

```shell
[root@zzgrhel8 ~]# scp /linux-soft/4/redis/redis-4.0.8.tar.gz 192.168.1.51:/root

[root@redisa ~]# yum install -y gcc
[root@redisa ~]# tar xf redis-4.0.8.tar.gz 
[root@redisa ~]# cd redis-4.0.8/
[root@redisa redis-4.0.8]# vim +27 src/Makefile 
PREFIX?=/usr/local/redis
[root@redisa redis-4.0.8]# make && make install

[root@redisa ~]# ssh-keygen 
[root@redisa ~]# for i in {51..56}
> do
> ssh-copy-id 192.168.1.$i
> done
[root@redisa ~]# for i in {52..56}; do scp -r /usr/local/redis/ 192.168.1.$i:/usr/local/; done
[root@redisa ~]# for i in {51..56}; do ssh 192.168.1.$i "echo 'export PATH=$PATH:/usr/local/redis/bin' >> /etc/bashrc"; done
[root@redisa ~]# source /etc/bashrc 
[root@redisa ~]# for i in {52..56}; do scp -r ~/redis-4.0.8 192.168.1.$i:/root/; done

# 自动回答初始化的问题
[root@redisa ~]# for i in {51..56}; do ssh 192.168.1.$i yum install -y expect; done
[root@redisa ~]# vim answer.sh
#!/bin/bash

expect <<EOF
    spawn /root/redis-4.0.8/utils/install_server.sh
    expect "6379]"
    send "\r"
    expect "/etc/redis/6379.conf]"
    send "\r"
    expect "/var/log/redis_6379.log]"
    send "\r"
    expect "/var/lib/redis/6379]"
    send "\r"
    expect "/usr/local/redis/bin/redis-server]"
    send "\r"
    expect "Ctrl-C to abort."
    send "\r"
expect eof
EOF
[root@redisa ~]# chmod +x answer.sh 
[root@redisa ~]# for i in {52..56}; do scp answer.sh 192.168.1.$i:/root/; done
[root@redisa ~]# for i in {51..56}; do ssh 192.168.1.$i "bash /root/answer.sh"; done
[root@redisa ~]# for i in {51..56}; do ssh 192.168.1.$i service redis_6379 stop; done
[root@redisa ~]# for i in {51..56}; do ssh 192.168.1.$i rm -rf /var/lib/redis/6379/*; done
[root@redisa ~]# vim /etc/redis/6379.conf 
  89 protected-mode no
  70 # bind 127.0.0.1
 815 cluster-enabled yes
 823 cluster-config-file nodes-6379.conf
 829 cluster-node-timeout 5000
[root@redisa ~]# for i in {52..56}; do scp /etc/redis/6379.conf 192.168.1.$i:/etc/redis/; done
[root@redisa ~]# for i in {51..56}; do ssh 192.168.1.$i service redis_6379 start; done

```

- 搭建redis集群

```shell
[root@zzgrhel8 ~]# scp /linux-soft/4/redis/redis-3.2.1.gem 192.168.1.57:/root
[root@mgmt ~]# yum install -y rubygems
[root@mgmt ~]# gem install redis-3.2.1.gem 
[root@redisa ~]# scp redis-4.0.8/src/redis-trib.rb 192.168.1.57:/usr/local/bin/
[root@mgmt ~]# redis-trib.rb create --replicas 1 192.168.1.{51..56}:6379
[root@mgmt ~]# redis-trib.rb info 192.168.1.51:6379
```

## 配置web服务器支持redis

```shell
[root@web33 ~]# yum install -y php-devel automake autoconf
[root@zzgrhel8 ~]# scp /linux-soft/4/redis/redis-cluster-4.3.0.tgz 192.168.1.33:/root/
[root@web33 ~]# tar xf redis-cluster-4.3.0.tgz 
[root@web33 ~]# cd redis-4.3.0/
[root@web33 redis-4.3.0]# phpize
[root@web33 redis-4.3.0]# ./configure --with-php-config=/usr/bin/php-config 
[root@web33 redis-4.3.0]# make && make install
[root@web33 ~]# vim +730 /etc/php.ini 
extension_dir = "/usr/lib64/php/modules"
extension = "redis.so"
[root@web33 ~]# systemctl restart php-fpm
```

- 测试

```shell
[root@nfs30 ~]# vim /sitedir/set_data.php
<?php
$redis_list = ['192.168.1.51:6379','192.168.1.52:6379','192.168.1.53:6379','192.168.1.54:6379','192.168.1.55:6379','192.168.1.56:6379'];
$client = new RedisCluster(NUll,$redis_list);
$client->set("i","tarenaA ");
$client->set("j","tarenaB ");
$client->set("k","tarenaC ");
?>
[root@nfs30 ~]# vim /sitedir/get_data.php
<?php
$redis_list = ['192.168.1.51:6379','192.168.1.52:6379','192.168.1.53:6379','192.168.1.54:6379','192.168.1.55:6379','192.168.1.56:6379'];
$client = new RedisCluster(NUll,$redis_list);
echo $client->get("i");
echo $client->get("j");
echo $client->get("k");
?>
# 访问http://192.168.1.33/set_data.php和http://192.168.1.33/get_data.php
[root@redisa ~]# redis-cli -h 192.168.1.51 -c
192.168.1.51:6379> keys *
1) "j"
192.168.1.51:6379> get i
```

## PXC

- 主从同步

```shell
[root@pxcnode66 ~]# yum install -y mysql-community*
[root@pxcnode66 ~]# systemctl start mysqld
[root@pxcnode66 ~]# grep password /var/log/mysqld.log 
[root@pxcnode66 ~]# mysqladmin -uroot -p'=6hU>ykqKjic' password NSD2021@tedu.cn

[root@pxcnode66 ~]# vim /etc/my.cnf
[mysqld]
server-id = 66
[root@pxcnode66 ~]# systemctl restart mysqld

# 主服务器完全备份
[root@mysql11 ~]# yum -y install percona-xtrabackup-24-2.4.7-1.el7
[root@mysql11 ~]# innobackupex --user root --password NSD2021@tedu.cn --slave-info /root/allbak --no-timestamp
[root@mysql11 ~]# scp -r /root/allbak/ 192.168.1.66:/root

# 从服务器恢复
[root@pxcnode66 ~]# yum -y install percona-xtrabackup-24-2.4.13-1.el7
[root@pxcnode66 ~]# systemctl stop mysqld
[root@pxcnode66 ~]# rm -rf /var/lib/mysql/*
[root@pxcnode66 ~]# innobackupex --apply-log /root/allbak/
[root@pxcnode66 ~]# innobackupex --copy-back /root/allbak/
[root@pxcnode66 ~]# chown -R mysql:mysql /var/lib/mysql
[root@pxcnode66 ~]# systemctl start mysqld

# 配置从服务器
[root@pxcnode66 ~]# grep master11 /root/allbak/xtrabackup_info 
binlog_pos = filename 'master11.000001', position '3070'
[root@pxcnode66 ~]# mysql -uroot -pNSD2021@tedu.cn
mysql> change master to
    -> master_host="192.168.1.11",
    -> master_user="repluser",
    -> master_password="NSD2021@tedu.cn",
    -> master_log_file="master11.000001",
    -> master_log_pos=3070;
mysql> start slave;
mysql> show slave status\G
             Slave_IO_Running: Yes
            Slave_SQL_Running: Yes
```

- pxcnode66获取数据后，更换为pxc软件

```shell
[root@pxcnode66 ~]# systemctl stop mysqld
[root@pxcnode66 ~]# yum remove -y mysql-community*
[root@pxcnode66 ~]# yum -y install Percona-XtraDB-Cluster-*
[root@pxcnode66 ~]# vim /etc/percona-xtradb-cluster.conf.d/mysqld.cnf 
... ...
server-id=66
... ...

[root@pxcnode66 ~]# vim /etc/percona-xtradb-cluster.conf.d/wsrep.cnf 
  8 wsrep_cluster_address=gcomm://  # 集群地址未填，表示新建集群
 25 wsrep_node_address=192.168.1.66
 27 wsrep_cluster_name=pxc-cluster
 30 wsrep_node_name=pxcnode66
 39 wsrep_sst_auth="sstuser:NSD2021@tedu.cn"
[root@pxcnode66 ~]# systemctl enable mysql
[root@pxcnode66 ~]# systemctl start mysql

[root@pxcnode66 ~]# mysql -uroot -pNSD2021@tedu.cn
mysql> grant all on *.* to sstuser@"localhost" identified by 'NSD2021@tedu.cn';
mysql> show slave status\G
                  Master_Host: 192.168.1.11
                  Master_User: repluser
             Slave_IO_Running: Yes
            Slave_SQL_Running: Yes
```

- 配置另外两台pxcnode

```shell
[root@pxcnode10 ~]# yum -y install Percona-XtraDB-Cluster-*
[root@pxcnode10 ~]# vim /etc/percona-xtradb-cluster.conf.d/mysqld.cnf 
[mysqld]
server-id=10
[root@pxcnode10 ~]# vim /etc/percona-xtradb-cluster.conf.d/wsrep.cnf 
  8 wsrep_cluster_address=gcomm://192.168.1.66,192.168.1.10
 25 wsrep_node_address=192.168.1.10
 27 wsrep_cluster_name=pxc-cluster
 30 wsrep_node_name=pxcnode10
 39 wsrep_sst_auth="sstuser:NSD2021@tedu.cn"
[root@pxcnode10 ~]# systemctl enable mysql
[root@pxcnode10 ~]# systemctl start mysql


[root@pxcnode88 ~]# yum -y install Percona-XtraDB-Cluster-*
[root@pxcnode88 ~]# vim /etc/percona-xtradb-cluster.conf.d/mysqld.cnf 
[mysqld]
server-id=88
[root@pxcnode88 ~]# vim /etc/percona-xtradb-cluster.conf.d/wsrep.cnf 
  8 wsrep_cluster_address=gcomm://192.168.1.66,192.168.1.10,192.168.1.88
 25 wsrep_node_address=192.168.1.88
 27 wsrep_cluster_name=pxc-cluster
 30 wsrep_node_name=pxcnode88
 39 wsrep_sst_auth="sstuser:NSD2021@tedu.cn"
[root@pxcnode88 ~]# systemctl enable mysql
[root@pxcnode88 ~]# systemctl start mysql

# 将pxcnode{66,10}的配置文件加入所有节点
[root@pxcnode{10,66} ~]# vim /etc/percona-xtradb-cluster.conf.d/wsrep.cnf 
  8 wsrep_cluster_address=gcomm://192.168.1.66,192.168.1.10,192.168.1.88
```

## 为pxc集群配置负载均衡

```shell
[root@haprox99 ~]# yum install -y haproxy
[root@haprox99 ~]# vim /etc/haproxy/haproxy.cfg 
# 删除63行到结尾的内容，然后添加以下内容：
 63 listen status
 64     mode http
 65     bind *:80
 66     stats enable
 67     stats uri /admin
 68     stats auth admin:admin
 69     
 70 listen mysql_3306 *:3306
 71     mode tcp
 72     option tcpka
 73     balance roundrobin
 74     server mysql_01 192.168.1.66:3306 check
 75     server mysql_02 192.168.1.10:3306 check
 76     server mysql_03 192.168.1.88:3306 check
[root@haprox99 ~]# systemctl start haproxy.service
[root@haprox99 ~]# systemctl enable haproxy.service
# 访问http://192.168.1.99/admin，查看监控信息

[root@haprox98 ~]# yum install -y haproxy
[root@haprox99 ~]# scp /etc/haproxy/haproxy.cfg 192.168.1.98:/etc/haproxy/
[root@haprox98 ~]# systemctl start haproxy.service
[root@haprox98 ~]# systemctl enable haproxy.service
# 访问http://192.168.1.98/admin，查看监控信息
```

## 配置高可用

```shell
[root@haprox99 ~]# yum install -y keepalived
[root@haprox99 ~]# vim /etc/keepalived/keepalived.conf 
# 在14行下面添加
 15    vrrp_iptables
... ...
 20 vrrp_instance VI_1 {
 21     state MASTER
 22     interface eth0
 23     virtual_router_id 51
 24     priority 150
 25     advert_int 1
 26     authentication {
 27         auth_type PASS
 28         auth_pass 1111
 29     }
 30     virtual_ipaddress {
 31         192.168.1.100
 32     }
 33 }

[root@haprox98 ~]# yum install -y keepalived
[root@haprox98 ~]# vim /etc/keepalived/keepalived.conf 
# 在14行下面添加
 15    vrrp_iptables
... ...
 20 vrrp_instance VI_1 {
 21     state BACKUP
 22     interface eth0
 23     virtual_router_id 51
 24     priority 100
 25     advert_int 1
 26     authentication {
 27         auth_type PASS
 28         auth_pass 1111
 29     }
 30     virtual_ipaddress {
 31         192.168.1.100
 32     }
 33 }
[root@haprox99 ~]# systemctl enable keepalived.service 
[root@haprox99 ~]# systemctl start keepalived.service 
[root@haprox98 ~]# systemctl start keepalived.service 
[root@haprox98 ~]# systemctl enable keepalived.service 
[root@haprox99 ~]# ip a s eth0   # 查看vip地址
[root@haprox99 ~]# shutdown 
[root@haprox98 ~]# ip a s eth0   # 过一段时间可以看到vip切换过来
```



