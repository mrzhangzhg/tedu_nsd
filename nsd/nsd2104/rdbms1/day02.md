# RDBMS1 day02

<p align="right">
  By <a href="https://www.jianshu.com/u/665c84e77f9c">Mr.张志刚</a>
</p>

[toc]

## 常用函数

### 分类

- 按使用方式分为：
  - 单行函数
  - 分组函数

- 按用途分为：
  - 字符函数
  - 数学函数
  - 日期函数
  - 流程控制函数
  
- 用法：

```mysql
SELECT 函数(参数) FROM 表;
```

### 函数应用

#### 字符函数实例：

- LENGTH(str)：返字符串长度，以字节为单位

```mysql
mysql> select length('abc');
+---------------+
| length('abc') |
+---------------+
|             3 |
+---------------+
1 row in set (0.00 sec)


mysql> select length('你好');
+------------------+
| length('你好')   |
+------------------+
|                6 |
+------------------+
1 row in set (0.00 sec)


mysql> select name, email, length(email) from employees where name='李平';
+--------+----------------+---------------+
| name   | email          | length(email) |
+--------+----------------+---------------+
| 李平   | liping@tedu.cn |            14 |
+--------+----------------+---------------+
1 row in set (0.00 sec)
```
- CHAR_LENGTH(str): 返回字符串长度，以字符为单位
```mysql
mysql> select char_length('abc');
+--------------------+
| char_length('abc') |
+--------------------+
|                  3 |
+--------------------+
1 row in set (0.00 sec)


mysql> select char_length('你好');
+-----------------------+
| char_length('你好')   |
+-----------------------+
|                     2 |
+-----------------------+
1 row in set (0.00 sec)
```
- CONCAT(s1,s2，...): 返回连接参数产生的字符串，一个或多个待拼接的内容，任意一个为NULL则返回值为NULL
```mysql
# 拼接字符串
mysql> select concat(dept_id, '-', dept_name) from departments;
+---------------------------------+
| concat(dept_id, '-', dept_name) |
+---------------------------------+
| 1-人事部                        |
| 2-财务部                        |
| 3-运维部                        |
| 4-开发部                        |
| 5-测试部                        |
| 6-市场部                        |
| 7-销售部                        |
| 8-法务部                        |
+---------------------------------+
8 rows in set (0.00 sec)
```
- UPPER(str)和UCASE(str): 将字符串中的字母全部转换成大写
```mysql
mysql> select name, upper(email) from employees where name like '李%';
+-----------+----------------------+
| name      | upper(email)         |
+-----------+----------------------+
| 李玉英    | LIYUYING@TEDU.CN     |
| 李平      | LIPING@TEDU.CN       |
| 李建华    | LIJIANHUA@TARENA.COM |
| 李莹      | LIYING@TEDU.CN       |
| 李柳      | LILIU@TARENA.COM     |
| 李慧      | LIHUI@TARENA.COM     |
| 李静      | LIJING@TARENA.COM    |
| 李瑞      | LIRUI@TARENA.COM     |
+-----------+----------------------+
8 rows in set (0.00 sec)
```
- LOWER(str)和LCASE(str):将str中的字母全部转换成小写
```mysql
# 转小写
mysql> select lower('HelloWorld');
+---------------------+
| lower('HelloWorld') |
+---------------------+
| helloworld          |
+---------------------+
1 row in set (0.00 sec)
```
- SUBSTR(s, start, length): 从子符串s的start位置开始，取出length长度的子串，位置从1开始计算
```mysql
mysql> select substr('hello world', 7);
+--------------------------+
| substr('hello world', 7) |
+--------------------------+
| world                    |
+--------------------------+
1 row in set (0.00 sec)


# 取子串，下标从7开始取出3个
mysql> select substr('hello world', 7, 3);
+-----------------------------+
| substr('hello world', 7, 3) |
+-----------------------------+
| wor                         |
+-----------------------------+
1 row in set (0.00 sec)
```
- INSTR(str,str1)：返回str1参数，在str参数内的位置
```mysql
# 子串在字符串中的位置
mysql> select instr('hello world', 'or');
+----------------------------+
| instr('hello world', 'or') |
+----------------------------+
|                          8 |
+----------------------------+
1 row in set (0.00 sec)


mysql> select instr('hello world', 'ol');
+----------------------------+
| instr('hello world', 'ol') |
+----------------------------+
|                          0 |
+----------------------------+
1 row in set (0.00 sec)
```

- TRIM(s): 返回字符串s删除了两边空格之后的字符串

```mysql
mysql> select trim('  hello world.  ');
+--------------------------+
| trim('  hello world.  ') |
+--------------------------+
| hello world.             |
+--------------------------+
1 row in set (0.00 sec)
```

#### 数学函数实例

- ABS(x)：返回x的绝对值

```mysql
mysql> select abs(-10);
+----------+
| abs(-10) |
+----------+
|       10 |
+----------+
1 row in set (0.00 sec)
```

- PI(): 返回圆周率π，默认显示6位小数

```mysql
mysql> select pi();
+----------+
| pi()     |
+----------+
| 3.141593 |
+----------+
1 row in set (0.00 sec)
```

- MOD(x,y): 返回x被y除后的余数

```mysql
mysql> select mod(10, 3);
+------------+
| mod(10, 3) |
+------------+
|          1 |
+------------+
1 row in set (0.00 sec)
```

- CEIL(x)、CEILING(x): 返回不小于x的最小整数

```mysql
mysql> select ceil(10.1);
+------------+
| ceil(10.1) |
+------------+
|         11 |
+------------+
1 row in set (0.00 sec)
```

- FLOOR(x): 返回不大于x的最大整数

```mysql
mysql> select floor(10.9);
+-------------+
| floor(10.9) |
+-------------+
|          10 |
+-------------+
1 row in set (0.00 sec)
```

- ROUND(x)、ROUND(x,y): 前者返回最接近于x的整数，即对x进行四舍五入；后者返回最接近x的数，其值保留到小数点后面y位，若y为负值，则将保留到x到小数点左边y位

```mysql
mysql> select round(10.6666);
+----------------+
| round(10.6666) |
+----------------+
|             11 |
+----------------+
1 row in set (0.00 sec)


mysql> select round(10.6666, 2);
+-------------------+
| round(10.6666, 2) |
+-------------------+
|             10.67 |
+-------------------+
1 row in set (0.00 sec)
```

#### 日期和时间函数实例

- CURDATE()、CURRENT_DATE(): 将当前日期按照"YYYY-MM-DD"或者"YYYYMMDD"格式的值返回，具体格式根据函数用在字符串或是数字语境中而定

```mysql
mysql> select curdate();
+------------+
| curdate()  |
+------------+
| 2021-03-09 |
+------------+
1 row in set (0.00 sec)


mysql> select curdate() + 0;
+---------------+
| curdate() + 0 |
+---------------+
|      20210309 |
+---------------+
1 row in set (0.00 sec)
```
- NOW(): 返回当前日期和时间值，格式为"YYYY_MM-DD HH:MM:SS"或"YYYYMMDDHHMMSS"，具体格式根据函数用在字符串或数字语境中而定

```mysql
mysql> select now();
+---------------------+
| now()               |
+---------------------+
| 2021-03-09 02:28:26 |
+---------------------+
1 row in set (0.00 sec)


mysql> select now() + 0;
+----------------+
| now() + 0      |
+----------------+
| 20210309022848 |
+----------------+
1 row in set (0.00 sec)
```
- UNIX_TIMESTAMP()、UNIX_TIMESTAMP(date): 前者返回一个格林尼治标准时间1970-01-01 00:00:00到现在的秒数，后者返回一个格林尼治标准时间1970-01-01 00:00:00到指定时间的秒数
```mysql
mysql> select unix_timestamp();
+------------------+
| unix_timestamp() |
+------------------+
|       1615275274 |
+------------------+
1 row in set (0.00 sec)
```
- FROM_UNIXTIME(date): 和UNIX_TIMESTAMP互为反函数，把UNIX时间戳转换为普通格式的时间
```mysql
mysql> select from_unixtime(0);
+---------------------+
| from_unixtime(0)    |
+---------------------+
| 1969-12-31 19:00:00 |
+---------------------+
1 row in set (0.00 sec)
```
- MONTH(date)和MONTHNAME(date):前者返回指定日期中的月份，后者返回指定日期中的月份的名称
```mysql
mysql> select month('20211001120000');
+-------------------------+
| month('20211001120000') |
+-------------------------+
|                      10 |
+-------------------------+
1 row in set (0.00 sec)


mysql> select monthname('20211001120000');
+-----------------------------+
| monthname('20211001120000') |
+-----------------------------+
| October                     |
+-----------------------------+
1 row in set (0.00 sec)
```
- DAYNAME(d)、DAYOFWEEK(d)、WEEKDAY(d): DAYNAME(d)返回d对应的工作日的英文名称，如Sunday、Monday等；DAYOFWEEK(d)返回的对应一周中的索引，1表示周日、2表示周一；WEEKDAY(d)表示d对应的工作日索引，0表示周一，1表示周二
```mysql
mysql> select dayname('20211001120000');
+---------------------------+
| dayname('20211001120000') |
+---------------------------+
| Friday                    |
+---------------------------+
1 row in set (0.00 sec)


mysql> select dayname('20211001');
+---------------------+
| dayname('20211001') |
+---------------------+
| Friday              |
+---------------------+
1 row in set (0.00 sec)
```
- WEEK(d): 计算日期d是一年中的第几周
```mysql
mysql> select week('20211001');
+------------------+
| week('20211001') |
+------------------+
|               39 |
+------------------+
1 row in set (0.00 sec)
```
- DAYOFYEAR(d)、DAYOFMONTH(d)： 前者返回d是一年中的第几天，后者返回d是一月中的第几天
```mysql
mysql> select dayofyear('20211001');
+-----------------------+
| dayofyear('20211001') |
+-----------------------+
|                   274 |
+-----------------------+
1 row in set (0.00 sec)
```

- YEAR(date)、QUARTER(date)、MINUTE(time)、SECOND(time): YEAR(date)返回指定日期对应的年份，范围是1970到2069；QUARTER(date)返回date对应一年中的季度，范围是1到4；MINUTE(time)返回time对应的分钟数，范围是0~59；SECOND(time)返回制定时间的秒值
```mysql
mysql> select year('20211001');
+------------------+
| year('20211001') |
+------------------+
|             2021 |
+------------------+
1 row in set (0.00 sec)


mysql> select quarter('20211001');
+---------------------+
| quarter('20211001') |
+---------------------+
|                   4 |
+---------------------+
1 row in set (0.00 sec)
```
#### 流程控制函数实例

- IF(expr,v1,v2): 如果expr是TRUE则返回v1，否则返回v2

```mysql
mysql> select if(3>0, 'yes', 'no');
+----------------------+
| if(3>0, 'yes', 'no') |
+----------------------+
| yes                  |
+----------------------+
1 row in set (0.00 sec)



mysql> select name, dept_id, if(dept_id=1, '人事部', '非人事部')  from employees where name='张亮';
+--------+---------+--------------------------------------------+
| name   | dept_id | if(dept_id=1, '人事部', '非人事部')        |
+--------+---------+--------------------------------------------+
| 张亮   |       7 | 非人事部                                   |
+--------+---------+--------------------------------------------+
1 row in set (0.00 sec)
```
- IFNULL(v1,v2): 如果v1不为NULL，则返回v1，否则返回v2
```mysql
mysql> select dept_id, dept_name, ifnull(dept_name, '未设置') from departments;
+---------+-----------+--------------------------------+
| dept_id | dept_name | ifnull(dept_name, '未设置')    |
+---------+-----------+--------------------------------+
|       1 | 人事部    | 人事部                         |
|       2 | 财务部    | 财务部                         |
|       3 | 运维部    | 运维部                         |
|       4 | 开发部    | 开发部                         |
|       5 | 测试部    | 测试部                         |
|       6 | 市场部    | 市场部                         |
|       7 | 销售部    | 销售部                         |
|       8 | 法务部    | 法务部                         |
+---------+-----------+--------------------------------+
8 rows in set (0.00 sec)


mysql> insert into departments(dept_id) values(9);
mysql> select dept_id, dept_name, ifnull(dept_name, '未设置') from departments; 
+---------+-----------+--------------------------------+
| dept_id | dept_name | ifnull(dept_name, '未设置')    |
+---------+-----------+--------------------------------+
|       1 | 人事部    | 人事部                         |
|       2 | 财务部    | 财务部                         |
|       3 | 运维部    | 运维部                         |
|       4 | 开发部    | 开发部                         |
|       5 | 测试部    | 测试部                         |
|       6 | 市场部    | 市场部                         |
|       7 | 销售部    | 销售部                         |
|       8 | 法务部    | 法务部                         |
|       9 | NULL      | 未设置                         |
+---------+-----------+--------------------------------+
9 rows in set (0.00 sec)
```
- CASE expr WHEN v1 THEN r1 [WHEN v2 THEN v2] [ELSE rn] END: 如果expr等于某个vn，则返回对应位置THEN后面的结果，如果与所有值都不想等，则返回ELSE后面的rn
```mysql
mysql> select dept_id, dept_name,
    -> case dept_name
    -> when '运维部' then '技术部门'
    -> when '开发部' then '技术部门'
    -> when '测试部' then '技术部门'
    -> when null then '未设置'
    -> else '非技术部门'
    -> end as '部门类型'
    -> from departments;
+---------+-----------+-----------------+
| dept_id | dept_name | 部门类型        |
+---------+-----------+-----------------+
|       1 | 人事部    | 非技术部门      |
|       2 | 财务部    | 非技术部门      |
|       3 | 运维部    | 技术部门        |
|       4 | 开发部    | 技术部门        |
|       5 | 测试部    | 技术部门        |
|       6 | 市场部    | 非技术部门      |
|       7 | 销售部    | 非技术部门      |
|       8 | 法务部    | 非技术部门      |
|       9 | NULL      | 非技术部门      |
+---------+-----------+-----------------+
9 rows in set (0.00 sec)


mysql> select dept_id, dept_name,
    -> case 
    -> when dept_name='运维部' then '技术部门'
    -> when dept_name='开发部' then '技术部门'
    -> when dept_name='测试部' then '技术部门'
    -> when dept_name is null then '未设置'
    -> else '非技术部门'
    -> end as '部门类型'
    -> from departments;
+---------+-----------+-----------------+
| dept_id | dept_name | 部门类型        |
+---------+-----------+-----------------+
|       1 | 人事部    | 非技术部门      |
|       2 | 财务部    | 非技术部门      |
|       3 | 运维部    | 技术部门        |
|       4 | 开发部    | 技术部门        |
|       5 | 测试部    | 技术部门        |
|       6 | 市场部    | 非技术部门      |
|       7 | 销售部    | 非技术部门      |
|       8 | 法务部    | 非技术部门      |
|       9 | NULL      | 未设置          |
+---------+-----------+-----------------+
9 rows in set (0.00 sec)
```
#### 分组函数

用于统计，又称为聚合函数或统计函数

- sum() ：求和
```mysql
mysql> select employee_id, sum(basic+bonus) from salary where employee_id=10 and year(date)=2018;
+-------------+------------------+
| employee_id | sum(basic+bonus) |
+-------------+------------------+
|          10 |           116389 |
+-------------+------------------+
1 row in set (0.00 sec)
```
- avg() ：求平均值
```mysql
mysql> select employee_id, avg(basic+bonus) from salary where employee_id=10 and year(date)=2018;
+-------------+------------------+
| employee_id | avg(basic+bonus) |
+-------------+------------------+
|          10 |       29097.2500 |
+-------------+------------------+
1 row in set (0.00 sec)
```
- max() ：求最大值
```mysql
mysql> select employee_id, max(basic+bonus) from salary where employee_id=10 and year(date)=2018;
+-------------+------------------+
| employee_id | max(basic+bonus) |
+-------------+------------------+
|          10 |            31837 |
+-------------+------------------+
1 row in set (0.00 sec)
```
- min() ：求最小值
```mysql
mysql> select employee_id, min(basic+bonus) from salary where employee_id=10 and year(date)=2018;
+-------------+------------------+
| employee_id | min(basic+bonus) |
+-------------+------------------+
|          10 |            24837 |
+-------------+------------------+
1 row in set (0.00 sec)
```
- count() ：计算个数
```mysql
mysql> select count(*) from departments;
+----------+
| count(*) |
+----------+
|        9 |
+----------+
1 row in set (0.00 sec)
```
## 分组查询

- 在对数据表中数据进行统计时，可能需要按照一定的类别分别进行统计。比如查询每个部门的员工数。

- 使用GROUP BY按某个字段，或者多个字段中的值，进行分组，字段中值相同的为一组

#### 语法格式

- 查询列表必须是分组函数和出现在GROUP BY后面的字段
- 通常而言，分组前的数据筛选放在where子句中，分组后的数据筛选放在having子句中

```mysql
SELECT 字段名1(要求出现在group by后面)，分组函数(),……
FROM 表名
WHERE 条件
GROUP BY 字段名1，字段名2
HAVING 过滤条件
ORDER BY 字段;
```

#### 应用实例

- 查询每个部门的人数
```mysql
mysql> select dept_id, count(*) from employees group by dept_id;
+---------+----------+
| dept_id | count(*) |
+---------+----------+
|       1 |        8 |
|       2 |        5 |
|       3 |        6 |
|       4 |       55 |
|       5 |       12 |
|       6 |        9 |
|       7 |       35 |
|       8 |        3 |
+---------+----------+
8 rows in set (0.00 sec)
```
- 查询每个部门中年龄最大的员工
```mysql
mysql> select dept_id, min(birth_date) from employees group by dept_id;
+---------+-----------------+
| dept_id | min(birth_date) |
+---------+-----------------+
|       1 | 1971-08-19      |
|       2 | 1971-11-02      |
|       3 | 1971-09-09      |
|       4 | 1972-01-31      |
|       5 | 1971-08-14      |
|       6 | 1973-04-14      |
|       7 | 1971-12-10      |
|       8 | 1989-05-19      |
+---------+-----------------+
8 rows in set (0.00 sec)
```
- 查询每个部门入职最晚员工的入职时间
```mysql
mysql> select dept_id, max(hire_date) from employees group by dept_id;
+---------+----------------+
| dept_id | max(hire_date) |
+---------+----------------+
|       1 | 2018-11-21     |
|       2 | 2018-09-03     |
|       3 | 2019-07-04     |
|       4 | 2021-02-04     |
|       5 | 2019-06-08     |
|       6 | 2017-10-07     |
|       7 | 2020-08-21     |
|       8 | 2019-11-14     |
+---------+----------------+
8 rows in set (0.00 sec)
```
- 统计各部门使用tedu.cn邮箱的员工人数
```mysql
mysql> select dept_id, count(*) from employees where email like '%@tedu.cn' group by dept_id;
+---------+----------+
| dept_id | count(*) |
+---------+----------+
|       1 |        5 |
|       2 |        2 |
|       3 |        4 |
|       4 |       32 |
|       5 |        7 |
|       6 |        5 |
|       7 |       15 |
|       8 |        1 |
+---------+----------+
8 rows in set (0.00 sec)
```
- 查看员工2018年工资总收入，按总收入进行降序排列
```mysql
mysql> select employee_id, sum(basic+bonus) as total from salary where year(date)=2018 group by employee_id order by total desc;
```
- 查询部门人数少于10人
```mysql
mysql> select dept_id, count(*) from employees where count(*)<10 group by dept_id;
ERROR 1111 (HY000): Invalid use of group function


mysql> select dept_id, count(*) from employees group by dept_id having count(*)<10;
+---------+----------+
| dept_id | count(*) |
+---------+----------+
|       1 |        8 |
|       2 |        5 |
|       3 |        6 |
|       6 |        9 |
|       8 |        3 |
+---------+----------+
5 rows in set (0.00 sec)
```
## 连接查询

- 也叫多表查询。常用于查询字段来自于多张表
- 如果直接查询两张表，将会得到笛卡尔积

```mysql
mysql> select name, dept_name from employees, departments;
```
- 通过添加有效的条件可以进行查询结果的限定
```mysql
mysql> select name, dept_name from employees, departments where employees.dept_id=departments.dept_id;
```
### 连接分类

#### 按功能分类

- 内连接(**重要**)
  - 等值连接
  - 非等值连接
  - 自连接
- 外连接
  - 左外连接(**重要**)
  - 右外连接(**重要**)
  - 全外连接(mysql不支持，可以使用UNION实现相同的效果)
- 交叉连接

#### 按年代分类

- SQL92标准：仅支持内连接
- SQL99标准：支持所有功能的连接

### SQL99标准多表查询

- 语法格式

```mysql
SELECT 字段... 
FROM 表1 [AS] 别名 [连接类型]
JOIN 表2 [AS] 别名
ON 连接条件
WHERE 分组前筛选条件
GROUP BY 分组
HAVING 分组后筛选条件
ORDER BY 排序字段
```

#### 内连接

- 语法格式

```mysql
select 查询列表
from 表1 别名
inner join 表2 别名 on 连接条件
inner join 表3 别名 on 连接条件
[where 筛选条件]
[group by 分组]
[having 分组后筛选]
[order by 排序列表]
```

##### 等值连接

- 查询每个员工所在的部门名
```mysql
mysql> select name, dept_name
    -> from employees
    -> inner join departments
    -> on employees.dept_id=departments.dept_id;
```
- 查询每个员工所在的部门名，使用别名
```mysql
mysql> select name, dept_name
    -> from employees as e
    -> inner join departments as d
    -> on e.dept_id=d.dept_id;
```
- 查询每个员工所在的部门名，使用别名。两个表中的同名字段，必须指定表名
```mysql
mysql> select name, d.dept_id, dept_name
    -> from employees as e
    -> inner join departments as d
    -> on e.dept_id=d.dept_id;
```
- 查询11号员工的名字及2018年每个月工资
```mysql
mysql> select name, date, basic+bonus as total
    -> from employees as e
    -> inner join salary as s
    -> on e.employee_id=s.employee_id
    -> where year(s.date)=2018 and e.employee_id=11;
```
- 查询2018年每个员工的总工资
```mysql
mysql> select name, sum(basic+bonus) from employees
    -> inner join salary
    -> on employees.employee_id=salary.employee_id
    -> where year(salary.date)=2018
    -> group by name;
```
- 查询2018年每个员工的总工资，按工资升序排列
```mysql
mysql> select name, sum(basic+bonus) as total from employees as e
    -> inner join salary as s
    -> on e.employee_id=s.employee_id
    -> where year(s.date)=2018
    -> group by name
    -> order by total;
```
- 查询2018年总工资大于30万的员工，按工资降序排列
```mysql
 mysql> select name, sum(basic+bonus) as total from employees as e
    -> inner join salary as s
    -> on e.employee_id=s.employee_id
    -> where year(s.date)=2018
    -> group by name
    -> having total>300000
    -> order by total desc;
```
##### 非等值连接

> 附：创建工资级别表
>
> 创建表语法：
>
> ```mysql
> CREATE TABLE 表名称
> (
> 列名称1 数据类型,
> 列名称2 数据类型,
> 列名称3 数据类型,
> ....
> )
> ```
>
> 创建工资级别表：
>
> - id：主键。仅作为表的行号
> - grade：工资级别，共ABCDE五类
> - low：该级别最低工资
> - high：该级别最高工资
>
> ```mysql
> mysql> use nsd2021;
> mysql> create table wage_grade
>     -> (
>     -> id int,
>     -> grade char(1),
>     -> low int,
>     -> high int,
>     -> primary key (id));
> ```
>
> 向表中插入数据：
>
> - 语法:
>
> ```mysql
> INSERT INTO 表名称 VALUES (值1, 值2,....);
> ```
>
> - 向wage_grade表中插入五行数据：
>
> ```mysql
> mysql> insert into wage_grade values
>     -> (1, 'A', 5000, 8000),
>     -> (2, 'B', 8001, 10000),
>     -> (3, 'C', 10001, 15000),
>     -> (4, 'D', 15001, 20000),
>     -> (5, 'E', 20001, 1000000);
> ```

- 查询2018年12月员工基本工资级别
```mysql
mysql> select employee_id, date, basic, grade
    -> from salary as s
    -> inner join wage_grade as g
    -> on s.basic between g.low and g.high
    -> where year(date)=2018 and month(date)=12;
```
- 查询2018年12月员工各基本工资级别的人数
```mysql
mysql> select grade, count(*)
    -> from salary as s
    -> inner join wage_grade as g
    -> on s.basic between g.low and g.high
    -> where year(date)=2018 and month(date)=12
    -> group by grade;
+-------+----------+
| grade | count(*) |
+-------+----------+
| A     |       13 |
| B     |       12 |
| C     |       30 |
| D     |       32 |
| E     |       33 |
+-------+----------+
5 rows in set (0.00 sec)
```
- 查询2018年12月员工基本工资级别，员工需要显示姓名
```mysql
mysql> select name, date, basic, grade
    -> from salary as s
    -> inner join employees as e
    -> on s.employee_id=e.employee_id
    -> inner join wage_grade
    -> on basic between low and high
    -> where date='20181210'
    -> order by grade, basic;
```
##### 自连接

- 要点：
  - 将一张表作为两张使用
  - 每张表起一个别名

- 查看哪些员的生日月份与入职月份相同
```mysql
mysql> select e.name, e.hire_date, em.birth_date
    -> from employees as e
    -> inner join employees as em
    -> on month(e.hire_date)=month(em.birth_date)
    -> and e.employee_id=em.employee_id;
+-----------+------------+------------+
| name      | hire_date  | birth_date |
+-----------+------------+------------+
| 李玉英    | 2012-01-19 | 1974-01-25 |
| 郑静      | 2018-02-03 | 1997-02-14 |
| 林刚      | 2007-09-19 | 1990-09-23 |
| 刘桂兰    | 2003-10-14 | 1982-10-11 |
| 张亮      | 2015-08-10 | 1996-08-25 |
| 许欣      | 2011-09-09 | 1982-09-25 |
| 王荣      | 2019-11-14 | 1999-11-22 |
+-----------+------------+------------+
7 rows in set (0.00 sec)
```
#### 外连接

- 常用于查询一个表中有，另一个表中没有的记录

- 如果从表中有和它匹配的，则显示匹配的值
- 如要从表中没有和它匹配的，则显示NULL
- 外连接查询结果=内连接查询结果+主表中有而从表中没有的记录
- 左外连接中，left join左边的是主表
- 右外连接中，right join右边的是主表
- 左外连接和右外连接可互换，实现相同的目标

##### 左外连接

- 语法

```mysql
SELECT tb1.字段..., tb2.字段
FROM table1 AS tb1
LEFT OUTER JOIN table2 AS tb2 
ON tb1.字段=tb2.字段
```

- 查询所有部门的人员以及没有员工的部门

```mysql
mysql> select d.*, e.name
    -> from departments as d
    -> left outer join employees as e
    -> on d.dept_id=e.dept_id;
```
##### 右外连接

- 语法

```mysql
SELECT tb1.字段..., tb2.字段
FROM table1 AS tb1
RIGHT OUTER JOIN table2 AS tb2 
ON tb1.字段=tb2.字段
```

- 查询所有部门的人员以及没有员工的部门
```mysql
mysql> select d.*, e.name
    -> from employees as e
    -> right outer join departments as d
    -> on d.dept_id=e.dept_id;
```
##### 交叉连接

- 返回笛卡尔积
- 语法：

```mysql
SELECT <字段名> FROM <表1> CROSS JOIN <表2> [WHERE子句]
```
- 查询员工表和部门表的笛卡尔积
```mysql
mysql> select name, dept_name
    -> from employees
    -> cross join departments;
```



> 附：授予管理员root可以通过任意地址访问数据库，密码是NSD2021@tedu.cn。默认情况下，root只允许在本机访问
>
> ```mysql
> mysql> grant all on *.* to root@'%' identified by 'NSD2021@tedu.cn';
> ```
>
> 向部门表中插入数据：
>
> ```mysql
> mysql> insert into departments(dept_name) values('采购部');
> 
> ```
>
> 



