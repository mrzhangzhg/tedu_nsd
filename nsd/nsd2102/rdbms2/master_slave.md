# 主从同步
<p align="right">
  By <a href="https://www.jianshu.com/u/665c84e77f9c">Mr.张志刚</a>
</p>

```mermaid
graph LR
master(mysql1:192.168.1.11)-->slave(mysql2:192.168.1.12)
```

服务器角色：mysql1为主，mysql2为从。

mysql1配置：ip地址为192.168.1.11，安装mysql，初始化密码为NSD2011@tedu.cn

mysql2配置：ip地址为192.168.1.12，安装mysql，初始化密码为NSD2011@tedu.cn

## 主从同步配置

### 主服务器

1. 在主服务器上导入数据

```mysql
[root@zzgrhel8 ~]# scp -r /root/tedu_nsd/dbs/mysql_scripts/ 192.168.1.11:/root
[root@zzgrhel8 ~]# ssh 192.168.1.11
[root@mysql1 ~]# mysql -uroot -pNSD2021@tedu.cn < mysql_scripts/nsd2021_data.sql 
```

2. 启用主服务器

```mysql
# 修改配置文件
[root@mysql1 ~]# vim /etc/my.cnf
[mysqld]
server_id = 11
log-bin = master11
... ...

# 重启服务
[root@mysql1 ~]# systemctl restart mysqld

# 验证
[root@mysql1 ~]# ls /var/lib/mysql/master11.*
/var/lib/mysql/master11.000001  /var/lib/mysql/master11.index
[root@mysql1 ~]# mysql -uroot -pNSD2021@tedu.cn
mysql> show master status;
+-----------------+----------+--------------+------------------+-------------------+
| File            | Position | Binlog_Do_DB | Binlog_Ignore_DB | Executed_Gtid_Set |
+-----------------+----------+--------------+------------------+-------------------+
| master11.000001 |      154 |              |                  |                   |
+-----------------+----------+--------------+------------------+-------------------+
1 row in set (0.00 sec)
```

3. 授权，允许从服务器通过repluser用户进行同步

```mysql
mysql> grant replication slave on *.* to repluser@'%' identified by 'NSD2021@tedu.cn';
```

### 从服务器

1. 修改配置文件

```mysql
[root@mysql2 ~]# vim /etc/my.cnf
[mysqld]
server_id = 12
... ...

[root@mysql2 ~]# systemctl restart mysqld
```

2. 与主服务器进行自动同步之前，先手工同步数据库

```mysql
# 主服务器备份数据库
[root@mysql1 ~]# mysqldump -uroot -pNSD2021@tedu.cn --master-data nsd2021 > /root/fullbackup.sql

# 拷贝备份文件到从服务器
[root@mysql1 ~]# scp /root/fullbackup.sql 192.168.1.12:/root

# 从服务器导入数据
[root@mysql2 ~]# mysql -uroot -pNSD2021@tedu.cn
mysql> create database nsd2021 default charset utf8mb4;
[root@mysql2 ~]# mysql -uroot -pNSD2021@tedu.cn nsd2021 < fullbackup.sql 
```

3. 设置从服务器从主服务器同步

```mysql
# 查看binlog日志的文件名和偏移量
[root@mysql2 ~]# grep master11 fullbackup.sql 
CHANGE MASTER TO MASTER_LOG_FILE='master11.000001', MASTER_LOG_POS=441;

# 通过命令设置主服务器信息
[root@mysql2 ~]# mysql -uroot -pNSD2021@tedu.cn 
mysql> change master to
    -> master_host='192.168.1.11',
    -> master_user='repluser',
    -> master_password='NSD2021@tedu.cn',
    -> master_log_file='master11.000001',
    -> master_log_pos=441;

# 启动从服务器进程
mysql> start slave;

# 查看
mysql> show slave status\G
... ...
             Slave_IO_Running: Yes
            Slave_SQL_Running: Yes
... ...
```





