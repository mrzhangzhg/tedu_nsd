# db_project_day01



## mysql服务器

### 配置lv

```shell
[root@mysql11 ~]# yum install -y lvm2
[root@mysql11 ~]# pvcreate /dev/vd{b,c}
[root@mysql11 ~]# vgcreate vg0 /dev/vd{b,c}
# 创建lv，使用全部的vg空间
[root@mysql11 ~]# lvcreate -n lv0 -l100%VG vg0
[root@mysql11 ~]# mkfs.xfs /dev/vg0/lv0 
```

### 配置mysql

```shell
# 安装mysql服务器
[root@mysql11 ~]# yum install -y mysql-community*
# 挂载lv0到/var/lib/mysql
[root@mysql11 ~]# vim /etc/fstab 
/dev/vg0/lv0    /var/lib/mysql  xfs     defaults	0 0
[root@mysql11 ~]# mount -a
# 初始化mysql服务器
[root@mysql11 ~]# systemctl enable mysqld
[root@mysql11 ~]# systemctl start mysqld
[root@mysql11 ~]# grep password /var/log/mysqld.log 
[root@mysql11 ~]# mysqladmin -uroot -p'0mI.ij-oJlxj' password NSD2021@tedu.cn
```

### mysql主从

- 主服务器

```mysql
[root@mysql11 ~]# vim /etc/my.cnf
... ...
[mysqld]
server-id = 11
log-bin = master11
... ...
[root@mysql11 ~]# systemctl restart mysqld
[root@mysql11 ~]# mysql -uroot -pNSD2021@tedu.cn
mysql> grant replication slave on *.* to repluser@'%' identified by 'NSD2021@tedu.cn';
mysql> show master status;
+-----------------+----------+--------------+------------------+-------------------+
| File            | Position | Binlog_Do_DB | Binlog_Ignore_DB | Executed_Gtid_Set |
+-----------------+----------+--------------+------------------+-------------------+
| master11.000001 |      441 |              |                  |                   |
+-----------------+----------+--------------+------------------+-------------------+
1 row in set (0.00 sec)
```

- 从服务器

```mysql
[root@mysql22 ~]# vim /etc/my.cnf
... ...
[mysqld]
server-id = 22
... ...
[root@mysql22 ~]# systemctl restart mysqld
[root@mysql22 ~]# mysql -uroot -pNSD2021@tedu.cn
mysql> change master to
    -> master_host="192.168.1.11",
    -> master_user="repluser",
    -> master_password="NSD2021@tedu.cn",
    -> master_log_file="master11.000001",
    -> master_log_pos=441;
mysql> start slave;
mysql> show slave status\G
... ...
             Slave_IO_Running: Yes
            Slave_SQL_Running: Yes
... ...
```

## 读写分离

```shell
[root@maxscale77 ~]# yum install -y maxscale
[root@maxscale77 ~]# vim /etc/maxscale.cnf
... ...
[maxscale]
threads=auto
... ...
[server1]
type=server
address=192.168.1.11
port=3306
protocol=MySQLBackend

[server2]
type=server
address=192.168.1.22
port=3306
protocol=MySQLBackend
... ...
[MySQL Monitor]
type=monitor
module=mysqlmon
servers=server1,server2
user=maxscalemon
passwd=NSD2021@tedu.cn
monitor_interval=10000
... ...
# [Read-Only Service]
# type=service
# router=readconnroute
# servers=server1
# user=myuser
# passwd=mypwd
# router_options=slave
... ...
[Read-Write Service]
type=service
router=readwritesplit
servers=server1,server2
user=maxscalerouter
passwd=NSD2021@tedu.cn
max_slave_connections=100%
... ...
# [Read-Only Listener]
# type=listener
# service=Read-Only Service
# protocol=MySQLClient
# port=4008
... ...

[Read-Write Listener]
type=listener
service=Read-Write Service
protocol=MySQLClient
port=3306
 
[MaxAdmin Listener]
type=listener
service=MaxAdmin Service
protocol=maxscaled
socket=default
port=4016

[root@mysql11 ~]# mysql -uroot -pNSD2021@tedu.cn
mysql> grant replication slave,replication client on *.* to maxscalemon@'%' identified by 'NSD2021@tedu.cn';
mysql> grant select on mysql.* to maxscalerouter@'%' identified by 'NSD2021@tedu.cn';

[root@maxscale77 ~]# systemctl enable maxscale
[root@maxscale77 ~]# systemctl start maxscale
[root@maxscale77 ~]# maxadmin -uadmin -pmariadb -P4016
MaxScale> list servers
Servers.
-------------------+-----------------+-------+-------------+--------------------
Server             | Address         | Port  | Connections | Status              
-------------------+-----------------+-------+-------------+--------------------
server1            | 192.168.1.11    |  3306 |           0 | Master, Running
server2            | 192.168.1.22    |  3306 |           0 | Slave, Running
-------------------+-----------------+-------+-------------+--------------------

[root@mysql11 ~]# mysql -uroot -pNSD2021@tedu.cn
mysql> grant all on *.* to zzg@'%' identified by 'NSD2021@tedu.cn';
[root@zzgrhel8 ~]# mysql -h192.168.1.77 -uzzg -pNSD2021@tedu.cn
MySQL [(none)]> select @@hostname;
+------------+
| @@hostname |
+------------+
| mysql22    |
+------------+

```

## NFS服务器

- 准备磁盘

```shell
# 创建一个GPT分区表的磁盘结构
[root@nfs30 ~]# fdisk /dev/vdb 
命令(输入 m 获取帮助)：g   # 标记磁盘为GPT分区表
命令(输入 m 获取帮助)：n
分区号 (1-128，默认 1)：   # 回车
第一个扇区 (2048-20971486，默认 2048)：  # 回车
Last sector, +sectors or +size{K,M,G,T,P} (2048-20971486，默认 20971486)：     # 回车
命令(输入 m 获取帮助)：p    # 查看分区结果
命令(输入 m 获取帮助)：w

[root@nfs30 ~]# mkfs.xfs /dev/vdb1
[root@nfs30 ~]# mkdir /sitedir
[root@nfs30 ~]# vim /etc/fstab 
/dev/vdb1       /sitedir        xfs     defaults	0 0
[root@nfs30 ~]# mount -a
```

- 配置NFS服务

```SHELL
[root@nfs30 ~]# yum install -y nfs-utils rpcbind
# nfs依赖RPC服务，所以需要先启动rpcbind
[root@nfs30 ~]# systemctl enable rpcbind
[root@nfs30 ~]# systemctl start rpcbind
[root@nfs30 ~]# vim /etc/exports
/sitedir        *(rw)
[root@nfs30 ~]# systemctl start nfs
[root@nfs30 ~]# showmount -e 
Export list for nfs30:
/sitedir *
```

## 配置web服务

```shell
[root@web33 ~]# yum repolist
[root@web33 ~]# yum install -y java-1.8.0-openjdk.x86_64 

[root@zzgrhel8 ~]# scp /linux-soft/2/lnmp_soft.tar.gz 192.168.1.33:/root

[root@web33 ~]# tar xf lnmp_soft.tar.gz 
[root@web33 ~]# cd lnmp_soft/
[root@web33 lnmp_soft]# tar xf apache-tomcat-8.0.30.tar.gz 
[root@web33 lnmp_soft]# mv apache-tomcat-8.0.30 /usr/local/tomcat

[root@web33 ~]# vim /etc/bashrc 
export PATH=$PATH:/usr/local/tomcat/bin
[root@web33 ~]# source /etc/bashrc

[root@web33 ~]# yum install -y mysql-connector-java.noarch 
[root@web33 ~]# cp /usr/share/java/mysql-connector-java.jar /usr/local/tomcat/lib/
[root@web33 ~]# rm -fr /usr/local/tomcat/webapps/ROOT/*

[root@web33 ~]# yum install -y nfs-utils rpcbind
[root@web33 ~]# showmount -e 192.168.1.30
Export list for 192.168.1.30:
/sitedir *
[root@web33 ~]# vim /etc/fstab 
192.168.1.30:/sitedir   /usr/local/tomcat/webapps/ROOT/	nfs     defaults        0 0
[root@web33 ~]# mount -a
[root@web33 ~]# df -h /usr/local/tomcat/webapps/ROOT/
文件系统               容量  已用  可用 已用% 挂载点
192.168.1.30:/sitedir   10G   32M   10G    1% /usr/local/tomcat/webapps/ROOT

[root@web33 ~]# startup.sh 
[root@web33 ~]# vim /etc/rc.d/rc.local   # 尾部追加
/usr/local/tomcat/bin/startup.sh
[root@web33 ~]# chmod +x /etc/rc.d/rc.local

[root@nfs30 ~]# vim /sitedir/test.html
<marquee>my web site test</marquee>

# 测试：通过浏览器访问http://192.168.1.33:8080/test.html
```

## 测试

```shell
[root@nfs30 ~]# vim /sitedir/linkdb.jsp
<%@ page language="java" import="java.util.*" pageEncoding="gbk"%>
<%@ page import="java.naming.*" %>
<%@ page import="java.sql.*" %>
<html>
     <body>
          <%
          Class.forName("com.mysql.jdbc.Driver");
Connection con=DriverManager.getConnection("jdbc:mysql://192.168.1.77:3306/", "jspuser1","NSD2021@tedu.cn");

Statement state=con.createStatement();

String sql="insert into gamedb.user(name) values('TOM')";
state.executeUpdate(sql);
           %>
           <h1>data save ok</h1>
     </body>
</html>

[root@mysql11 ~]# mysql -uroot -pNSD2021@tedu.cn
mysql> grant all on gamedb.* to jspuser1@'%' identified by 'NSD2021@tedu.cn';
mysql> create database gamedb default charset utf8mb4;
mysql> use gamedb;
mysql> create table user(
    ->   id int primary key auto_increment,
    ->   name varchar(20)
    -> );

# 访问http://192.168.1.33:8080/linkdb.jsp
[root@mysql11 ~]# mysql -uroot -pNSD2021@tedu.cn
mysql> select * from gamedb.user;
+----+------+
| id | name |
+----+------+
|  1 | TOM  |
|  2 | TOM  |
+----+------+
2 rows in set (0.00 sec)
[root@mysql22 ~]# mysql -uroot -pNSD2021@tedu.cn
mysql> select * from gamedb.user;
+----+------+
| id | name |
+----+------+
|  1 | TOM  |
|  2 | TOM  |
+----+------+
2 rows in set (0.00 sec)
```













