# day01

- 对服务的管理，不能仅限于可用性。还需要服务可以安全、稳定、高效地运行。
- 监控的目的：早发现、早治疗
- 被监控的资源类型：
  - 公开数据：对外开放的、不需要认证即可获得的数据
  - 私有数据：对外不开放、需要认证才能获得的数据

## 监控命令

```shell
[root@zzgrhel8 ~]# uptime   # 最后的三个数是CPU一分钟、五分钟、15分钟的平均负载
 09:55:37 up 22 days, 54 min,  0 users,  load average: 0.63, 0.26, 0.16
# 负载是指有多个任务在同时运行，该值不应该长期大于CPU总核心数

[root@zzgrhel8 ~]# free -m    # 主要查看内存
[root@zzgrhel8 ~]# swapon -s  # 查看swap空间
[root@zzgrhel8 ~]# df -h      # 查看硬盘利用率
[root@zzgrhel8 ~]# df -i      # 查看inode节点利率
[root@zzgrhel8 ~]# tracepath www.baidu.com   # 跟踪路径

[root@zzgrhel8 ~]# yum install sysstat.x86_64
[root@zzgrhel8 ~]# iostat 1 3   # 测试硬盘IO，每隔一秒采一次样，共采用3次
```

## zabbix

- 实施监控的几个方面：
  - 数据采集：使用snmp协议（网络设备等）或agent（可装软件的系统）
  - 数据存储：使用数据库mysql
  - 数据展示：通过web页面
- zabbix通过snmp或agent采集数据，存储到mysql数据库，通过web展示

- 环境准备：YUM、SELINUX和防火墙关闭

| 主机名       | 地址             |
| ------------ | ---------------- |
| zabbixserver | 192.168.4.5/24   |
| web1         | 192.168.4.100/24 |
| web2         | 192.168.4.200/24 |

- 安装zabbix server。注意，将mysql yum源注释掉，使用光盘自带的源即可。

```shell
# 配置nginx web服务器
[root@zabbixserver ~]# yum install -y gcc pcre-devel openssl-devel
[root@zzgrhel8 ~]# scp /linux-soft/2/lnmp_soft.tar.gz 192.168.4.5:/root/
[root@zabbixserver ~]# tar xf lnmp_soft.tar.gz 
[root@zabbixserver ~]# cd lnmp_soft/
[root@zabbixserver lnmp_soft]# tar xf nginx-1.12.2.tar.gz 
[root@zabbixserver nginx-1.12.2]# ./configure --with-http_ssl_module
[root@zabbixserver nginx-1.12.2]# make && make install


# 配置php连接mariadb服务器
[root@zabbixserver nginx-1.12.2]# yum install php php-mysql php-fpm mariadb-server mariadb-devel
# 根据zabbix手册，配置nginx参数
[root@zabbixserver ~]# vim /usr/local/nginx/conf/nginx.conf
... ...
 34     fastcgi_buffers     8 16k;  # 缓存php生成的页面内容，8个16k
 35     fastcgi_buffer_size 32k;    # 缓存php生成的头部信息
 36     fastcgi_connect_timeout     300;   # 连接Php的超时时间
 37     fastcgi_send_timeout        300;   # 发送php的超时时间
 38     fastcgi_read_timeout        300;   # 读取请求的超时时间
 ... ...
 70         location ~ \.php$ {
 71             root           html;
 72             fastcgi_pass   127.0.0.1:9000;
 73             fastcgi_index  index.php;
 74         #    fastcgi_param  SCRIPT_FILENAME  /script    s$fastcgi_script_name;
 75             include        fastcgi.conf;   # 注意这里要修改
 76         }
 
# 启动相关服务
[root@zabbixserver ~]# /usr/local/nginx/sbin/nginx 
# 将nginx命令加入开机自动执行。/etc/rc.local是开机最后一个运行的脚本
[root@zabbixserver ~]# echo /usr/local/nginx/sbin/nginx >> /etc/rc.local 
[root@zabbixserver ~]# systemctl enable mariadb --now
[root@zabbixserver ~]# systemctl enable php-fpm --now

# 编译安装zabbix
[root@zabbixserver lnmp_soft]# yum install -y net-snmp-devel curl-devel autoconf libevent-devel
[root@zabbixserver ~]# cd lnmp_soft/
[root@zabbixserver lnmp_soft]# tar xf zabbix-3.4.4.tar.gz 
[root@zabbixserver lnmp_soft]# cd zabbix-3.4.4/
[root@zabbixserver zabbix-3.4.4]# ./configure --enable-server --enable-proxy --enable-agent --with-mysql=/usr/bin/mysql_config --with-net-snmp --with-libcurl
# --enable-server  安装服务器
# --enable-proxy   安装proxy代理，不是必须的
# --enable-agent  安装被控端
# --with-mysql    指定mysql_config文件所在位置
# --with-net-snmp 启用snmp支持
# --with-libcurl  安装curl库文件，以便zabbix可以通过curl连接http服务
[root@zabbixserver zabbix-3.4.4]# make && make install
```

> 注意：删除错误的软件包的方法
>
> ```shell
> [root@zabbixserver ~]# yum remove -y mysql-community* Percona*
> 
> # yum源中只有以下内容，多余的源删掉！！！
> [root@zabbixserver ~]# vim /etc/yum.repos.d/local.repo 
> [local_repo]
> name=CentOS-$releasever - Base
> baseurl=ftp://192.168.4.254/centos-1804
> enabled=1
> gpgcheck=0
> [root@zabbixserver ~]# yum install mariadb-server mariadb-devel
> 
> # 起服务
> [root@zabbixserver ~]# systemctl enable mariadb --now
> ```

- 初始化zabbix

```shell
# 创建zabbix工作时需要的数据库zabbix，缺省的字符集为utf8
[root@zabbixserver ~]# mysql
MariaDB [(none)]> create database zabbix default charset utf8;

# 创建名为zabbix的用户，他的密码也是zabbix，可以对zabbix数据库有所有权限
MariaDB [(none)]> grant all on zabbix.* to zabbix@'localhost' identified by 'zabbix';
MariaDB [(none)]> exit

# 导入初始化数据，注意导入顺序
[root@zabbixserver ~]# cd lnmp_soft/zabbix-3.4.4/database/mysql/
# -u指定用户名，-p指定密码，第3个zabbix是数据库名
[root@zabbixserver mysql]# mysql -uzabbix -pzabbix zabbix < schema.sql 
[root@zabbixserver mysql]# mysql -uzabbix -pzabbix zabbix < images.sql 
[root@zabbixserver mysql]# mysql -uzabbix -pzabbix zabbix < data.sql 



# 配置zabbix_server
[root@zabbixserver ~]# vim /usr/local/etc/zabbix_server.conf
 12 # ListenPort=10051     # 服务端口
 38 LogFile=/tmp/zabbix_server.log    # 日志文件
 85 DBHost=localhost       # 数据库服务器
 95 DBName=zabbix          # 数据库名字
111 DBUser=zabbix          # 登陆数据库的账号
119 DBPassword=zabbix      # 登陆数据库的密码

# 创始用于启动zabbix的账号
[root@zabbixserver ~]# useradd -s /sbin/nologin zabbix

# 创建用于管理zabbix的service文件
[root@zabbixserver ~]# vim /usr/lib/systemd/system/zabbix_server.service
[Unit]
Description=zabbix server
After=network.target remote-fs.target nss-lookup.target
[Service]
Type=forking
PIDFile=/tmp/zabbix_server.pid
ExecStart=/usr/local/sbin/zabbix_server
ExecStop=/bin/kill $MAINPID
[Install]
WantedBy=multi-user.target

# 启服务
[root@zabbixserver ~]# systemctl enable zabbix_server.service --now
[root@zabbixserver ~]# ss -tlnp | grep zabbix
```

- 配置zabbix监控自己，被监控端是agent

```shell
[root@zabbixserver ~]# vim /usr/local/etc/zabbix_agentd.conf
 30 LogFile=/tmp/zabbix_agentd.log 
 93 Server=127.0.0.1,192.168.4.5      # 允许哪些主机监控
101 # ListenPort=10050                # 客户端服务端口号
134 ServerActive=127.0.0.1,192.168.4.5  # 允许哪些主机主动监控
145 Hostname=zabbixserver             # 主机名
280 UnsafeUserParameters=1            # 允许用户自定义监控项

# 配置服务
[root@zabbixserver ~]# vim /usr/lib/systemd/system/zabbix_agentd.service
[Unit]
Description=zabbix agent
After=network.target remote-fs.target nss-lookup.target
[Service]
Type=forking
PIDFile=/tmp/zabbix_agentd.pid
ExecStart=/usr/local/sbin/zabbix_agentd
ExecStop=/bin/kill $MAINPID
[Install]
WantedBy=multi-user.target

# 启服务
[root@zabbixserver ~]# systemctl enable zabbix_agentd.service --now
```

- zabbix的管理是通过web页面完成的。拷贝zabbix的web页面

```shell
# 拷贝文件
[root@zabbixserver ~]# cp -r lnmp_soft/zabbix-3.4.4/frontends/php/* /usr/local/nginx/html/

# php-fpm服务需要对该目录有读写权限，php-fpm所用的用户叫apache
[root@zabbixserver ~]# chown -R apache:apache /usr/local/nginx/html/

# 打开浏览器访问 http://192.168.4.5/index.php。
# 点击Next Step时，如果缺少组件，将会有提示：
[root@zabbixserver ~]# yum install -y php-gd php-xml php-bcmath php-mbstring
[root@zabbixserver ~]# systemctl restart php-fpm

# 配置php.ini，以满足zabbix对php的需要
[root@zabbixserver ~]# vim /etc/php.ini
 394 max_input_time = 300
 384 max_execution_time = 300
 878 date.timezone = Asia/Shanghai
 672 post_max_size = 32M
[root@zabbixserver ~]# systemctl restart php-fpm
```

### 添加被监控端









