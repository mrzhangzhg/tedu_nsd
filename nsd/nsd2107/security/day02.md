# day02

## 添加被控端

- 在web1上安装zabbix-agent。

```shell
# server和agent使用的是相同的源码文件
[root@zabbixserver ~]# scp lnmp_soft/zabbix-3.4.4.tar.gz 192.168.4.100:/root

# 安装编译的依赖环境
[root@web1 ~]# yum install -y gcc pcre-devel autoconf

# 安装agent
[root@web1 ~]# tar xf zabbix-3.4.4.tar.gz 
[root@web1 ~]# cd zabbix-3.4.4/
[root@web1 zabbix-3.4.4]# ./configure --enable-agent
[root@web1 zabbix-3.4.4]# make && make install

# 修改配置文件
[root@web1 ~]# vim /usr/local/etc/zabbix_agentd.conf
 30 LogFile=/tmp/zabbix_agentd.log
 69 EnableRemoteCommands=1   # 允许监控端远程执行命令
 93 Server=127.0.0.1,192.168.4.5    # 允许自己和zabbixserver监控
134 ServerActive=127.0.0.1,192.168.4.5  # 允许自己和zabbixserver主动监控
145 Hostname=web1
280 UnsafeUserParameters=1   # 允许用户创建自定义监控项

# 配置服务
[root@zabbixserver ~]# vim /usr/lib/systemd/system/zabbix_agentd.service 
[Unit]
Description=zabbix agent
After=network.target remote-fs.target nss-lookup.target
[Service]
Type=forking
PIDFile=/tmp/zabbix_agentd.pid
ExecStart=/usr/local/sbin/zabbix_agentd
ExecStop=/bin/kill $MAINPID
[Install]
WantedBy=multi-user.target
[root@web1 ~]# useradd -s /sbin/nologin zabbix
[root@web1 ~]# systemctl enable zabbix_agentd.service --now
[root@web1 ~]# ss -tlnp | grep :10050
```

## 在zabbix web管理平台中配置监控

- 主机：被监控的主机
- 主机组：根据需求，将多台主机加入到一个主机组中，方便管理。系统已经提供一些主机组。
- 模板：将监控项配置到模板中，模板再应用给主机，方便相同监控项的配置。系统中已经包括大量模板。

### 添加监控主机

- 访问：192.168.4.5/index.php

![image-20211009103110365](../imgs/image-20211009103110365.png)

![image-20211009103255479](../imgs/image-20211009103255479.png)

![image-20211009103513705](../imgs/image-20211009103513705.png)

- 查看监项数据

![image-20211009104234567](../imgs/image-20211009104234567.png)

需要查看哪个项目，可以点击右边的“图形”

![image-20211009104356220](../imgs/image-20211009104356220.png)

常用的监控指标：

- CPU idle time：CPU空闲时间。不宜过低
- Processor load (1 min average per core) / Processor load (5 min average per core) / Processor load (15 min average per core)：CPU每个核心1分钟、5分钟、15分钟内的平均负载。该值不应该长期大于1
- Free disk space on / ：根文件系统的剩余空间大小
- Free disk space on / (percentage) ：根文件系统的剩余空间百分比

- Available memory：剩余内存
- Incoming network traffic on eth0：eth0网卡进入的流量
- Outgoing network traffic on eth0：eth0网卡流出的流量
- Maximum number of processes：最多运行的进程数
- Number of logged in users：已登陆的用户数

### 自定义监控项

- 实现监控用户数的监控项：/etc/passwd有多少行就有多少个用户

##### 在监控主机上创建自定义key（监控项）

```shell
# 创建监控项。语法：UserParameter=自定义key名,命令
# 命令的值结果是key的value

# 声明自定义监控项所用取的配置文件
[root@web1 ~]# vim /usr/local/etc/zabbix_agentd.conf
264 Include=/usr/local/etc/zabbix_agentd.conf.d/

# 创建自定义配置文件。
[root@web1 ~]# vim /usr/local/etc/zabbix_agentd.conf.d/count.line.passwd
# count.line.passwd是监控项的名；sed -n '$=' /etc/passwd是命令，它的执行结果是count.line.passwd的值
UserParameter=count.line.passwd,sed -n '$=' /etc/passwd

# 测试自定义监控项
[root@web1 ~]# systemctl restart zabbix_agentd.service 
# 查看127.0.0.1上key名为count.line.passwd这个监控项的值
[root@web1 ~]# zabbix_get -s 127.0.0.1 -k count.line.passwd
23
```

##### 创建监控模板

- 模板可以包含一到多个应用集。相当于是相似功能的监控项组
- 应用集中包含一到多个监控项。

![image-20211009114126788](../imgs/image-20211009114126788.png)

创建名为count.line.passwd的模板。它属于一个新的组：count-passwd，该组不存在则自动创建。

![image-20211009114246453](../imgs/image-20211009114246453.png)

- 在新模板中创建应用集

![image-20211009114531862](../imgs/image-20211009114531862.png)

![image-20211009114608482](../imgs/image-20211009114608482.png)

![image-20211009114649712](../imgs/image-20211009114649712.png)

- 在应用集中添加监控项

![image-20211009114752343](../imgs/image-20211009114752343.png)

![image-20211009114834521](../imgs/image-20211009114834521.png)

![image-20211009114956605](../imgs/image-20211009114956605.png)

##### 将新模板应用到主机

![image-20211009115659638](../imgs/image-20211009115659638.png)

![image-20211009115738765](../imgs/image-20211009115738765.png)

#### 查看最新监控数据

- 注意等待一会

![image-20211009115844587](../imgs/image-20211009115844587.png)

## 发送告警

- 默认情况下，监控项不会自动发送告警消息
- 需要配置触发器与告警配置，并且设置通知方式以及联系人才能发出告警消息

- 触发器：例如硬盘空间不足20%时、例如系统用户数超过30人时，会触发某一条件，比如达到了告警条件，将会执行某个动作
- 动作：触发器条件达到之后要采取的动作，如发邮件或执行命令

### 当系统用户数超过35时，发送邮件告警

##### 创建触发器规则

![image-20211009141857021](../imgs/image-20211009141857021.png)

![image-20211009141937098](../imgs/image-20211009141937098.png)

![image-20211009142113266](../imgs/image-20211009142113266.png)

![image-20211009142224765](../imgs/image-20211009142224765.png)

添加后表达式的结果如下：

![image-20211009142329729](../imgs/image-20211009142329729.png)

- 表达式语法：

- 创建触发器时需要定义表达式，触发器表达式（Expression）是触发异常的条件，触发器表达式格式如下：

  `{<server>:<key>.<function>(<parameter>)}<operator><constant>`

  {主机：key.函数(参数)}<表达式>常数

我们的例子中`{count.line.passwd:count.line.passwd.last()}>35`中冒号左边的count.line.passwd是模板名，冒号右边的count.line.passwd是自定义的key值，last()是zabbix自带的函数，表示最近一次取值。`>35`指的是最近一次取值大于35。

- 其他用法举例如下：
  - `{web1:system.cpu.load[all,avg1].last(0)}>5`			#0为最新数据

如果web1主机最新的CPU平均负载值大于5，则触发器状态Problem

- `{vfs.fs.size[/,free].max(5m)}<10G`					#5m为最近5分钟

根分区，最近5分钟的最大容量小于10G，则状态进入Problem

- `{vfs.file.cksum[/etc/passwd].diff(0)}>0`				#0为最新数据

最新一次校验/etc/passwd如果与上一次有变化，则状态进入Problem

- 触发器一旦满足条件，将会出现problem(问题)状态

##### 创建邮件报警媒介类型

![image-20211009143748657](../imgs/image-20211009143748657.png)

![image-20211009144132609](../imgs/image-20211009144132609.png)

##### 为用户关联邮件报警

![image-20211009144729491](../imgs/image-20211009144729491.png)

![image-20211009144820740](../imgs/image-20211009144820740.png)

![image-20211009144955348](../imgs/image-20211009144955348.png)

##### 创建动作

- 当出现Problem状态时，给admin发邮件

![image-20211009151612465](../imgs/image-20211009151612465.png)

![image-20211009152002883](../imgs/image-20211009152002883.png)

![image-20211009152241662](../imgs/image-20211009152241662.png)

![image-20211009152452328](../imgs/image-20211009152452328.png)

##### 配置zabbixserver成为邮件服务器

```shell
[root@zabbixserver ~]# yum install -y postfix mailx
[root@zabbixserver ~]# systemctl enable postfix --now
```

##### 验证

```shell
# 在web1上创建用户，使总用户数超过35
[root@web1 ~]# for user in user{1..20}
> do
> useradd $user
> done
```

![image-20211009154251273](../imgs/image-20211009154251273.png)

![image-20211009154404010](../imgs/image-20211009154404010.png)

![image-20211009154512197](../imgs/image-20211009154512197.png)

```shell
# 在zabbixserver上查看邮件
[root@zabbixserver ~]# mail
>N  1 root@localhost.local  Sat Oct  9 15:41  21/915   
 N  2 root@localhost.local  Sat Oct  9 15:42  21/915   
 N  3 root@localhost.local  Sat Oct  9 15:43  21/915   
 N  4 root@localhost.local  Sat Oct  9 15:44  21/915   
 N  5 root@localhost.local  Sat Oct  9 15:45  21/915   
& 5    # 这里写5回车，表示查看第5封邮件
Message  5:
From root@localhost.localdomain  Sat Oct  9 15:45:18 202
1
Return-Path: <root@localhost.localdomain>
X-Original-To: root@localhost
Delivered-To: root@localhost.localdomain
From: <root@localhost.localdomain>
To: <root@localhost.localdomain>
Date: Sat, 09 Oct 2021 15:45:18 +0800
Subject: Problem: password_line_gt_35
Content-Type: text/plain; charset="UTF-8"
Status: R

2021.10.09 /  15:41:15 出现问题了
主机： web1
Problem started at 15:41:15 on 2021.10.09
... ...
& q     # q回车，退出
```

## 配置自动发现

- 配置zabbix server不轮询被控端
- 被控端一旦出现问题，主动向zabbix server汇报
- 自动发现流程：
  - 创建自动发现规则
  - 创建动作，当主机被发现后，执行什么操作
  - 通过动作，添加主机，将模板应用到发现的主机

#### 配置自动发现

- 创建自动发现规则

![image-20211009163820665](../imgs/image-20211009163820665.png)

![image-20211009164038717](../imgs/image-20211009164038717.png)

- 创建动作

![image-20211009164529777](../imgs/image-20211009164529777.png)

![image-20211009164634515](../imgs/image-20211009164634515.png)

![image-20211009164738033](../imgs/image-20211009164738033.png)

![image-20211009164857584](../imgs/image-20211009164857584.png)

- 配置web2，启用zabbix agentd

```shell
[root@web1 ~]# scp zabbix-3.4.4.tar.gz 192.168.4.200:/root/
[root@web2 ~]# yum install -y gcc pcre-devel autoconf
[root@web2 ~]# tar xf zabbix-3.4.4.tar.gz 
[root@web2 ~]# cd zabbix-3.4.4/
[root@web2 zabbix-3.4.4]# ./configure --enable-agent && make && make install
[root@web2 zabbix-3.4.4]# useradd -s /sbin/nologin zabbix
[root@web2 zabbix-3.4.4]# vim /usr/local/etc/zabbix_agentd.conf
 69 EnableRemoteCommands=1
 93 Server=127.0.0.1,192.168.4.5
134 ServerActive=127.0.0.1,192.168.4.5
145 Hostname=web2
280 UnsafeUserParameters=1
[root@web2 zabbix-3.4.4]# vim /usr/lib/systemd/system/zabbix_agentd.service 
[Unit]
Description=zabbix agent
After=network.target remote-fs.target nss-lookup.target
[Service]
Type=forking
PIDFile=/tmp/zabbix_agentd.pid
ExecStart=/usr/local/sbin/zabbix_agentd
ExecStop=/bin/kill $MAINPID
[Install]
WantedBy=multi-user.target
[root@web2 zabbix-3.4.4]# systemctl enable zabbix_agentd.service --now
```

在zabbix web页面中查看web2是否发现。

![image-20211009170022854](../imgs/image-20211009170022854.png)

## 主动监控

- 默认zabbix采用的是被动监控，主动和被动都是对被监控端主机而言的！
- 被动监控：Server向Agent发起连接，索取监控数据。此种模式应用更多
- 主动监控：Agent向Server发起连接，Agent周期性地收集数据发送给Server。
- 区别：Server不用每次需要数据都连接Agent，Agent会自己收集数据并处理数据，Server仅需要保存数据即可。

### 实施主动监控

#### 配置web2使用主动监控

- 修改配置文件，启动服务

```shell
[root@web2 ~]# vim /usr/local/etc/zabbix_agentd.conf
 93 # Server=127.0.0.1,192.168.4.5   # 因为使用主动汇报的方式，所以不接受其他主机的查询
118 StartAgents=0    # 不接受被动检查，也不开启任何端口
134 ServerActive=192.168.4.5   # 主动向192.168.4.5汇报
145 Hostname=web2    # 此处必须与将来在web页面中名称一致
183 RefreshActiveChecks=120   # 120秒检查一次配置
264 Include=/usr/local/etc/zabbix_agentd.conf.d/
280 UnsafeUserParameters=1

# 重启服务
[root@web2 ~]# systemctl restart zabbix_agentd.service 
[root@web2 ~]# ss -tlnp | grep :10050   # 将不会出现10050端口
```

- 创建用于主动监控的模板。可以基于现在模板，完全克隆一份后，进行修改。

![image-20211009174454344](../imgs/image-20211009174454344.png)

![image-20211009174527405](../imgs/image-20211009174527405.png)

![image-20211009174642418](../imgs/image-20211009174642418.png)

![image-20211009174721180](../imgs/image-20211009174721180.png)

- 修改模板的监控项为主动模式。在模板页，点击新建的模板

![image-20211009175034682](../imgs/image-20211009175034682.png)

修改监控项为主动监控

![image-20211009175624108](../imgs/image-20211009175624108.png)

![image-20211009175143879](../imgs/image-20211009175143879.png)

![image-20211009175207955](../imgs/image-20211009175207955.png)

![image-20211009175256277](../imgs/image-20211009175256277.png)

![image-20211009175314253](../imgs/image-20211009175314253.png)

由于有三项不支持主动监控，所以把它们禁用

![image-20211009175426736](../imgs/image-20211009175426736.png)

- 添加监控主机，使用主动监控模板

![image-20211009175936684](../imgs/image-20211009175936684.png)

![image-20211009180109687](../imgs/image-20211009180109687.png)

![image-20211009180207488](../imgs/image-20211009180207488.png)

- 查看最新数据

![image-20211009180511569](../imgs/image-20211009180511569.png)





