# day03

[toc]

翻译软件：https://cr5.198254.com/copytranslatorlinux.zip

## zabbix拓扑图

- 编辑现有拓扑图

![image-20211011093108735](../imgs/image-20211011093108735.png)

![image-20211011093138268](../imgs/image-20211011093138268.png)

添加元素：

- 图标：可以是一个装饰图形，也可以对应具体的设备
- 形状：长方形、圆形等
- 链接：连线，多个设备之间才能使用链接

##### 创建完整的拓扑图

- 添加交换机

![image-20211011093625759](../imgs/image-20211011093625759.png)

![image-20211011093738831](../imgs/image-20211011093738831.png)

- 添加连接：圈中两台设备，点 链接的添加

![image-20211011093924690](../imgs/image-20211011093924690.png)

- 添加主机

![image-20211011094401267](../imgs/image-20211011094401267.png)

![image-20211011094508613](../imgs/image-20211011094508613.png)

## 聚合图形

- 将常用的监控图形整合到一个页面

![image-20211011101231497](../imgs/image-20211011101231497.png)

![image-20211011101321401](../imgs/image-20211011101321401.png)

![image-20211011101405428](../imgs/image-20211011101405428.png)

![image-20211011101429254](../imgs/image-20211011101429254.png)

![image-20211011101525333](../imgs/image-20211011101525333.png)

点击更改后，添加希望出现的数据即可。

![image-20211011101621625](../imgs/image-20211011101621625.png)

![image-20211011101657170](../imgs/image-20211011101657170.png)

同样的方法，点击其他的“更改”

![image-20211011101923352](../imgs/image-20211011101923352.png)



## Prometheus

- Prometheus不光是一个监控服务器，它也是一个时序数据库TSDB。
- Prometheus还有专门的PQL，即Prometheus查询语言
- Prometheus采用go语言编写
- Prometheus主要用于监控容器数据，也可以监控常规主机
- Prometheus是一个框架，可以与其他组件完美结合

![image-20211011104321276](../imgs/image-20211011104321276.png)

### 部署监控服务器

- 安装Prometheus

```shell
[root@zzgrhel8 ~]# scp /linux-soft/2/prometheus_soft.tar.gz 192.168.4.10:/root/

# Prometheus采用go语言编写，已经编译完成，解压即可
[root@prometheus ~]# tar xf prometheus_soft.tar.gz 
[root@prometheus ~]# ls prometheus_soft
[root@prometheus ~]# cd prometheus_soft/
[root@prometheus prometheus_soft]# tar xf prometheus-2.17.2.linux-386.tar.gz 
[root@prometheus prometheus_soft]# mv prometheus-2.17.2.linux-386 /usr/local/prometheus
```

- 修改配置文件

```shell
# 将最后一行的localhost改成本机IP
[root@prometheus ~]# vim /usr/local/prometheus/prometheus.yml 
 29     - targets: ['192.168.4.10:9090']

# 检查语法
[root@prometheus ~]# /usr/local/prometheus/promtool check config /usr/local/prometheus/prometheus.yml
```

- 编写service文件，管理服务

```shell
[root@prometheus ~]# vim /usr/lib/systemd/system/prometheus.service
[Unit]
Description=Prometheus Monitoring System
After=network.target

[Service]
ExecStart=/usr/local/prometheus/prometheus \
  --config.file=/usr/local/prometheus/prometheus.yml \
  --storage.tsdb.path=/usr/local/prometheus/data/

[Install]
WantedBy=multi-user.target

[root@prometheus ~]# systemctl enable prometheus.service --now
[root@prometheus ~]# systemctl status prometheus.service --now
```

- 查看结果：访问http://192.168.4.10:9090

![image-20211011114013573](../imgs/image-20211011114013573.png)

被监控的对象称为target

![image-20211011114248512](../imgs/image-20211011114248512.png)

prometheus已经监控自己了：

![image-20211011114338135](../imgs/image-20211011114338135.png)

查看监控图形：

![image-20211011114748412](../imgs/image-20211011114748412.png)

添加需要查看的监控项

![image-20211011114947884](../imgs/image-20211011114947884.png)

![image-20211011115034507](../imgs/image-20211011115034507.png)

查看监控项的图形信息：

![image-20211011115124956](../imgs/image-20211011115124956.png)

![image-20211011115254041](../imgs/image-20211011115254041.png)

### 添加被监控端

- 监控方式：
  - 拉取：pull。监控端联系被监控端，采集数据
  - 推送：push。被监控端主动把数据发送给监控端

- 被监控端根据自身运行的服务，可以运行不同的exporter（被监控端安装的、可以与Prometheus通信，实现数据传递的软件）:https://prometheus.io/docs/instrumenting/exporters/

##### 部署通用的监控exporter

- node-exporter用于监控硬件和操作系统的常用指标
- exporter运行于被监控的主机，以服务形式存在

- 在node1[192.168.4.11]上部署node exporter

```shell
# 部署
[root@prometheus ~]# scp prometheus_soft.tar.gz 192.168.4.11:/root
[root@node1 ~]# tar xf prometheus_soft.tar.gz 
[root@node1 ~]# cd prometheus_soft/
[root@node1 prometheus_soft]# tar xf node_exporter-1.0.0-rc.0.linux-amd64.tar.gz 
[root@node1 prometheus_soft]# mv node_exporter-1.0.0-rc.0.linux-amd64 /usr/local/node_exporter

# 编写服务文件
[root@node1 prometheus_soft]# vim /usr/lib/systemd/system/node_exporter.service
[Unit]
Description=node_exporter
After=network.target

[Service]
Type=simple
ExecStart=/usr/local/node_exporter/node_exporter

[Install]
WantedBy=multi-user.target

[root@node1 ~]# systemctl enable node_exporter.service --now
[root@node1 ~]# ss -tlnp | grep :9100
LISTEN     0      128         :::9100                    :::*                   users:(("node_exporter",pid=1094,fd=3))
```

- 在Prometheus服务器上添加监控项

```shell
[root@prometheus ~]# vim /usr/local/prometheus/prometheus.yml 
# 在结尾追加以下三行，注意缩进
  - job_name: 'node1'      # 此处可以是任意自定义的值
    static_configs:
    - targets: ['192.168.4.11:9100']
[root@prometheus ~]# systemctl restart prometheus.service 
```

> 附：vim配置
>
> ```shell
> [root@prometheus ~]# vim ~/.vimrc
> set ai
> set et
> set ts=2
> ```

- 查看结果

![image-20211011144322719](../imgs/image-20211011144322719.png)

- 查看节点1分钟的平均负载

![image-20211011144650754](../imgs/image-20211011144650754.png)

![image-20211011144715784](../imgs/image-20211011144715784.png)

### grafana可视化

- grafana是一款开源的、基于web的可视化工具
- 展示方式：客户端图表、面板插件
- 数据源可以来自于各种源，如prometheus

#### 部署grafana

- 安装grafana，并启动服务

```shell
[root@prometheus ~]# cd prometheus_soft/
[root@prometheus prometheus_soft]# ls *.rpm
grafana-6.7.3-1.x86_64.rpm
[root@prometheus prometheus_soft]# yum install -y grafana-6.7.3-1.x86_64.rpm
[root@prometheus ~]# systemctl enable grafana-server.service --now
```

- 修改配置，使其对接prometheus

访问http://192.168.4.10:3000。初始用户名和密码都是admin。第一次登陆，要求改密码，我的密码修改为`tedu.cn`。

![image-20211011152230581](../imgs/image-20211011152230581.png)

![image-20211011153727629](../imgs/image-20211011153727629.png)

![image-20211011153814503](../imgs/image-20211011153814503.png)

![image-20211011153943404](../imgs/image-20211011153943404.png)

![image-20211011154000848](../imgs/image-20211011154000848.png)

- 导入模板文件（展示数据的不同主题风格）

![image-20211011154333024](../imgs/image-20211011154333024.png)

![image-20211011154416593](../imgs/image-20211011154416593.png)

![image-20211011154546439](../imgs/image-20211011154546439.png)

![image-20211011154629159](../imgs/image-20211011154629159.png)

- 查看结果

![image-20211011154659959](../imgs/image-20211011154659959.png)

### 展示node1的监控信息

- grafana模板下载：https://grafana.com/grafana/dashboards/

- 导入主机监控的模板

```shell
[root@prometheus ~]# scp prometheus_soft/*.json 192.168.4.254:/tmp/
```

![image-20211011161809562](../imgs/image-20211011161809562.png)

![image-20211011161841353](../imgs/image-20211011161841353.png)

![image-20211011162254949](../imgs/image-20211011162254949.png)

![image-20211011162415539](../imgs/image-20211011162415539.png)

## 监控mysql数据库

### 安装数据库

```shell
[root@node1 ~]# yum install -y mariadb-server
[root@node1 ~]# systemctl enable mariadb.service --now
```

### 在数据库服务器上安装mysql exporter

- mysql exporter需要连接数据库，所以要在mysql数据库中为exporter创建授权用户

```shell
[root@node1 ~]# mysql
MariaDB [(none)]> grant all on *.* to 'jerry'@'localhost' identified by '123';
```

- 配置exporter

```shell
[root@node1 ~]# cd prometheus_soft/
[root@node1 prometheus_soft]# tar xf mysqld_exporter-0.12.1.linux-amd64.tar.gz 
[root@node1 prometheus_soft]# mv mysqld_exporter-0.12.1.linux-amd64 /usr/local/mysqld_exporter
# 编写连接mysql服务器的数据文件
[root@node1 ~]# vim /usr/local/mysqld_exporter/.my.cnf
[client]
host=127.0.0.1
port=3306
user=jerry
password=123

# 创建service文件
[root@node1 ~]# vim /usr/lib/systemd/system/mysqld_exporter.service
[Unit]
Description=node_exporter
After=network.target

[Service]
ExecStart=/usr/local/mysqld_exporter/mysqld_exporter \
--config.my-cnf=/usr/local/mysqld_exporter/.my.cnf

[Install]
WantedBy=multi-user.target

[root@node1 ~]# systemctl enable mysqld_exporter.service --now
[root@node1 ~]# ss -tlnp | grep :9104
LISTEN     0      128         :::9104                    :::*                   users:(("mysqld_exporter",pid=1697,fd=3))
```

### 配置prometheus

```shell
[root@prometheus ~]# vim /usr/local/prometheus/prometheus.yml 
# 追加以下内容：
  - job_name: 'mysql'
    static_configs:
    - targets: ['192.168.4.11:9104']
[root@prometheus ~]# systemctl restart prometheus.service 
[root@prometheus ~]# systemctl status prometheus.service 
```

查看状态：

![image-20211011174259410](../imgs/image-20211011174259410.png)

![image-20211011174319770](../imgs/image-20211011174319770.png)

### 导入grafana模板

![image-20211011175013018](../imgs/image-20211011175013018.png)

![image-20211011175033194](../imgs/image-20211011175033194.png)

![image-20211011175146938](../imgs/image-20211011175146938.png)

![image-20211011175430016](../imgs/image-20211011175430016.png)

### 模板切换

查看其他模板的方法：

![image-20211011175506407](../imgs/image-20211011175506407.png)

![image-20211011175526482](../imgs/image-20211011175526482.png)



