# day06

## playbook与python数据类型的对应关系

```yaml
---
- name: test network
  hosts: all
  tasks:
    - name: use ping to test network
      ping:
```

```python
[
	{
		'name': 'test network',
		'hosts': 'all',
		'tasks': [
			{
				'name': 'use ping',
				'ping':
			}
		]
	}
]
```

## playbook实战

- 在webserver上安装httpd、php和php-mysqlnd，并启动httpd服务；在database上安装mariadb-server并启动mariadb服务

```shell
[root@control ansible]# vim lnmp.yml
---
- name: configure webserver
  hosts: webserver
  tasks:
    - name: install httpd pkgs
      yum:
        name:
          - httpd
          - php
          - php-mysqlnd
        state: present

    - name: start httpd service
      service:
        name: httpd
        state: started
        enabled: yes

- name: configure database
  hosts: database
  tasks:
    - name: install mariadb pkgs
      yum:
        name: mariadb-server
        state: latest

    - name: start mariadb service
      service:
        name: mariadb
        state: started
        enabled: yes

# 执行语法检查
[root@control ansible]# ansible-playbook lnmp.yml --syntax-check
```

- 编写test_john.yml，要求创建用户johnd，它的uid是1040，主组是daemon，密码为123

```shell
[root@control ansible]# vim test_john.yml
---
- name: create user johnd
  hosts: webserver
  tasks:
    - name: add user johnd
      user:
        name: johnd
        uid: 1040
        group: daemon
        password: "{{ '123' | password_hash('sha512') }}"

[root@control ansible]# ansible-playbook test_john.yml --syntax-check
[root@control ansible]# ansible-playbook test_john.yml 
```

- 编写user_james.yml，在webserver上创建用户james，它的登陆shell是/bin/bash，属于bin和adm组，密码是123

```shell
[root@control ansible]# vim user_james.yml
---
- name: add user james
  hosts: webserver
  tasks:
    - name: create user james
      user:
        name: james
        shell: /bin/bash
        groups: bin, adm
        password: "{{ '123' | password_hash('sha512') }}"

[root@control ansible]# ansible-playbook user_james.yml --syntax-check
[root@control ansible]# ansible-playbook user_james.yml 
```

- 在webserver上删除用户johnd

```shell
[root@control ansible]# vim del_john.yml
---
- name: delete user johnd
  hosts: webserver
  tasks:
    - name: delete johnd
      user:
        name: johnd
        state: absent

[root@control ansible]# ansible-playbook del_john.yml --syntax-check
[root@control ansible]# ansible-playbook del_john.yml 
```

- 删除test的分区

```shell
[root@node1 ~]# fdisk /dev/vdb
Command (m for help): d
Partition number (1,2, default 2): 回车
Command (m for help): d
Command (m for help): p
Command (m for help): w
[root@node1 ~]# lsblk 
```

- 磁盘分区格式：
  - mbr: 主引导记录。最多建立4个主分区，或3主加一个扩展分区。扩展分区不直接使用，在它上面建立逻辑驱动器。最多支持2TB左右的硬盘
  - gpt：GUID分区表。最多建立128个主分区。支持大硬盘
- 在test主机上，对vdb进行分区，一个为3GB，一个为5GB

```shell
[root@control ansible]# vim disks.yml
---
- name: create partitions
  hosts: test
  tasks:
    - name: add 3GiB partition
      parted:
        device: /dev/vdb
        number: 1
        state: present
        part_end: 3GiB

    - name: add 5GiB partition
      parted:
        device: /dev/vdb
        number: 2
        state: present
        part_start: 3GiB
        part_end: 8GiB

[root@control ansible]# ansible-playbook disks.yml --syntax-check
[root@control ansible]# ansible-playbook disks.yml 
```

- 将上一步的vdb1/vdb2组合成名为my_vg的卷组

```shell
[root@control ansible]# vim disks.yml
... ...
    - name: create my_vg
      lvg:
        vg: my_vg
        pvs: /dev/vdb1,/dev/vdb2

[root@control ansible]# ansible-playbook disks.yml --syntax-check
[root@control ansible]# ansible-playbook disks.yml 
```

- 在上面的my_vg基础上创建1GB的逻辑卷，名为my_lv

```shell
[root@control ansible]# vim disks.yml
... ...
    - name: create my_lv
      lvol:
        vg: my_vg
        lv: my_lv
        size: 1G

[root@control ansible]# ansible-playbook disks.yml --syntax-check
[root@control ansible]# ansible-playbook disks.yml 
```

- 把上一步创建的my_lv格式化为ext4格式，并挂载到/data

```shell
[root@control ansible]# vim disks.yml
... ...
    - name: mkfs my_lv
      filesystem:
        fstype: ext4
        dev: /dev/my_vg/my_lv

    - name: mkdir /data
      file:
        path: /data
        mode: '0755'
        state: directory

    - name: mount my_lv
      mount:
        path: /data
        src: /dev/my_vg/my_lv
        fstype: ext4
        state: mounted

[root@control ansible]# ansible-playbook disks.yml --syntax-check
[root@control ansible]# ansible-playbook disks.yml
```

- 以上全部过程的yaml文件如下：

```shell
[root@control ansible]# vim disks.yml 
---
- name: create partitions
  hosts: test
  tasks:
    - name: add 3GiB partition
      parted:
        device: /dev/vdb
        number: 1
        state: present
        part_end: 3GiB

    - name: add 5GiB partition
      parted:
        device: /dev/vdb
        number: 2
        state: present
        part_start: 3GiB
        part_end: 8GiB

    - name: create my_vg
      lvg:
        vg: my_vg
        pvs: /dev/vdb1,/dev/vdb2

    - name: create my_lv
      lvol:
        vg: my_vg
        lv: my_lv
        size: 1G

    - name: mkfs my_lv
      filesystem:
        fstype: ext4
        dev: /dev/my_vg/my_lv

    - name: mkdir /data
      file:
        path: /data
        mode: '0755'
        state: directory

    - name: mount my_lv
      mount:
        path: /data
        src: /dev/my_vg/my_lv
        fstype: ext4
        state: mounted
```

- 安装rpm包组Development Tools，并且升级系统

```shell
[root@node1 ~]# yum groupinstall "Development Tools"

[root@control ansible]# vim pkgs.yml
---
- name: rpms manage
  hosts: webserver
  tasks:
    - name: install rpm group
      yum:
        name: "@Development Tools"
        state: present
        
    - name: update system
      yum:
        name: '*'
        state: latest

[root@control ansible]# ansible-playbook pkgs.yml --syntax-check
[root@control ansible]# ansible-playbook pkgs.yml 
```

## setup模块

- 用于收集远程主机预定义的变量(facts量、事实变量)
- 常用于收集的变量有：
  - ansible_all_ipv4_addresses：所有的ipv4地址
  - ansible_bios_version：硬件版本号
  - ansible_memtotal_mb：内存大小
  - ansible_hostname：主机名
  - ansible_devices：设备，如硬盘等

```shell
# 查看全部的facts变量
[root@control ansible]# ansible test -m setup

# 只查看ip地址
[root@control ansible]# ansible test -m setup -a "filter=ansible_all_ipv4_addresses"
# 只查看内存大小
[root@control ansible]# ansible test -m setup -a "filter=ansible_memtotal_mb"
```

## debug模块

- 用于显示调试信息

```shell
[root@control ansible]# vim debug.yml
---
- name: display ansible facts
  hosts: test
  tasks:
    - name: display facts vars
      debug:
        msg: "hostname: {{ ansible_hostname }} with {{ ansible_memtotal_mb }}MB memory"

[root@control ansible]# ansible-playbook debug.yml --syntax-check
[root@control ansible]# ansible-playbook debug.yml 
```

## 变量

- 为了灵活，可以使用变量来代替经常变化的内容

### 主机清单变量

```shell
[root@control ansible]# vim inventory 
[test]
node1 iname="nb"    # 只对node1有效的变量

[webserver]
node[3:4]
... ...
[webserver:vars]
iname="dachui"      # 对webserver中所有主机有效的变量

# 使用变量iname创建用户
[root@control ansible]# vim vars_name.yml
---
- name: create users
  hosts: node1, webserver
  tasks:
    - name: add user
      user:
        name: "{{ iname }}"
        state: present

[root@control ansible]# ansible-playbook vars_name.yml 
```

### facts事实变量

- 通过setup模块自动收集的变量

### playbook变量

- 在playbook中定义的变量

```shell
[root@control ansible]# vim pb_vars.yml
---
- name: create user
  hosts: test
  vars:
    uname: zhangsan
    upass: '123456'
  tasks:
    - name: add user and password
      user:
        name: "{{ uname }}"
        password: "{{ upass | password_hash('sha512') }}"

[root@control ansible]# ansible-playbook pb_vars.yml
```

### 变量文件

- 可以将变量单独的放在一个文件中

```shell
[root@control ansible]# vim vars.yml
---
yonghu: lisi
mima: '123456'

[root@control ansible]# vim file_var.yml
---
- name: create user
  hosts: test
  vars_files: vars.yml
  tasks:
    - name: add user
      user:
        name: "{{ yonghu }}"
        password: "{{ mima | password_hash('sha512') }}"
        
[root@control ansible]# ansible-playbook file_var.yml
```

## 其他模块

- firewalld模块：用于配置防火墙

```shell
[root@control ansible]# vim firewall.yml
---
- name: configure firewall
  hosts: test
  tasks:
    - name: install firewalld
      yum:
        name: firewalld
        state: present

    - name: run firewalld
      service:
        name: firewalld
        state: started
        enabled: yes

    - name: set firewall rules
      firewalld:
        port: 80/tcp       # 开放哪个端口
        permanent: yes     # 永久生效，但不会立即生效
        immediate: yes     # 立即生效
        state: enabled     # 启用该规则

[root@control ansible]# ansible-playbook firewall.yml
[root@node1 ~]# firewall-cmd --list-all
```

> 附：服务和端口号的对应关系：/etc/services

- template模块：lineinfile等模块只能改文件中一行数据，改很多数据时吃力。使用template模块可以在文件中使用变量。

```shell
# 创建一个模板文件
[root@control ansible]# mkdir template
[root@control ansible]# vim template/index.j2
Welcome to {{ ansible_hostname }} on {{ ansible_eth0.ipv4.address }}

# 将模板文件上传到webserver中
[root@control ansible]# vim templ.yml
---
- name: upload index
  hosts: webserver
  tasks:
    - name: create index web page
      template:
        src: template/index.j2
        dest: /var/www/html/index.html

[root@control ansible]# ansible-playbook templ.yml
[root@node3 ~]# cat /var/www/html/index.html 
Welcome to node3 on 192.168.4.13
[root@node4 ~]# cat /var/www/html/index.html 
Welcome to node4 on 192.168.4.14
```

