## day07

## playbook格式

```yaml
---
- name: 第一个play
  hosts: 主机组1, 主机1
  tasks:
    - name: 任务一
      模块1:
        参数1: 值1
        参数2: 值2
    
    - name: 任务二
      模块2:
        参数1: 值1
        参数2: 值2

- name: 第二个play

- name: 第三个play
```

## `>`和`|`的含义

- `>`表示多行合并为一行

```shell
[root@control ansible]# vim myfile.yml
---
- name: make file
  hosts: test
  tasks:
    - name: create file
      copy:
        dest: /tmp/tmpfile.txt
        content: >
          Hello World!
          Ni hao!
          Chi le ma?

[root@control ansible]# ansible-playbook myfile.yml
[root@node1 ~]# cat /tmp/tmpfile.txt 
Hello World! Ni hao! Chi le ma?
```

- `|`保留多行

```shell
[root@control ansible]# cp myfile.yml myfile2.yml
[root@control ansible]# vim myfile2.yml
---
- name: make file
  hosts: test
  tasks:
    - name: create file
      copy:
        dest: /tmp/tmpfile2.txt
        content: |
          Hello World!
          Ni hao!
          Chi le ma?

[root@control ansible]# ansible-playbook myfile2.yml
[root@node1 ~]# cat /tmp/tmpfile2.txt 
Hello World!
Ni hao!
Chi le ma?
```

## 错误处理机制

- 默认情况下，当playbook遇到错误时，它将崩溃，终止执行

```shell
[root@control ansible]# vim myerr.yml
---
- name: my errors
  hosts: test
  tasks:
    - name: start httpd service
      service:
        name: http
        state: started
        enabled: yes
    
    - name: touch a file
      file:
        path: /tmp/service.txt
        state: touch

[root@control ansible]# ansible-playbook myerr.yml --syntax-check
# 因为没有http服务，所以程序崩溃，第2个任务不会执行
[root@control ansible]# ansible-playbook myerr.yml 
```

- 通过`ignore_errors`可以忽略错误，继续执行

```shell
[root@control ansible]# vim myerr.yml
---
- name: my errors
  hosts: test
  tasks:
    - name: start httpd service
      service:
        name: http
        state: started
        enabled: yes
      ignore_errors: yes

    - name: touch a file
      file:
        path: /tmp/service.txt
        state: touch

[root@control ansible]# ansible-playbook myerr.yml --syntax-check
# 第一个任务不能执行，报错，但是第2个任务仍然可以执行
[root@control ansible]# ansible-playbook myerr.yml 
```

- 如果Playbook中所有的任务都是独立的，无论哪个出错，都可以继续，可以在playbook全局设置

```shell
[root@control ansible]# vim myerr.yml
---
- name: my errors
  hosts: test
  ignore_errors: yes
  tasks:
    - name: start httpd service
      service:
        name: http
        state: started
        enabled: yes

    - name: touch a file
      file:
        path: /tmp/service.txt
        state: touch

[root@control ansible]# ansible-playbook myerr.yml 
```

## 触发执行任务

- 可以通过handlers定义任务
- handlers中定义的任务不是一定要执行的
- 在tasks中的任务可以通过notify来指定要执行的任务
- 只有tasks中的任务状态是changed才会进行通知

```shell
[root@control ansible]# vim handlers.yml
---
- name: handler tasks
  hosts: test
  tasks:
    - name: create a directory
      file:
        path: /tmp/mydemo
        state: directory
      notify: touch file
      
  handlers:
    - name: touch file
      file:
        path: /tmp/mydemo/myfile
        state: touch

# 第一次运行playbook，两个任务都会执行
[root@control ansible]# ansible-playbook handlers.yml
# 第二次运行playbook，touch file任务不会执行
[root@control ansible]# ansible-playbook handlers.yml
```

## when条件

- 只有满足某一条件时，才执行任务
- 常见的操作符有：
  - `==`： 相等
  - `!=`：不等
  - `>`：大于
  - `<`： 小于
  - `<=`：小于等于
  - `>=`：大于等于

- 多个条件可以用and或or连接
- when表达式中的变量，不需要`{{}}`

- 当test主机的内存大于2G的时候，才安装mariadb-server

```shell
[root@control ansible]# vim when1.yml
---
- name: install mariadb
  hosts: test
  tasks:
    - name: install db pkgs
      yum:
        name: mariadb-server
        state: present
      when: ansible_memtotal_mb > 2048

# 如果test主机的内存不足2GB，那么将会跳过安装(skipping)
[root@control ansible]# ansible-playbook when1.yml
```

- 系统的发行版是RedHat 8才执行任务

```shell
[root@control ansible]# ansible test -m setup | grep distribution
[root@control ansible]# vim when2.yml
---
- name: when condition
  hosts: test
  tasks:
    - name: touch a file
      file:
        path: /tmp/when.txt
        state: touch
      when: >
        ansible_distribution == "RedHat"
        and
        ansible_distribution_major_version == "8"

[root@control ansible]# ansible-playbook when2.yml
```

## 任务块

### block

- 可以将多个任务通过block组合为一个组
- 可以将整个组一起控制是否要执行

- 如果发行版是RedHat，则安装并启动httpd

```shell
[root@control ansible]# vim block1.yml
---
- name: block tasks
  hosts: test
  tasks:
    - name: define a group of tasks
      block:
        - name: install httpd
          yum:
            name: httpd
            state: present

        - name: start httpd
          service:
            name: httpd
            state: started
            enabled: yes
      when: ansible_distribution == "RedHat"

[root@control ansible]# ansible-playbook  block1.yml
```

### rescue和always

- block和rescue和always联用的时候
  - block中的任务成功，rescue中的任务不执行
  - block中的任务失败，rescue中的任务执行
  - block中的任务不管成功还是失败，always中的任务总是执行

```shell
[root@control ansible]# vim block2.yml
---
- name: block control
  hosts: test
  tasks:
    - block:
        - name: touch a file
          file:
            name: /tmp/test1.txt
            state: touch
      rescue:
        - name: touch a file test2.txt
          file:
            name: /tmp/test2.txt
            state: touch
      always:
        - name: touch file test3.txt
          file:
            name: /tmp/test3.txt
            state: touch

# 因为block没有错误，所以rescue不会执行
[root@control ansible]# ansible-playbook block2.yml
```

```shell
[root@control ansible]# vim block2.yml
---
- name: block control
  hosts: test
  tasks:
    - block:
        - name: touch a file
          file:
            name: /tmp/test111.txt
            state: file
      rescue:
        - name: touch a file test2.txt
          file:
            name: /tmp/test2.txt
            state: touch
      always:
        - name: touch file test3.txt
          file:
            name: /tmp/test3.txt
            state: touch

# 因为无法创建test111.txt，所以block报错，rescue将会执行
[root@control ansible]# ansible-playbook block2.yml
```

## loop循环

- ansible中的loop相当于for循环
- 循环时的变量名是固定的，叫item

- 创建5个目录

```shell
[root@control ansible]# vim loop.yml
---
- name: use loop
  hosts: test
  tasks:
    - name: create directory
      file:
        path: /tmp/demo/{{ item }}
        state: directory
      loop:
        - aaa
        - bbb
        - ccc
        - ddd
        - eee

[root@control ansible]# ansible-playbook loop.yml
```

> 附：vim中批量添加注释
>
> 1. 可视模式：光标定位在需注释的第一行，按ctrl + v，然后按上下箭头先中多行，接着按大I，输入`# `，最后按ESC
> 2. 查找替换：`:起始行,结束行s/要替换的内容/目标数据/`
>
> 如：把6到9行加注释 `:6,9s/^/# /`

- 创建用户，设置密码

```shell
[root@control ansible]# vim loop_user.yml
---
- name: create users
  hosts: test
  tasks:
    - name: create multiple users
      user:
        name: "{{ item.uname }}"
        password: "{{ item.upass | password_hash('sha512') }}"
      loop:
        - {"uname": "wangwu", "upass": "123"}
        - {"uname": "zhaoliu", "upass": "456"}

[root@control ansible]# ansible-playbook loop_user.yml
```

## role角色

- 为了实现playbook的重用，可以使用role角色
- 角色role相当于是把任务打散，放到不同的文件中
- 再把固定的一些值，如用户名、服务、软件包名等用变量来表示
- 调用role的时候，只要改变变量名就行了

```shell
# 创建角色
[root@control ansible]# mkdir roles
[root@control ansible]# cd roles/
# 创建mytest角色目录，目录名就是角色名
[root@control roles]# ansible-galaxy init mytest
[root@control roles]# yum install -y tree
[root@control roles]# tree mytest/
mytest/
|-- README.md         # 对该角色的说明
|-- defaults          # 定义变量的缺省值，优先级最低
|   `-- main.yml
|-- files             # 用于保存文件
|-- handlers          # 保存handlers中的任务
|   `-- main.yml
|-- meta              # 元数据、如作者的名字及联系方式等
|   `-- main.yml
|-- tasks             # 定义任务
|   `-- main.yml
|-- templates         # 保存模板文件的位置
|-- tests             # 用于保存测试该角色的playbook和主机清单文件
|   |-- inventory
|   `-- test.yml
`-- vars              # 用于保存变量，建议变量写在此处
    `-- main.yml
```

- 创建playbook，用于修改/etc/motd

```shell
[root@control ansible]# vim template/motd.j2
System Hostname: {{ ansible_hostname }}
Current Date: {{ ansible_date_time.date }}
Contact To {{ admin }}

[root@control ansible]# vim motd.yml
---
- name: modify /etc/motd
  hosts: test
  vars:
  	admin: root@tedu.cn
  tasks:
    - name: modify motd
      template:
        src: template/motd.j2
        dest: /etc/motd

[root@control ansible]# ansible-playbook motd.yml
```

- 创建playbook，使用role修改/etc/motd

```shell
# 1. 创建名为motd的role
[root@control ansible]# ansible-galaxy init roles/motd

# 2. 在角色的templates目录下创建模板文件
[root@control ansible]# vim roles/motd/templates/motd.j2
System Hostname: {{ ansible_hostname }}
Current Date: {{ ansible_date_time.date }}
Contact To {{ admin }}

# 3. 创建admin变量
[root@control ansible]# vim roles/motd/vars/main.yml 
---
# vars file for roles/motd
admin: zzg@tedu.cn

# 4. 创建任务
[root@control ansible]# vim roles/motd/tasks/main.yml 
---
# tasks file for roles/motd
- name: modify motd
  template:
    src: motd.j2
    dest: /etc/motd

# 5. 在ansible.cfg中声明角色目录
[root@control ansible]# vim ansible.cfg 
[defaults]
inventory = ./inventory
host_key_checking = False
roles_path = ./roles

# 6. 创建playbook，引用role
[root@control ansible]# vim role_motd.yml
---
- name: modify motd with role
  hosts: test
  roles:
    - motd

# 7. 执行playbook
[root@control ansible]# ansible-playbook role_motd.yml
```

- ansible公共仓库：https://galaxy.ansible.com/

```shell
# 在公共仓库中搜索角色
[root@zzgrhel8 myansible]# ansible-galaxy search httpd
# 下载搜索到的角色到当前目录的roles中
[root@zzgrhel8 myansible]# ansible-galaxy install acandid.httpd -p roles/
```

## ansible vaults

- 用于文件加解密

```shell
[root@control ansible]# vim hello.txt
meet you at xiagu tonight.

# 加密
[root@control ansible]# ansible-vault encrypt hello.txt
New Vault password: 123
Confirm New Vault password: 123
Encryption successful
[root@control ansible]# cat hello.txt   # 已加密

# 解密
[root@control ansible]# ansible-vault decrypt hello.txt  
Vault password: 123
[root@control ansible]# cat hello.txt 
meet you at xiagu tonight.

# 加密后更改密码
[root@control ansible]# ansible-vault encrypt hello.txt
New Vault password: 123
Confirm New Vault password: 123
# 更改密码
[root@control ansible]# ansible-vault rekey hello.txt
Vault password: 123
New Vault password: abc
Confirm New Vault password: abc
# 不解密，查看
[root@control ansible]# ansible-vault view hello.txt 
Vault password: abc

# 使用密码文件加解密
[root@control ansible]# echo 'tedu.cn' > pass.txt
[root@control ansible]# echo 'hello world' > data.txt
# 使用pass.txt中密码加解密data.txt
[root@control ansible]# ansible-vault encrypt --vault-id=pass.txt data.txt 
[root@control ansible]# cat data.txt 
[root@control ansible]# ansible-vault decrypt --vault-id=pass.txt data.txt 
Decryption successful
[root@control ansible]# cat data.txt 
hello world
```

## sudo

- 一般用于普通用户执行需要root权限的命令
- 普通用于配置为可以使用sudo的方式执行管理员命令

```shell
[root@node1 ~]# visudo     # 大概在101行的位置
# 中间的两个ALL不用管，第三个ALL表示全部的命令
... ...
root    ALL=(ALL)       ALL     
zhangsan        ALL=(ALL)       ALL
... ...

[root@node1 ~]# su - zhangsan
[zhangsan@node1 ~]$ passwd lisi    # 不能执行
passwd: Only root can specify a user name.
[zhangsan@node1 ~]$ sudo passwd lisi
[sudo] password for zhangsan:      # 输入张三的密码


# lisi不用输入密码即可执行所有命令
[root@node1 ~]# visudo
... ...
root    ALL=(ALL)       ALL
zhangsan        ALL=(ALL)       ALL
lisi    ALL=(ALL)       NOPASSWD: ALL
... ...

[root@node1 ~]# su - lisi
[lisi@node1 ~]$ useradd marry
useradd: Permission denied.
useradd: cannot lock /etc/passwd; try again later.
[lisi@node1 ~]$ sudo useradd marry
```

## 了解sudo的使用

- 在远程主机上创建lisi并设置它可以用sudo执行任何命令

```shell
# 在被管端创建lisi
[root@node1 ~]# useradd lisi
[root@node1 ~]# echo a | passwd --stdin lisi
[root@node1 ~]# visudo 
lisi    ALL=(ALL)       NOPASSWD: ALL

# 管理端配置
[root@control ~]# mkdir myansible
[root@control ~]# cd myansible/
[root@control myansible]# vim ansible.cfg
[defaults]
inventory = hosts           # 主清清单文件是hosts
remote_user = lisi          # 用lisi以ssh登陆到远程主机

[privilege_escalation]      # 提权配置
become=True                 # 要切换用户
become_method=sudo          # 以sudo的方式切换账号执行命令
become_user=root            # 切换成root
become_ask_pass=False       # 切换用户不需要输入密码

[root@control myansible]# vim hosts
[group1]
node1

# 配置以lisi登陆可以免密
[root@control myansible]# ssh-copy-id lisi@node1
lisi@node1's password: a
# 执行ansible命令
[root@control myansible]# ansible all -m ping
```

## 特殊的inventory变量

- ansible_ssh_user：登陆远程主机的用户名
- ansible_ssh_pass：登陆远程主机的密码
- ansible_ssh_port：远程主机的端口号

```shell
# 修改远程主机的ssh端口号
[root@node3 ~]# vim /etc/ssh/sshd_config 
... ...
Port 220
... ...
[root@node3 ~]# systemctl restart sshd
[root@zzgrhel8 ~]# ssh 192.168.4.13 -p220   # 登陆时需指定端口

# 失败的ping
[root@control ansible]# ansible node3 -m ping
[root@control ansible]# vim inventory 
... ...
[webserver]
node3 ansible_ssh_port=220
node4

... ...
[root@control ansible]# ansible node3 -m ping   # 成功
```







