## day05

[toc]

## ad-hoc模式

```shell
# 查看版本及所用的配置文件，在不同的目录下，观察配置文件的变化
[root@control ~]# ansible --version
[root@control ~]# cd ansible
[root@control ansible]# ansible --version

# 工作目录下的配置文件
[root@control ansible]# cat ansible.cfg 
[defaults]
inventory = ./inventory          # 被管理的主机清单
host_key_checking = False        # 忽略主机密钥检查

# 主机清单文件
[root@control ansible]# cat inventory 
[test]
node1

[proxy]
node2

[webserver]
node[3:4]

[database]
node5

[cluster:children]
webserver
database

# 注意：写在主机清单文件中的名字，必须可以解析为IP地址
[root@control ansible]# cat /etc/hosts
... ...
192.168.4.253	control
192.168.4.11	node1
192.168.4.12	node2
192.168.4.13	node3
192.168.4.14	node4
192.168.4.15	node5


# 查看cluster组中包含的主机
[root@control ansible]# ansible cluster --list-hosts
  hosts (3):
    node3
    node4
    node5
```

### ad-hoc语法

```shell
ansible 主机或组 -m 模块 -a 参数
```

- 检查网络连通性

```shell
[root@control ansible]# ansible all -m ping
```

### 常用模块

- 查看帮助的方法

```shell
# 列出全部模块
[root@control ansible]# ansible-doc -l

# 查看包含file的模块
[root@control ansible]# ansible-doc -l | grep file
```

- command和shell的区别：command不支持shell特性，如管道、重定向

```shell
[root@control ansible]# ansible test -m shell -a "mkdir /tmp/mydir"
```

- script：将复杂的指令写到脚本中，通过该模块在远程主机执行

```shell
[root@control ansible]# vim adduser.sh
#!/bin/bash

useradd tom
echo 123456 | passwd --stdin tom

[root@control ansible]# ansible test -m script -a "./adduser.sh"
[root@control ansible]# ansible test -m command -a "id tom"
```

- file模块：可以创建文件、目录、链接；修改权限和属性

```shell
# 查看file模块的帮助，回车向下翻一行，空格向下翻一屏，也可以按上下箭头、pageup/pagedown。大G跳到结尾，小g回到开头。搜索directory，输入/directory，按小n向下查找下一个，大N向上查找上一个。
[root@control ansible]# ansible-doc file
... ...
EXAMPLES:
# file是模块，写在ad-hoc命令的-m后面；下面缩进部分是参数，写在-a后面。参数中的key: value，要写为key=value的形式
- name: Change file ownership, group and permissions
  file:
    path: /etc/foo.conf
    owner: foo
    group: foo
    mode: '0644'
... ...

# 在test组中的主机上创建/tmp/myfile.txt，属主属组均为root，权限是644，状态是touch。touch表示没有则创建，有则更新时间戳。
[root@control ansible]# ansible test -m file -a "path=/tmp/myfile.txt owner=root group=root mode='0644' state=touch"

# stat可以查看文件的元数据（时间戳、权限等）
[root@control ansible]# stat /tmp/abc.txt 

# 创建目录，权限为755
[root@control ansible]# ansible test -m file -a "path=/tmp/mydir state=directory mode='0755'"

# 删除目录
[root@control ansible]# ansible test -m file -a "path=/tmp/mydir state=absent"
```

- copy模块：将文件从管理端上传到被控端

```shell
# 将本地管理员家目录下的a3.txt上传到test组中主机的/root目录
[root@control ansible]# echo AAA > ~/a3.txt
[root@control ansible]# ansible test -m copy -a "src=~/a3.txt dest=/root/"

# 使用copy模块在test组中主机的/tmp目录下创建newfile.txt文件，文件中内容为Hello World。
[root@control ansible]# ansible test -m copy -a "content='Hello World\n' dest=/tmp/newfile.txt"
```

- fetch：与copy相反，它将远程主机的上文件下载到管理端

```shell
# 将远程主机的/etc/hostname下载到当前用户的家目录
[root@control ansible]# ansible test -m fetch -a "src=/etc/hostname dest=~/"
[root@control ansible]# cat /root/node1/etc/hostname 
node1
```

- lineinfile：某一行文本一定要出现在文件中。如果没有，默认添加到文件尾部

```shell
# 远程主机的/etc/issue中一定要有Hello World
[root@control ansible]# ansible test -m lineinfile -a "path=/etc/issue line='Hello World'"

# 远程主机的/etc/issue中以Hello开头的行，换成ni hao
[root@control ansible]# ansible test -m lineinfile -a "path=/etc/issue regexp='^Hello' line='ni hao'"
```

- replace：lineinfile替换的是一整行，replace替换关键词

```shell
# 远程主机的/etc/issue中把行首的Hello，换成ni hao
[root@control ansible]# ansible test -m replace -a "path=/etc/issue regexp='^Hello' replace='ni hao'"

# 将远程主机/root/a3.txt的内容加注释。
# ^表示开头，$表示结尾，.+表示至少有一个任意字符。()表示把匹配的内容存储下来，将来\1是对匹配内容的引用
[root@control ansible]# ansible test -m replace -a "path=/root/a3.txt regexp='^(.+)$ replace='# \1'"
```

- user：实现对系统用户的管理
  - 用户的信息会写入到 /etc/passwd、 /etc/shadow、 /etc/group、 /etc/gshadow
  - 其中passwd有7个部分，分别是  ***用户名:密码占位符:uid:gid:注释:家目录:登陆shell***

```shell
# 创建用户user1
[root@control ansible]# ansible test -m user -a "name=user1"
或
[root@control ansible]# ansible test -m user -a "name=user1 state=present"

# 创建user2，要求他的UID是1010，主组是adm，附加组是daemon和root，家目录是/home/user_2
[root@control ansible]# ansible test -m user -a "name=user2 uid=1010 group=adm groups=daemon,root home=/home/user_2 state=present"

# 设置user2的密码为123456。密码通过sha512加密处理
[root@control ansible]# ansible test -m user -a "name=user2 password={{'123456'|password_hash('sha512')}}"
```

- yum_repository：配置yum客户端

```shell
[root@control ansible]# ansible test -m yum_repository -a "file=myrepo name=myyum description='myum repo' baseurl=ftp://192.168.4.254/centos gpgcheck=no enabled=yes"

[root@node1 ~]# cat /etc/yum.repos.d/myrepo.repo 
[myyum]
baseurl = ftp://192.168.4.254/centos
enabled = 1
gpgcheck = 0
name = myum repo


[root@control ansible]# ansible test -m yum_repository -a "file=rhel8 name=app baseurl=ftp://192.168.4.254/AppStream gpgcheck=no enabled=yes description=app"
[root@control ansible]# ansible test -m yum_repository -a "file=rhel8 name=BaseOS baseurl=ftp://192.168.4.254/BaseOS gpgcheck=no enabled=yes description=BaseOS" 

[root@node1 ~]# cat /etc/yum.repos.d/rhel8.repo 
[app]
baseurl=ftp://192.168.4.254/AppStream
enabled = 1
gpgcheck = 0
name = app

[BaseOS]
baseurl=ftp://192.168.4.254/BaseOS
enabled = 1
gpgcheck = 0
name = BaseOS
```

- yum：可以进行装包、升级、卸载
  - state: present   如果没有则安装，有则忽略
  - state: absent    如果有则卸载，没有则忽略
  - state: latest       如果没有则安装，有的话看是不是最新版本，不是最新的，则升级

```shell
# 安装httpd，要求是安装状态即可
[root@control ansible]# ansible test -m yum -a "name=httpd state=present"

# 安装php，要求是最新版本
[root@control ansible]# ansible test -m yum -a "name=php state=latest"

# 卸载php
[root@control ansible]# ansible test -m yum -a "name=php state=absent"

# 同时装多个包
[root@control ansible]# ansible test -m yum -a "name=httpd,php state=present"
```

- service：服务管理：起动、重启、停止
  - state: started   起服务
  - state: stopped   停服务
  - state: restarted  重启服务

```shell
# 启动httpd服务，并设置为开机自启
[root@control ansible]# ansible test -m service -a "name=httpd state=started enabled=yes"

# 关闭httpd服务，并设置为开机不启动
[root@control ansible]# ansible test -m service -a "name=httpd state=stopped enabled=no"
```

- 逻辑卷：普通分区大小固定，如果空间不足，不能扩容。逻辑卷是动态管理存储空间的一种方法。先将硬盘或分区转换成物理卷PV，再将一到多个PV组合成卷组VG，最后从VG上划分逻辑卷LV。LV就可以像普通的分区一样，进行格式化、挂载了。如果将来空间不足，LV可以在线扩容。

```shell
# 在node1上对新加硬盘分区
# 查看硬盘
[root@node1 ~]# lsblk 
[root@node1 ~]# fdisk /dev/vdb 
Command (m for help): n
Select (default p): 回车
Partition number (1-4, default 1): 回车
First sector (2048-41943039, default 2048): 回车
Last sector, +sectors or +size{K,M,G,T,P} (2048-41943039, default 41943039): +10G
Command (m for help): n
Select (default p): 回车
Partition number (2-4, default 2): 回车
First sector (20973568-41943039, default 20973568): 回车
Last sector, +sectors or +size{K,M,G,T,P} (20973568-41943039, default 41943039): 回车
Command (m for help): w
[root@node1 ~]# lsblk 
```

- lvg模块：创建、删除卷组

```shell
# 在目录主机上安装lvm2，以便支持逻辑卷特性
[root@control ansible]# ansible test -m yum -a "name=lvm2 state=present"

# 创建名为myvg的卷组，该卷组由vdb1组成
[root@control ansible]# ansible test -m lvg -a "vg=myvg pvs=/dev/vdb1"

# 扩展现有卷组myvg，把vdb2加入
[root@control ansible]# ansible test -m lvg -a "vg=myvg pvs=/dev/vdb1,/dev/vdb2"

# 在目标主机上查看结果
[root@node1 ~]# vgs
  VG   #PV #LV #SN Attr   VSize  VFree 
  myvg   2   0   0 wz--n- 19.99g 19.99g
```

- lvol：管理逻辑卷

```shell
# 在myvg卷组上，创建1G大小的逻辑卷，名为mylv
[root@control ansible]# ansible test -m lvol -a "vg=myvg lv=mylv size=1G"

# 扩容mylv到4GB
[root@control ansible]# ansible test -m lvol -a "vg=myvg lv=mylv size=4G"
```

- filesystem：格式化

```shell
# 格式化
[root@control ansible]# ansible test -m filesystem -a "fstype=ext4 dev=/dev/myvg/mylv"

# 删除lv
[root@control ansible]# ansible test -m lvol -a "lv=mylv vg=myvg state=absent force=yes"

# 删除vg
[root@control ansible]# ansible test -m lvg -a "vg=myvg state=absent"
```

### Playbook

- 设置vim的默认选项
  - 自动缩进
  - 按tab键跳2个空格
  - 将tab转换成空格

```shell
[root@control ansible]# vim ~/.vimrc
set ai
set ts=2
set et
```

- 第一个playbook

```shell
[root@control ansible]# ansible all -m ping
[root@control ansible]# vim mytest.yml
---
- name: test network
  hosts: all
  tasks:
    - name: use ping to test network
      ping:

[root@control ansible]# ansible-playbook mytest.yml
```

- 在test组和node2主机上创建/tmp/demo目录，权限755，将本机的/etc/hosts拷贝到目标主机的/tmp/demo中

```shell
[root@control ansible]# vim fileop.yml
---
- name: create dir and copy file
  hosts: test, node2
  tasks:
    - name: create dir
      file:
        path: /tmp/demo
        state: directory
        mode: '0755'

    - name: copy file
      copy:
        src: /etc/hosts
        dest: /tmp/demo/hosts

[root@control ansible]# ansible-playbook fileop.yml
```















