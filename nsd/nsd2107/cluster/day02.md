# day02

## KeepAlived

- 用于实现高可用集群
- 可以为服务器提供一个VIP

### 配置高可用web集群

```shell
# 在两台web服务器上，安装keepalived
[root@web1 ~]# yum install -y keepalived
[root@web2 ~]# yum install -y keepalived

# 修改配置文件
[root@web1 ~]# vim /etc/keepalived/keepalived.conf 
 12    router_id web1
 13    vrrp_iptables
... ...
 20 vrrp_instance VI_1 {
 21     state MASTER              # 状态，备份服务器为BACKUP
 22     interface eth0            # 网卡名
 23     virtual_router_id 51      # 虚拟路由器ID
 24     priority 100              # 优先级
 25     advert_int 1              # 发送心跳信息的间隔
 26     authentication {          # 两台机器的密码相同才能通过
 27         auth_type PASS
 28         auth_pass 1111
 29     }
 30     virtual_ipaddress {       # vip地址
 31         192.168.4.80/24
 32     }
 33 }
# 33行以下全部删除
 
# 打开一个新的终端监控日志
[root@web1 ~]# tail -f /var/log/messages
# 在第一个终端启服务，观察日志
[root@web1 ~]# systemctl start keepalived

# 查看vip
[root@web1 ~]# ip a s eth0


# 配置web2
[root@web1 ~]# scp /etc/keepalived/keepalived.conf 192.168.4.200:/etc/keepalived/
[root@web2 ~]# vim /etc/keepalived/keepalived.conf 
... ...
 12    router_id web2
 13    vrrp_iptables
... ...
 20 vrrp_instance VI_1 {
 21     state BACKUP
 22     interface eth0
 23     virtual_router_id 51
 24     priority 80
 25     advert_int 1
 26     authentication {
 27         auth_type PASS
 28         auth_pass 1111
 29     }
 30     virtual_ipaddress {
 31         192.168.4.80/24
 32     }
 33 }

# 打开一个新的终端监控日志
[root@web2 ~]# tail -f /var/log/messages
# 在第一个终端启服务，观察日志
[root@web2 ~]# systemctl start keepalived.service 
[root@web2 ~]# ip a s eth0    # 没有vip地址


# 打开一个终端监控日志
[root@web2 ~]# tail -f /var/log/messages
# 关闭web1上的keepalived
[root@web1 ~]# systemctl stop keepalived
[root@web2 ~]# ip a s eth0    # 出现vip地址
# 客户端访问
[root@client1 ~]# curl http://192.168.4.80
apache web server2

# 启动web1上的keepalived
[root@web1 ~]# systemctl start keepalived
# 观察结果，VIP地址将会切回到web1
```

### 配置高可用负载均衡的web集群

- 环境准备: LVS-DR模式

```shell
# 在两台web服务器上停止keepalived
[root@web1 ~]# systemctl stop keepalived
[root@web1 ~]# yum remove -y keepalived.x86_64 
[root@web2 ~]# systemctl stop keepalived.service 
[root@web2 ~]# yum remove -y keepalived.x86_64 

# 2台web服务lo:0的VIP仍然保留，内核参数配置仍然保留


# lvs1删除LVS规则，因为规则将会由keepalived配置
[root@lvs1 ~]# ipvsadm -D -t 192.168.4.15:80
# lvs1删除VIP，因为VIP将由keepalived配置
[root@lvs1 ~]# ifdown eth0:0
[root@lvs1 ~]# rm -f /etc/sysconfig/network-scripts/ifcfg-eth0:0


# 开启新的lvs2
[root@zzgrhel8 ~]# clone-vm7 
Enter VM number: 16
[root@zzgrhel8 ~]# virsh domrename tedu_node16 lvs2107-2
[root@zzgrhel8 ~]# virsh start lvs2107-2
[root@zzgrhel8 ~]# virsh console lvs2107-2
localhost login: root
Password: 123456
# 执行以下命令
hostnamectl set-hostname lvs2
nmcli connection modify eth0 ipv4.method manual ipv4.addresses 192.168.4.6/24
nmcli connection down eth0
nmcli connection up eth0
echo a | passwd --stdin root
[root@localhost ~]# exit
# 按ctrl+]退出控制台，可以通过ssh的方式登陆
```

- 在LVS上配置keepalived，实现高可用

```shell
# 在lvs1上配置keepalived

# 安装
[root@lvs1 ~]# yum install -y keepalived
[root@lvs1 ~]# vim /etc/keepalived/keepalived.conf 
! Configuration File for keepalived

global_defs {
   notification_email {
     root@localhost            # 收件人地址
   }
   notification_email_from admin@tedu.cn   # 发件人地址
   smtp_server 127.0.0.1       # 邮件服务器地址
   smtp_connect_timeout 30
   router_id lvs1              # 唯一标别ID
   vrrp_iptables               # 自动开启IPTABLES放行规则
   vrrp_skip_check_adv_addr
   vrrp_strict
   vrrp_garp_interval 0
   vrrp_gna_interval 0
}

vrrp_instance VI_1 {
    state MASTER
    interface eth0
    virtual_router_id 51
    priority 100
    advert_int 1
    authentication {
        auth_type PASS
        auth_pass 1111
    }
    virtual_ipaddress {
        192.168.4.15/24       # VIP地址
    }
}

virtual_server 192.168.4.15 80 {   # LVS虚拟服务器
    delay_loop 6              # 健康检查延迟6秒
    lb_algo rr                # 调度算法
    lb_kind DR                # 工作模式
    persistence_timeout 50    # 50秒内相同客户端调度到相同服务器
    protocol TCP

    real_server 192.168.4.100 80 {    # real server配置
        weight 1              # 权重
	TCP_CHECK {              # 对real server健康检查的配置
	    connect_timeout 3    # 连接超时时间
	    nb_get_retry    3    # 健康检查重试次数
	    delay_before_retry 3 # 两次检查的间隔
	}
    }
    real_server 192.168.4.200 80 {
        weight 2
	TCP_CHECK {
	    connect_timeout 3
	    nb_get_retry    3
	    delay_before_retry 3
	}
    }
}


# 为了实现邮件报警功能，安装邮件服务
[root@lvs1 ~]# yum install -y postfix
[root@lvs1 ~]# systemctl enable postfix --now
[root@lvs1 ~]# ss -tlnp | grep :25

# 启动keepalived服务
[root@lvs1 ~]# systemctl start keepalived

# 验证
[root@lvs1 ~]# ip a s eth0    # 查看到vip 192.168.4.15
[root@lvs1 ~]# ipvsadm -Ln    # 规则已经生成
# 因为50秒内相同客户端总是被调度到相同服务器，所以看到的内容相同
[root@client1 ~]# for i in {1..4}
> do
> curl http://192.168.4.15/
> done
apache web server2
apache web server2
apache web server2
apache web server2
# 50秒later
[root@client1 ~]# for i in {1..4}; do curl http://192.168.4.15/; done
192.168.2.100
192.168.2.100
192.168.2.100
192.168.2.100

# 关闭web1的服务
[root@web1 ~]# systemctl stop httpd
[root@lvs1 ~]# ipvsadm -Ln   # 192.168.4.100消失
[root@lvs1 ~]# yum install -y mailx
[root@lvs1 ~]# mail   # 查看当前用户的邮件，回车查看邮件，按q退出

# 启动web1的服务
[root@web1 ~]# systemctl start httpd
[root@lvs1 ~]# ipvsadm -Ln   # 192.168.4.100再次出现
[root@lvs1 ~]# mail   # 收到服务up的邮件




# 配置第二台lvs
[root@lvs2 ~]# yum install -y keepalived ipvsadm postfix mailx
[root@lvs1 ~]# scp /etc/keepalived/keepalived.conf 192.168.4.6:/etc/keepalived/
[root@lvs2 ~]# vim /etc/keepalived/keepalived.conf 
... ...
 10    router_id lvs2
 19     state BACKUP
 22     priority 80
[root@lvs2 ~]# systemctl start keepalived
[root@lvs2 ~]# ipvsadm -Ln

# 验证，关闭lvs1，客户端仍然可以访问192.168.4.15。
[root@lvs1 ~]# shutdown -h now
[root@lvs2 ~]# ip a s eth0   # 出现192.168.4.15
[root@client1 ~]# curl http://192.168.4.15/
```

## HaProxy

- 是实现负载均衡集群的另一种方式
- 配置

| 主机    | 地址             |
| ------- | ---------------- |
| client1 | 192.168.4.10/24  |
| haproxy | 192.168.4.5/24   |
| web1    | 192.168.4.100/24 |
| web2    | 192.168.4.200/24 |

注意：haproxy不要求使用vip。但是现有环境中web的vip也没有影响。

```shell
# haproxy继续使用lvs1，把它清理一下
[root@lvs1 ~]# yum remove -y keepalived ipvsadm
[root@lvs1 ~]# hostnamectl set-hostname haproxy1

[root@haproxy1 ~]# yum install -y haproxy
[root@haproxy1 ~]# vim /etc/haproxy/haproxy.cfg 
# 在配置文件中global是全局配置，default是缺省配置，如果后续有和default冲突的配置，default将会被覆盖。
# frontend是前端，表示调度器响应客户端的配置。backend是后端，表示调度器到后台服务器的配置。一般将它们合而为一，用listen来代替。
# 删除61行到结尾的配置， 并加入自己的配置如下：
 61 listen myweb 0.0.0.0:80   # 声明虚拟服务器
 62     balance roundrobin    # 调度算法
      # 服务器每2000毫秒检查一次，连续2次成功则为up，5次失败为down
 63     server web1 192.168.4.100 check inter 2000 rise
    2 fall 5
 64     server web2 192.168.4.200 check inter 2000 rise     2 fall 5
 65     
 66 listen stats *:1080
 67     stats refresh 30s
 68     stats uri /stats
 69     stats realm Haproxy Manager
 70     stats auth admin:admin

# 启服务
[root@haproxy1 ~]# systemctl start haproxy
# 客户端验证
[root@client1 ~]# for i in {1..4}
> do
> curl http://192.168.4.5/
> done
apache web server2
192.168.2.100
apache web server2
192.168.2.100

# 通过浏览器访问临控页面：http://192.168.4.5:1080/stats
# 关闭任意一个web服务，观察监控页的变化
```



明天的4台机器： 192.168.4.11-13  名字node1-3。client1: 192.168.4.10

