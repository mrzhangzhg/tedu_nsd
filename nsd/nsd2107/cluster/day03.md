# day03

## ceph

- 官方手册：http://docs.ceph.org.cn/

- ceph被称作面向未来的存储
- ceph可以实现的存储方式
  - 块存储：像基本硬盘一样
  - 文件系统存储：类似于NFS的共享方式
  - 对象存储：像百度云盘一样，需要单独的客户端
- ceph存储数据采用多副本的方式存储，生产环境下，一个文件至少要存3份

### ceph组件

- OSD：功能是存储数据，处理数据的复制、恢复、回填、再均衡，并通过检查其他OSD 守护进程的心跳来向 Ceph Monitors 提供一些监控信息。存储节点有几个用于ceph存储的硬盘，就有几个OSD进程。
- Monitor：维护着展示集群状态的各种图表，包括监视器图、 OSD 图、归置组（ PG ）图、和 CRUSH 图。主要负责管理集群状态。MON节点数一般为奇数，因为大家意见不一致时，需要少数服从多数。
- MDS：为Ceph 文件系统存储元数据（也就是说，Ceph 块设备和 Ceph 对象存储不使用MDS ）。只有文件系统方式提供存储，才需要该组件。元数据是描述数据的数据。文件的元数据，如文件的大小、权限、访问时间等。
- RGW：对象存储网关。主要为访问ceph的软件提供API接口。

### 搭建ceph集群

- 三个节点，每个节点添加2块20G硬盘

| 主机名 | IP地址          |
| ------ | --------------- |
| node1  | 192.168.4.11/24 |
| node2  | 192.168.4.12/24 |
| node3  | 192.168.4.13/24 |
| client | 192.168.4.10/24 |

- 基础环境准备
```shell
# 创建4台虚拟机，配置好名字和IP地址，以及yum
[root@zzgrhel8 ~]# clone-vm7    # 创建4台虚拟机

# 配置ceph的yum源
[root@zzgrhel8 ~]# yum install -y vsftpd
[root@zzgrhel8 ~]# systemctl start vsftpd
[root@zzgrhel8 ~]# mkdir /var/ftp/ceph
[root@zzgrhel8 ~]# vim /etc/fstab 
/iso/ceph10.iso /var/ftp/ceph   iso9660 defaults,loop   0 0
[root@zzgrhel8 ~]# mount -a
[root@zzgrhel8 ~]# df -h /var/ftp/ceph
文件系统        容量  已用  可用 已用% 挂载点
/dev/loop3      284M  284M     0  100% /var/ftp/ceph

# 在node1-3节点上配置yum
[root@node1 ~]# vim /etc/yum.repos.d/ceph.repo
[osd]
name=ceph osd
baseurl=ftp://192.168.4.254/ceph/OSD
enabled=1
gpgcheck=0
[mon]
name=ceph mon
baseurl=ftp://192.168.4.254/ceph/MON
enabled=1
gpgcheck=0
[TOOLS]
name=ceph tools
baseurl=ftp://192.168.4.254/ceph/Tools
enabled=1
gpgcheck=0
[root@node1 ~]# for i in 12 13; do scp /etc/yum.repos.d/ceph.repo 192.168.4.$i:/etc/yum.repos.d/; done


# 关闭防火墙和SELINUX

# 一切配置操作都在node1上完成。所以node1需要对远程主机有权限
[root@node1 ~]# ssh-keygen 
[root@node1 ~]# for i in {10..13}
> do
> ssh-copy-id 192.168.4.$i
> done

# 在所有的主机上配置名称解析
[root@node1 ~]# vim /etc/hosts
.. ...
192.168.4.10    client1
192.168.4.11    node1
192.168.4.12    node2
192.168.4.13    node3
[root@node1 ~]# for i in 10 12 13
> do
> scp /etc/hosts 192.168.4.$i:/etc/
> done
```
- 安装集群
```shell
# 在3个节点上安装软件包
[root@node1 ~]# for i in node{1..3}
> do
> ssh $i yum install -y ceph-mon ceph-osd ceph-mds ceph-radosgw
> done

# 配置client为NTP服务器
[root@client1 ~]# yum install -y chrony
[root@client1 ~]# vim /etc/chrony.conf 
 29 allow 192.168.4.0/24
 33 local stratum 10  # 即使没有从一个源同步到时间，也会提供时间服务
[root@client1 ~]# systemctl restart chronyd

# 配置node1-3为NTP客户端
[root@node1 ~]# for i in node{1..3}
> do
> ssh $i yum install -y chrony
> done
[root@node1 ~]# vim /etc/chrony.conf 
... ...
7  server 192.168.4.10 iburst
... ...
[root@node1 ~]# for i in node{2..3}
> do
> scp /etc/chrony.conf $i:/etc/
> done
[root@node1 ~]# for i in node{1..3}; do ssh $i systemctl restart chronyd; done
# 验证时间是否同步
[root@node1 ~]# chronyc sources -v


# 在node1上安装ceph集群管理工具
[root@node1 ~]# yum install -y ceph-deploy
# 创建ceph-deploy的工作目录
[root@node1 ~]# mkdir ceph-cluster
[root@node1 ~]# cd ceph-cluster
[root@node1 ceph-cluster]# ceph-deploy new node{1..3}
[root@node1 ceph-cluster]# ls
ceph.conf  ceph-deploy-ceph.log  ceph.mon.keyring

# 开启COW分层快照的功能。COW：写时复制Copy On Write
[root@node1 ceph-cluster]# vim ceph.conf 
rbd_default_features = 1

# 初始化monitor
[root@node1 ceph-cluster]# ceph-deploy mon create-initial
[root@node1 ceph-cluster]# systemctl status ceph-mon@node1.service 
[root@node2 ~]# systemctl status ceph-mon@node2.service  
[root@node3 ~]# systemctl status ceph-mon@node3.service  

# 查看集群状态
[root@node1 ceph-cluster]# ceph -s

# 向ceph存储添加硬盘
# 初始化硬盘。相当于对硬进行初始化
[root@node1 ceph-cluster]# ceph-deploy disk zap node1:vdb node1:vdc
[root@node1 ceph-cluster]# ceph-deploy disk zap node2:vd{b,c}
[root@node1 ceph-cluster]# ceph-deploy disk zap node3:vd{b,c}

# 创建存储空间。每块硬盘将会分成两个分区，一个分区固定大小5G，另一个分区使用剩余空间
[root@node1 ceph-cluster]# for i in {1..3}
> do
> ceph-deploy osd create node$i:vd{b,c}
> done

# 查看osd状态。节点上有几块OSD硬盘，就会有几个进程
[root@node1 ceph-cluster]# systemctl status ceph-osd*

[root@node1 ceph-cluster]# ceph -s   # health HEALTH_OK正常 
```

### 块存储

- 本地存储设备，有块设备和字符设备。块设备存取数据时可以一次存取很多，查是字符设备只能是字符流。

```shell
# 块设备为b，表示block，字符设备是c，表示character
[root@node1 ceph-cluster]# ll /dev/vda
brw-rw---- 1 root disk 253, 0 9月  29 10:35 /dev/vda
[root@node1 ceph-cluster]# ll /dev/tty1
crw--w---- 1 root tty 4, 1 9月  29 10:36 /dev/tty1
```

- 块存储，就是可以提供像硬盘一样设备的存储。使用块存储的节点，需要对块设备进行分区、格式化、挂载后使用。

```shell
# 查看存储池。ceph默认创建了编号为0的存储池，名为rbd。
[root@node1 ceph-cluster]# ceph osd lspools 
0 rbd,

# 在存储池中划分空间（创建镜像），提供给客户端使用
[root@node1 ceph-cluster]# rbd create demo-image --size 10G

# 列出默认存储池中的镜像
[root@node1 ceph-cluster]# rbd list 
demo-image

# 查看默认存储池中的镜像demo-image信息
[root@node1 ceph-cluster]# rbd info demo-image
# 查看rbd存储池中的镜像demo-image信息
[root@node1 ceph-cluster]# rbd info rbd/demo-image

```









> 附：创建链接克隆的虚拟机
>
> ```shell
> # .node_base.qcow2是已有虚拟机的硬盘。myvm.img是基于模板硬盘的新硬盘。30GB是myvm.img虚拟大小
> [root@zzgrhel8 images]# qemu-img create -f qcow2 -b .node_base.qcow2 myvm.img 30G
> [root@zzgrhel8 images]# ls -lh myvm.img 
> [root@zzgrhel8 images]# qemu-img info myvm.img
> 
> # 通过virt-manager新建虚拟机，使用现有硬盘，创建新虚拟机
> # 新建虚拟机->导入现有磁盘映像->现有硬盘镜像输入/var/lib/libvirt/images/myvm.img。系统选rhel7。剩下默认。
> ```



