# day04

## ceph应用

- 检查ceph服务是否健康

```shell
[root@node1 ~]# ceph -s
... ...
     health HEALTH_OK
... ...

# 如果不健康，检查时间是否同步，然后重启相关服务
[root@node1 ~]# systemctl restart ceph\*.service ceph\*.target
```

### ceph块存储

- 存储池：所有的存储划分成的逻辑管理单元，默认有一个名为rbd的存储池，编号为0

```shell
# 查看存储池
[root@node1 ~]# ceph osd lspools
0 rbd,

# 查看存储池rbd存储数据时，存储的副本数
[root@node1 ~]# ceph osd pool get rbd size
size: 3

# 查看存储池的使用情况
[root@node1 ~]# ceph df

# 查看每个OSD的信息
[root@node1 ~]# ceph osd df

# 在默认存储池中创建10G的镜像，名为demo-img
[root@node1 ~]# rbd  create demo-img --size 10G

# 在存储池rbd中创建10G的镜像，名为zzg-img
[root@node1 ~]# rbd create rbd/zzg-img --size 10G

# 列出默认存储池中的镜像
[root@node1 ~]# rbd list
demo-image
demo-img
zzg-img

# 查看镜像信息
[root@node1 ~]# rbd info demo-img

# 扩容
[root@node1 ~]# rbd resize --size 15G demo-img
Resizing image: 100% complete...done.
[root@node1 ~]# rbd info demo-img
rbd image 'demo-img':

# 缩减
[root@node1 ~]# rbd resize --size 7G demo-img --allow-shrink
Resizing image: 100% complete...done.
[root@node1 ~]# rbd info demo-img
rbd image 'demo-img':
	size 7168 MB in 1792 objects

# 删除镜像
[root@node1 ~]# rbd rm demo-image


# 在客户端使用块设备。
# 客户端需要解决的问题：
# 1. 怎么用？（装软件）
# 2. 集群在哪？（配置文件，集群地址）3. 权限（keyring）
[root@client1 ~]# yum install -y ceph-common
[root@client1 ~]# ls /etc/ceph/
rbdmap
[root@node1 ~]# scp /etc/ceph/ceph.conf 192.168.4.10:/etc/ceph/
[root@node1 ~]# scp /etc/ceph/ceph.client.admin.keyring 192.168.4.10:/etc/ceph/

# 查看本地块设备
[root@client1 ~]# lsblk    # 只有本地硬盘

# 将ceph的镜像映射到本地
[root@client1 ~]# rbd map demo-img
/dev/rbd0

# 查看本地块设备
[root@client1 ~]# lsblk    # 将会看到新的名为rbd0的硬盘
NAME   MAJ:MIN RM SIZE RO TYPE MOUNTPOINT
vda    253:0    0  30G  0 disk 
└─vda1 253:1    0  30G  0 part /
rbd0   252:0    0   7G  0 disk 

# 查看映射
[root@client1 ~]# rbd showmapped
id pool image    snap device    
0  rbd  demo-img -    /dev/rbd0 

# 使用rbd设备 
[root@client1 ~]# mkfs.xfs /dev/rbd0 
[root@client1 ~]# mount /dev/rbd0 /mnt/
[root@client1 ~]# df -h /mnt/
文件系统        容量  已用  可用 已用% 挂载点
/dev/rbd0       7.0G   33M  7.0G    1% /mnt
[root@client1 ~]# cp /etc/hosts /mnt/

# 停用设备
[root@client1 ~]# umount /mnt/
[root@client1 ~]# rbd unmap /dev/rbd0 
```

- 快照。保存镜像某一个时间点的数据

```shell
# 查看demo-img有没有快照
[root@node1 ~]# rbd snap ls demo-img

# 为demo-img创建快照
[root@node1 ~]# rbd snap create demo-img --snap demo-snap1
[root@node1 ~]# rbd snap ls demo-img
SNAPID NAME          SIZE 
     4 demo-snap1 7168 MB 

# 删除数据镜像中的数据
[root@client1 ~]# rbd map demo-img
/dev/rbd0
[root@client1 ~]# mount /dev/rbd0 /mnt/
[root@client1 ~]# ls /mnt/
hosts
[root@client1 ~]# rm -f /mnt/hosts 
[root@client1 ~]# umount /mnt/
[root@client1 ~]# rbd unmap /dev/rbd0

# 通过快照demo-snap1还原数据
[root@node1 ~]# rbd snap rollback demo-img --snap demo-snap1
[root@client1 ~]# rbd map demo-img
/dev/rbd0
[root@client1 ~]# mount /dev/rbd0 /mnt/
[root@client1 ~]# ls /mnt/
hosts

# 查看demo-img有哪些镜像
[root@node1 ~]# rbd snap ls demo-img
SNAPID NAME          SIZE 
     4 demo-snap1 7168 MB 

# 保护demo-img的快照demo-snap1，防止快照被误删除
[root@node1 ~]# rbd snap protect demo-img --snap demo-snap1
# 删除demo-img的快照demo-snap1，失败
[root@node1 ~]# rbd snap rm demo-img --snap demo-snap1

# 为快照创建克隆。为demo-img镜像的快照demo-snap1创建克隆
[root@node1 ~]# rbd clone demo-img --snap demo-snap1 demo-sn1-clone --image-feature layering

# 查看克隆镜像的信息
[root@node1 ~]# rbd info demo-sn1-clone
rbd image 'demo-sn1-clone':
	size 7168 MB in 1792 objects
	order 22 (4096 kB objects)
	block_name_prefix: rbd_data.108a3d1b58ba
	format: 2
	features: layering
	flags: 
	parent: rbd/demo-img@demo-snap1   # 该克隆的父镜像
	overlap: 7168 MB

# 新的克隆也是镜像
[root@node1 ~]# rbd list
demo-img
demo-sn1-clone
zzg-img

# 克隆镜像同样可以挂载使用
[root@client1 ~]# rbd map demo-sn1-clone
/dev/rbd1
# 挂载的时候rbd0和rbd1不要同时挂载，它们的UUID相同
[root@client1 ~]# mount /dev/rbd1 /media/
[root@client1 ~]# ls /media/
hosts


# 克隆快照（子镜像）依赖于父镜像。如果父镜像删除了，子镜像也无法使用。
# 可以把父镜像内容合并到子镜像中一份，这样子镜像就变成独立镜像了
# 把demo-sn1-clone父镜像内容合并过来
[root@node1 ~]# rbd flatten demo-sn1-clone

# 取消快照保护
[root@node1 ~]# rbd snap unprotect demo-img --snap demo-snap1
# 删除快照
[root@node1 ~]# rbd snap rm demo-img --snap demo-snap1
# 删除镜像，如果客户端正在使用，需要把它unmap
[root@node1 ~]# rbd rm demo-sn1-clone
```

### ceph文件系统

- 文件系统：相当于是组织数据存储的方式。
- 格式化时，就是在为存储创建文件系统。
- Linux对ceph支持的很好，可以把ceph文件系统直接挂载到本地。

- 存储方式：
  - DAS(Direct-attached Storage) 直连存储。直连式存储与服务器主机直接相连。如本地硬盘。文件系统在使用端创建。
  - NAS(Network Attached Storage)  网络附加存储——是一个网络上的文件系统。比如NFS、CIFS（windows的共享）。简单来说，就是共享文件夹。文件系统在提供存储端创建。
  - SAN(Storage Area Network)  存储区域网络——是一个网络上的磁盘。分成ip-SAN（基于传统IP网络）和FC-SAN（基于光纤网络）

- ceph的文件系统需要配置MDS组件
- 实现

```shell
# 在node3节点上部署MDS
[root@node3 ~]# yum install -y ceph-mds

# 创建CEPH文件系统
[root@node1 ~]# cd ceph-cluster/
[root@node1 ceph-cluster]# ceph-deploy mds create node3

# 新建存储池。创建名为cephfs_data的存储池，PG数量为64。PG称为归置组，可以理解为硬盘上的文件夹。典型配置给每个 OSD 分配大约 100 个归置组
[root@node1 ceph-cluster]# ceph osd pool create cephfs_data 64
# 创建名为cephfs_metadata的存储池，PG数量为64。
[root@node1 ceph-cluster]# ceph osd pool create cephfs_metadata 64
# 现在cephfs_data和cephfs_metadata除了名字以外，全部一样。
[root@node1 ceph-cluster]# ceph osd lspools
0 rbd,1 cephfs_data,2 cephfs_metadata,

# 创建cephfs，将数据写到ceph_data存储池中，元数据写到cephfs_metadata存储池中
[root@node1 ceph-cluster]# ceph fs new myfs1 cephfs_metadata cephfs_data
# 查看文件系统
[root@node1 ceph-cluster]# ceph fs ls
name: myfs1, metadata pool: cephfs_metadata, data pools: [cephfs_data ]



# 客户端挂载文件系统
[root@client1 ~]# cat /etc/ceph/ceph.client.admin.keyring 
[client.admin]
	key = AQCHFVRhs16AMxAAFqIyPiGH1/bSUjVVXV5UwA==

[root@client1 ~]# mount -t ceph -o name=admin,secret=AQCHFVRhs16AMxAAFqIyPiGH1/bSUjVVXV5UwA== 192.168.4.13:6789:/ /mnt 

[root@client1 ~]# df -h /mnt
文件系统             容量  已用  可用 已用% 挂载点
192.168.4.13:6789:/   90G  252M   90G    1% /mnt
```

### ceph对象存储

- 需要专门的客户端访问
- 键值对存储方式

- 配置

```shell
# 安装
[root@node3 ~]# yum install -y ceph-radosgw
[root@node1 ~]# cd ceph-cluster/
[root@node1 ceph-cluster]# ceph-deploy rgw create node3
[root@node3 ~]# systemctl status ceph-radosgw@\*
```

- 使用：http://docs.ceph.org.cn/radosgw/s3/python/
  - http://docs.ceph.org.cn/radosgw/s3/















