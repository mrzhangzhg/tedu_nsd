# day01

[toc]

## 集群

- 将很多机器组织到一起，作为一个整体对外提供服务
- 集群在扩展性、性能方面都可以做到灵活
- 集群分类：
  - 负载均衡集群LB：Load Balance
  - 高可用集群HA：High Availablitiy
  - 高性能计算HPC：High Performance Computing

## LVS集群

- LVS：Linux虚拟服务器
- 是章文嵩在国防科技大学读博士期间编写
- 实现负载均衡集群
- LVS的工作模式：
  - NAT：网络地址转换
  - DR：路由模式
  - TUN：隧道模式

- 术语：
  - 调度器：LVS服务器
  - 真实服务器：Real Server，提供服务的服务器
  - VIP：虚拟地址，提供给用户访问的地址
  - DIP：LVS服务器上，用于连接后台服务器的地址
  - RIP：真实IP，真实服务器上的地址

- 调度算法：支持的有10种，常见的4种
  - 轮询rr：Real Server轮流提供服务
  - 加权轮询wrr：Real Server根据权重，轮流提供服务
  - 最少连接LC：根据Real Server的连接数数，分配请求
  - 加权最少连接WLC：类似于wrr，给不同的服务器分配不同的权重

### LVS-NAT

#### 基础环境

- 环境准备
  - client1：eth0_192.168.4.10
  - lvs1：eth0_192.168.4.5，eth1_192.168.2.5
  - web1: eth1_192.168.2.100
  - web2: eth1_192.168.2.200

```shell
# 创建4台虚拟机
[root@zzgrhel8 ~]# clone-vm7    # 执行4遍
Enter VM number: 11     # 填写不重复的数字
[root@zzgrhel8 ~]# virsh list --all
 Id   名称           状态
---------------------------
 -    tedu_node11    关闭

# 虚拟机改名，可选
[root@zzgrhel8 ~]# virsh domrename tedu_node11 client2107-1
[root@zzgrhel8 ~]# virsh domrename tedu_node12 lvs2107-1
[root@zzgrhel8 ~]# virsh domrename tedu_node13 web2107-1
[root@zzgrhel8 ~]# virsh domrename tedu_node14 web2107-2

# 初始化虚拟机
[root@zzgrhel8 ~]# virsh start client2107-1     # 启动
[root@zzgrhel8 ~]# virsh console client2107-1   # 打开控制台
localhost login: root
Password: 123456

# 在虚拟机中执行以下命令：修改主机名、配置IP地址、重启网卡、改root密码
hostnamectl set-hostname client1
nmcli connection modify eth0 ipv4.method manual ipv4.addresses 192.168.4.10/24
nmcli connection down eth0
nmcli connection up eth0
echo a | passwd --stdin root

# 退出虚拟机，就可以ssh远程连接了
[root@localhost ~]# exit
# 退出后，按组合键ctrl+]，退出控制台，返回物理机
```

> 说明：通过clone-vm7创建出来的虚拟机，没有安装firewall防火墙，selinux已经设置为disabled。yum已经配置为192.168.4.254
>
> 网卡配置文件目录：`/etc/sysconfig/network-scripts/`，网卡配置文件名：`ifcfg-网卡名`
>
> - cowsay: http://rpmfind.net搜cowsay

#### 配置步骤

- 准备web服务器的内容

```shell
# 安装apache web服务器
[root@web1 ~]# yum install -y httpd
[root@web2 ~]# yum install -y httpd

# 默认情况下，apache web服务器存放网页的目录是/var/www/html/
# 默认的首页文件，名为index.html
[root@web1 ~]# echo "192.168.2.100" > /var/www/html/index.html
[root@web2 ~]# echo "apache web server2" > /var/www/html/index.html

# 启服务
[root@web1 ~]# systemctl enable httpd --now
[root@web2 ~]# systemctl enable httpd --now

# 在lvs主机上访问验证
[root@lvs1 ~]# curl http://192.168.2.100
192.168.2.100
[root@lvs1 ~]# ^100^200   # 将上一条命令中的100换成200后执行
curl http://192.168.2.200
apache web server2
```

- 配置2台web服务器的网关

```shell
[root@web1 ~]# nmcli connection modify eth1 ipv4.gateway 192.168.2.5
[root@web1 ~]# ifdown eth1; ifup eth1
[root@web1 ~]# route -n
# 看到出现0.0.0.0的Gateway是192.168.2.5表示成功
```

- LVS开启路由转发功能

```shell
# 查看路由转发功能，这是一项内核级别的功能
[root@lvs1 ~]# sysctl -a | grep ip_for

[root@lvs1 ~]# vim /etc/sysctl.conf 
net.ipv4.ip_forward = 1
[root@lvs1 ~]# sysctl -p   # 使得sysctl.conf中的配置立即生效

# 验证。/proc不是硬盘上的数据，不要直接改该目录下的内容
[root@lvs1 ~]# cat /proc/sys/net/ipv4/ip_forward
1
```

- 安装lvs

```shell
[root@lvs1 ~]# yum install -y ipvsadm
```

- 配置LVS的规则

  - ipvsadm命令说明

    ```shell
    [root@lvs1 ~]# ipvsadm 选项：
    -A：添加虚拟服务器（虚拟IP）
    -E：编辑虚拟服务器
    -t|u：tcp或udp协议
    -s：指定调度算法，如rr/wrr/lc/wlc
    
    -a: 创建虚拟服务器后，向虚拟服务器中加入真实服务器
    -r：真实服务器地址
    -w: 设置权重，默认是1
    -m：指定LVS的工作模式是NAT
    -g：指定LVS的工作模式是DR
    ```

```shell
# 1. 为web服务创建虚拟服务器，调度算法是rr
[root@lvs1 ~]# ipvsadm -A -t 192.168.4.5:80 -s rr

# 2. 查看配置
[root@lvs1 ~]# ipvsadm -Ln
... ...
TCP  192.168.4.5:80 rr

# 3. 向虚拟服务器添加rip（web服务器地址）
[root@lvs1 ~]# ipvsadm -a -t 192.168.4.5:80 -r 192.168.2.100 -w 1 -m
[root@lvs1 ~]# ipvsadm -a -t 192.168.4.5:80 -r 192.168.2.200 -w 2 -m

# 2. 查看配置
[root@lvs1 ~]# ipvsadm -Ln
... ...
TCP  192.168.4.5:80 rr
  -> 192.168.2.100:80             Masq    1      0          0         
  -> 192.168.2.200:80             Masq    2      0          0      
```

> 附：查看命令帮助
>
> ```shell
> [root@web1 ~]# ls --help
> 用法：ls [选项]... [文件]...
> []表示可选项
> ...表示可以有多项
> 
> [root@lvs1 ~]# ipvsadm -h
> ipvsadm -A|E -t|u|f service-address [-s scheduler] [-p [timeout]] [-M netmask] [--pe persistence_engine] [-b sched-flags]
> -A|E：没有用任何括号包裹，表示必选项
> |分开的各项表示N选一
> ```
>

- 客户端验证

```shell
[root@client1 ~]# for i in {1..4}
> do
> curl http://192.168.4.5/
> done
apache web server2
192.168.2.100
apache web server2
192.168.2.100
```

- 排错
  1. 检查各主机的地址
  2. web服务器没有配置网关
  3. LVS要打开ip_forward功能
  4. lvs规则不正确
  5. 防火墙要关闭

- 修改虚拟服务器的调度算法

```shell
[root@lvs1 ~]# ipvsadm -E -t 192.168.4.5:80 -s wrr
[root@lvs1 ~]# ipvsadm -Ln
... ...
TCP  192.168.4.5:80 wrr
... ...

[root@client1 ~]# for i in {1..6}; do curl http://192.168.4.5/; done
apache web server2
apache web server2
192.168.2.100
apache web server2
apache web server2
192.168.2.100
```

### LVS-DR模式

- LVS的DR模式，LVS只需要一块网卡，它不处于网关的位置。
- LVS和其他的真实服务器用相同的交换机连接，处于同一网络。

- 配置环境

```shell
# 删除LVS虚拟服务器
[root@lvs1 ~]# ipvsadm -D -t 192.168.4.5:80 

# 停用eth1
[root@lvs1 ~]# nmcli connection modify eth1 ipv4.method disabled ipv4.addresses ''
[root@lvs1 ~]# ifdown eth1

# 两台web服务器使用eth0 192.168.4.x网络
[root@web1 ~]# nmcli connection modify eth1 ipv4.method disabled ipv4.addresses '' ipv4.gateway ''
[root@web1 ~]# nmcli connection modify eth0 ipv4.method manual ipv4.addresses 192.168.4.100/24
[root@web1 ~]# nmcli connection down eth0
[root@web1 ~]# nmcli connection up eth0
[root@web1 ~]# ifdown eth1


[root@web2 ~]# nmcli connection modify eth0 ipv4.method manual ipv4.addresses 192.168.4.200/24
[root@web2 ~]# nmcli connection down eth0
[root@web2 ~]# nmcli connection up eth0
[root@web2 ~]# nmcli connection modify eth1 ipv4.method disabled ipv4.addresses '' ipv4.gateway ''
[root@web2 ~]# ifdown eth1
```

- 配置DR模式

  - 在lvs的eth0上配置vip

    ```shell
    # 为eth0增加一个逻辑子接口
    [root@lvs1 ~]# cd /etc/sysconfig/network-scripts/
    [root@lvs1 network-scripts]# cp ifcfg-eth0 ifcfg-eth0:0
    [root@lvs1 network-scripts]# vim ifcfg-eth0:0
    TYPE=Ethernet
    BOOTPROTO=none
    IPV4_FAILURE_FATAL=no
    NAME=eth0:0
    DEVICE=eth0:0
    ONBOOT=yes
    IPADDR=192.168.4.15
    PREFIX=24
    
    [root@lvs1 network-scripts]# ifup eth0:0
    [root@lvs1 network-scripts]# ifconfig eth0:0
    ```

    

  - 在2台web服务器的本地环回网卡lo上配置vip

    ```shell
    # lo是本地环回。它上面的地址以127开头。每台机器都认为127开头的地址是“我自己”的意思。不能通过127访问远程机器，因为127是自己的意思
    
    [root@web1 ~]# cd /etc/sysconfig/network-scripts/
    [root@web1 network-scripts]# cp ifcfg-lo ifcfg-lo:0
    [root@web1 network-scripts]# vim ifcfg-lo:0
    DEVICE=lo:0
    IPADDR=192.168.4.15
    NETMASK=255.255.255.255
    NETWORK=192.168.4.15
    BROADCAST=192.168.4.15
    ONBOOT=yes
    NAME=lo:0
    [root@web1 network-scripts]# ifup lo:0
    [root@web1 network-scripts]# ifconfig lo:0
    
    [root@web1 network-scripts]# scp ./ifcfg-lo:0 192.168.4.200:/etc/sysconfig/network-scripts/
    [root@web2 ~]# ifup lo:0
    [root@web2 ~]# ifconfig lo:0
    
    ```

    

  - 在2台web服务器上修改内核参数，使得它们不响应vip的查询请求

    ```shell
    [root@web1 ~]# sysctl -a | grep arp_ignore
    [root@web1 ~]# sysctl -a | grep arp_announce
    [root@web1 ~]# vim /etc/sysctl.conf
    net.ipv4.conf.all.arp_ignore = 1
    net.ipv4.conf.lo.arp_ignore = 1
    net.ipv4.conf.all.arp_announce = 2
    net.ipv4.conf.lo.arp_announce = 2
    [root@web1 ~]# sysctl -p
    ```

  - 在lvs上配置规则

    ```shell
    [root@lvs1 ~]# ipvsadm -A -t 192.168.4.15:80 -s wlc
    [root@lvs1 ~]# ipvsadm -a -t 192.168.4.15:80 -r 192.168.4.100 -g -w 1
    [root@lvs1 ~]# ipvsadm -a -t 192.168.4.15:80 -r 192.168.4.200 -g -w 2
    [root@lvs1 ~]# ipvsadm -Ln
    ... ...
    # 第一列是服务器配置；第二列是模式，DR显示为Route，NAT显示为Masq；第三列是权重；第四列是活跃连接；第五列是不活跃连接
    TCP  192.168.4.15:80 wlc
      -> 192.168.4.100:80             Route   1      0          0
      -> 192.168.4.200:80             Route   2      0          0  ... ...
    ```

  - 客户端验证

    ```shell
    [root@client1 ~]# for i in {1..6}; do curl http://192.168.4.15/; done
    apache web server2
    192.168.2.100
    apache web server2
    apache web server2
    192.168.2.100
    apache web server2
    ```





