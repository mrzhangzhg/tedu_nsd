import re

class CountPatt:
    def __init__(self, fname):
        self.fname = fname

    def count_patt(self, patt):
        patt_dict = {}  # 定义用于保存结果的变量
        with open(self.fname) as fobj:
            for line in fobj:
                m = re.search(patt, line)
                if m:  # 如果m是None为假，非None为真
                    k = m.group()
                    patt_dict[k] = patt_dict.get(k, 0) + 1

        return patt_dict

if __name__ == '__main__':
    log_file = 'access_log'
    ip = '^(\d+\.){3}\d+'
    br = 'Firefox|Chrome|MSIE'
    cp1 = CountPatt(log_file)
    result1 = cp1.count_patt(ip)
    result2 = cp1.count_patt(br)
    print(result1)
    print(result2)
