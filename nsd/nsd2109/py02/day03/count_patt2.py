import re

class CountPatt:
    def count_patt(self, fname, patt):
        patt_dict = {}  # 定义用于保存结果的变量
        with open(fname) as fobj:
            for line in fobj:
                m = re.search(patt, line)
                if m:  # 如果m是None为假，非None为真
                    k = m.group()
                    patt_dict[k] = patt_dict.get(k, 0) + 1

        return patt_dict

if __name__ == '__main__':
    log_file = 'access_log'
    ip = '^(\d+\.){3}\d+'
    cp1 = CountPatt()
    result1 = cp1.count_patt(log_file, ip)
    print(result1)
