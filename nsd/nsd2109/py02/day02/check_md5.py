import hashlib
import sys

def check_md5(fname):
    # 创建md5对象
    m = hashlib.md5()
    # 读取文件，计算md5值
    with open(fname, 'rb') as fobj:
        while 1:
            data = fobj.read(4096)  # 每次读4096字节
            if len(data) == 0:      # 如果没有数据了，则中断循环
                break
            m.update(data)          # 更新md5

    return m.hexdigest()

if __name__ == '__main__':
    md5 = check_md5(sys.argv[1])
    print(md5)
