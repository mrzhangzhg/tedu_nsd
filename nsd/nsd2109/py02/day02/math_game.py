# 10 + 2 = 12
# Very Good!
# 继续吗(y/n)? y
# 1 + 1 = 2
# Very Good!
# 继续吗(y/n)? n
# Bye-bye
from random import randint, choice

def exam():
    '出题作答'
    # 随机产生两个100以内的整数，并排序
    nums = [randint(1, 100) for i in range(2)]
    nums.sort()      # 升序排列
    nums.reverse()   # 翻转
    # 随机取出加减号
    op = choice('+-')
    # 求出正确答案
    if op == '+':
        result = nums[0] + nums[1]
    else:
        result = nums[0] - nums[1]

    # 用户作答，判断对错
    prompt = f'{nums[0]} {op} {nums[1]} = '
    n = 0
    while n < 3:
        try:
            answer = int(input(prompt))
        except:  # 不指定异常，可以捕获所有异常
            print()
            continue

        if answer == result:
            print('不错哟')
            break
        print('不对哟')
        n += 1
    else:
        print(f'{prompt}{result}')

def main():
    '主程序'
    while 1:
        exam()
        # 去除用户输入字符两端的空格，然后取出第1个字符
        try:
            yn = input('继续吗(Y/n)? ').strip()[0]
        except IndexError:
            yn = 'y'
        except (KeyboardInterrupt, EOFError):
            yn = 'n'

        if yn in ['n', 'N']:
            print('\nBye-bye')
            break

if __name__ == '__main__':
    main()
