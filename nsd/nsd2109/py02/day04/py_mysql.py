import pymysql

# 创建到数据库的连接
conn = pymysql.connect(
    host='localhost',
    user='root',
    password='tedu.cn',
    db='nsd2021',
    charset='utf8mb4'
)
# 创建游标。游标类似于文件对象，通过文件对象读写文件，通过游标操作数据库
cursor = conn.cursor()
# 编写sql语句
sql1 = '''CREATE TABLE departments(
id INT PRIMARY KEY AUTO_INCREMENT, dep_name VARCHAR(10)
)'''
# 通过游标执行sql语句
cursor.execute(sql1)
# 确认更改
conn.commit()
# 关闭
cursor.close()
conn.close()
