# nsd2021-py02-day04

[TOC]

## 复杂列表的排序

```python
>>> ips = {'172.40.58.150': 10, '172.40.58.124': 6, '172.40.58.101': 10, '127.0.0.1': 121, '192.168.4.254': 103, '192.168.2.254': 110, '201.1.1.254': 173, '201.1.2.254': 119, '172.40.0.54': 391, '172.40.50.116': 244}
>>> l1 = list(ips.items())
>>> l1
[('172.40.58.150', 10), ('172.40.58.124', 6), ('172.40.58.101', 10), ('127.0.0.1', 121), ('192.168.4.254', 103), ('192.168.2.254', 110), ('201.1.1.254', 173), ('201.1.2.254', 119), ('172.40.0.54', 391), ('172.40.50.116', 244)]

# 列表的sort方法接受一个名为key的参数，key应该对应一个函数。这个函数作用于列表的每一项，它将列表项的处理结果作为排序依据
>>> def func1(seq):
...   return seq[-1]
>>> l1.sort(key=func1)
>>> l1
[('172.40.58.124', 6), ('172.40.58.101', 10), ('172.40.58.150', 10), ('192.168.4.254', 103), ('192.168.2.254', 110), ('201.1.2.254', 119), ('127.0.0.1', 121), ('201.1.1.254', 173), ('172.40.50.116', 244), ('172.40.0.54', 391)]

# 匿名函数、降序排列
>>> l1.sort(key=lambda seq: seq[-1], reverse=True)
>>> l1
[('172.40.0.54', 391), ('172.40.50.116', 244), ('201.1.1.254', 173), ('127.0.0.1', 121), ('201.1.2.254', 119), ('192.168.2.254', 110), ('192.168.4.254', 103), ('172.40.58.101', 10), ('172.40.58.150', 10), ('172.40.58.124', 6)]
```

## 安装python包

- python相关的软件包数量庞大，python标准包没有包括全部可用的包
- python官方支持的软件包站点：https://pypi.org/
- 安装python包可以使用pip。yum用于安装rpm包，pip用于安装python包
- pip可以在线安装装软件包，也可以安装本地包
- 在线安装时，默认将会到pypi官方站点安装，速度慢。
- 配置使用国内镜像站点安装python包

```python
[root@localhost nsd2020]# mkdir ~/.pip
[root@localhost nsd2020]# vim ~/.pip/pip.conf
[global]
index-url = http://mirrors.aliyun.com/pypi/simple/
[install]
trusted-host=mirrors.aliyun.com

# 在线安装wget / pymysql
[root@localhost nsd2020]# pip3 install wget
[root@localhost nsd2020]# pip3 install pymysql
```

- 本地安装python包。需事先在http://pypi.org上下载

```python
# 本地安装pymysql
[root@localhost software]# tar xf zzg_pypkgs.tar.gz 
[root@localhost software]# cd zzg_pypkgs/
[root@localhost zzg_pypkgs]# pip3 install pymysql_pkgs/* pymysql_pkgs/*.tar.gz
```

- 查看安装的额外的包

```python
[root@localhost ~]# pip3 freeze
asn1crypto==0.24.0
cffi==1.11.5
cryptography==2.4.2
idna==2.7
pycparser==2.19
PyMySQL==0.9.2
six==1.11.0
wget==3.2	
```

## pymysql应用

- 安装mariadb-server或mysql-server

```shell
[root@localhost ~]# yum install -y mariadb-server
[root@localhost ~]# systemctl start mariadb
[root@localhost ~]# systemctl enable mariadb
[root@localhost ~]# mysqladmin password tedu.cn
[root@localhost ~]# mysql -uroot -ptedu.cn
MariaDB [(none)]> CREATE DATABASE nsd2021 DEFAULT CHARSET utf8mb4;


# 查看数据库
MariaDB [(none)]> show databases;
# 切换数据库
MariaDB [(none)]> use nsd2021;
# 查看当前数据库中的表
MariaDB [nsd2021]> show tables;
```

- 操作

```python
# py_mysql.py
import pymysql

# 创建到数据库的连接
conn = pymysql.connect(
    host='localhost',
    user='root',
    password='tedu.cn',
    db='nsd2021',
    charset='utf8mb4',
)

# 创建游标。游标像文件对象一样，通过文件对象对文件读写；通过游标对数据库进行增删改查
cursor = conn.cursor()

# 编写sql语句
create_dep = """CREATE TABLE departments(
id INT PRIMARY KEY AUTO_INCREMENT, dep_name VARCHAR(10)
)"""

# 执行sql语句
cursor.execute(create_dep)
# 确认
conn.commit()
# 关闭
cursor.close()
conn.close()

# pymysql_crud.py
# create / retrieve / update / delete
import pymysql

# 创建到数据库的连接
conn = pymysql.connect(
    host='localhost',
    user='root',
    password='tedu.cn',
    db='nsd2021',
    charset='utf8mb4',
)

# 创建游标。游标像文件对象一样，通过文件对象对文件读写；通过游标对数据库进行增删改查
cursor = conn.cursor()

############################################
# 编写sql语句
# insert1 = "INSERT INTO departments VALUES(%s, %s)"

# 插入一行数据
# cursor.execute(insert1, (1, '人事部'))
# 插入多行数据
# cursor.executemany(insert1, [(2, '运维部'), (3, '开发部'), (4, '测试部'), (5, '财务部')])
############################################
# 查询
# select1 = "SELECT id, dep_name FROM departments"
# cursor.execute(select1)
# result1 = cursor.fetchone()  # 取出一行数据
# result2 = cursor.fetchmany(2)  # 继续取出2行数据
# result3 = cursor.fetchall()  # 继续取出剩余全部数据
# print(result1)
# print('*' * 30)
# print(result2)
# print('*' * 30)
# print(result3)
############################################
# 修改
# update1 = 'UPDATE departments SET dep_name=%s WHERE dep_name=%s'
# cursor.execute(update1, ('人力资源部', '人事部'))
# select2 = 'SELECT id, dep_name FROM departments WHERE dep_name=%s'
# cursor.execute(select2, ('人力资源部',))
# print(cursor.fetchall())
############################################
# 删除
delete1 = 'DELETE FROM departments WHERE id=%s'
cursor.execute(delete1, (5,))
############################################
# 确认
conn.commit()
# 关闭
cursor.close()
conn.close()

```

## 多线程编程

- 程序是存储在磁盘上的可执行文件
- 运行程序后，将会生成一或多个进程。所以可以认为进程是程序的一次执行，是加载到内存中的一系列指令。
- 每个进程都拥有它自己的运行空间。
- 一个进程可以产一到多个线程，线程是CPU的最小调度单位。
- 多个线程共享进程的运行空间。
- linux系统支持多进程和多线程。
- windows系统不支持多进程。
- 多线程编程思路
  - 主线程产生工作线程
  - 工作线程做具体的工作

```python
import threading

def hello():
    print('Hello World!')

if __name__ == '__main__':
    for i in range(3):
        t = threading.Thread(target=hello)  # 创建工作线程
        t.start()  # 启动工作线程，相当于执行target()
```

## paramiko模块

- 它实现了ssh客户端的功能，也可以实现sftp功能

```python
# 安装
[root@localhost day02]# pip3 install paramiko
>>> import paramiko
>>> ssh = paramiko.SSHClient()
# 设置自动接受服务器的主机密钥
>>> ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
# 登陆
>>> ssh.connect('192.168.1.136', username='root', password='redhat', port=22)
# 执行命令
>>> result = ssh.exec_command('id root; id zhangsan')
>>> len(result)
3
# 执行命令的返回值是一个长度为3的元组。这三项分别是输入、输出和错误的类文件对象
>>> stdin, stdout, stderr = ssh.exec_command('id root; id zhangsan')
>>> out = stdout.read()
>>> err = stderr.read()
>>> out
b'uid=0(root) gid=0(root) \xe7\xbb\x84=0(root)\n'
>>> err
b'id: zhangsan: no such user\n'
>>> out.decode()  # bytes转str
'uid=0(root) gid=0(root) 组=0(root)\n'
>>> err.decode()
'id: zhangsan: no such user\n'
# 关闭连接
>>> ssh.close()
```
