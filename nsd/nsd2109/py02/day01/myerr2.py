def get_info(name, age):
    if not 0 < age < 120:
        raise ValueError('年龄超出范围(1~119)')
    print(f'{name}{age}岁了')

def get_info2(name, age):
    # 如果0 < age < 120不成立，一定出现AssertionError
    # 如果0 < age < 120成立，不会发生错误，继续向下执行
    assert 0 < age < 120, '年龄超出范围(1~119)'
    print(f'{name}{age}岁了')

if __name__ == '__main__':
    get_info('牛老师', 20)
    get_info2('凯哥', 220)
