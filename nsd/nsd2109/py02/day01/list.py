import os
#
# folder = '/tmp/mydemo'
#
# for data in os.walk(folder):
#     print(data)

# folder = '/tmp/mydemo'
# for path, subpath, files in os.walk(folder):
#     print(f'{path}:')
#     print(subpath, files)
#     print()

folder = '/tmp/mydemo'
for path, subpath, files in os.walk(folder):
    print(f'{path}:')
    for i in subpath:
        print(i, end='\t')
    for i in files:
        print(i, end='\t')
    print('\n')
