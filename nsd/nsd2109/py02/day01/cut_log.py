import time

log_file = 'mylog.txt'
# 定义9点和12点的时间对象
t9 = time.strptime('2019-05-15 09:00:00', '%Y-%m-%d %H:%M:%S')
t12 = time.strptime('2019-05-15 12:00:00', '%Y-%m-%d %H:%M:%S')

# 在文件的每一行中取出时间，如果时间介于t9和t12之间，则打印
with open(log_file) as fobj:
    for line in fobj:
        t = time.strptime(line[:19], '%Y-%m-%d %H:%M:%S')
        # if t9 <= t < t12:
        #     print(line, end='')
        if t > t12:
            break
        if t >= t9:
            print(line, end='')
