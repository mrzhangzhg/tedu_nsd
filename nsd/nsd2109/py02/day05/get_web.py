import os
import wget
import re

def get_patt(fname, patt):
    result = []
    with open(fname) as fobj:
        for line in fobj:
            m = re.search(patt, line)
            if m:
                result.append(m.group())

    return result

if __name__ == '__main__':
    # 创建下载目录
    dest = '/tmp/163'
    if not os.path.exists(dest):
        os.mkdir(dest)
    # 将网易首页下载到下载目录
    url163 = 'https://www.163.com/'
    fname163 = '/tmp/163/163.html'
    if not os.path.exists(fname163):
        wget.download(url163, fname163)
    # 在网易首页中查找所有的图片url
    img_patt = '(http|https)://[\w./-]+\.(jpg|jpeg|png|gif)'
    img_urls = get_patt(fname163, img_patt)
    # print(img_urls)
    # 下载所有的图片到下载目录
    for url in img_urls:
        wget.download(url, dest)
