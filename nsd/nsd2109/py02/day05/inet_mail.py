from email.mime.text import MIMEText
from email.header import Header
import smtplib
import getpass

body = '''牛教练：
你好。
今天去峡古吗？
'''
# plain表示纯文本格式
message = MIMEText(body, 'plain', 'utf8')  # 设置正文本
message['From'] = Header('zhangzhigang79@qq.com', 'utf8')  # 发件人
message['To'] = Header('zhangzhigang79@163.com', 'utf8')   # 收件人
message['Subject'] = Header('python互联网邮件测试', 'utf8')   # 主题

smtp = smtplib.SMTP()  # 创建一个smtp对象
smtp.connect('smtp.qq.com')    # 连接邮件服务器
# smtp.starttls()    # 服务器要求安全登陆，需要执行它，否则不需要
passwd = getpass.getpass()
smtp.login('zhangzhigang79@qq.com', passwd)  # 密码是授权码
smtp.sendmail(
    'zhangzhigang79@qq.com',
    ['zhangzhigang79@163.com', 'zhangzhigang79@qq.com'],
    message.as_bytes()
)
