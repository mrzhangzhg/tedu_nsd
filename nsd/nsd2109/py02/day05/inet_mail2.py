from email.mime.text import MIMEText
from email.header import Header
import smtplib
import getpass

def inet_mail(body, sender, receivers, subject, host, passwd):
    # plain表示纯文本格式
    message = MIMEText(body, 'plain', 'utf8')  # 设置正文本
    message['From'] = Header(sender, 'utf8')  # 发件人
    message['To'] = Header(receivers[0], 'utf8')   # 收件人
    message['Subject'] = Header(subject, 'utf8')   # 主题

    smtp = smtplib.SMTP()  # 创建一个smtp对象
    smtp.connect(host)    # 连接邮件服务器
    # smtp.starttls()    # 服务器要求安全登陆，需要执行它，否则不需要
    smtp.login(sender, passwd)  # 密码是授权码
    smtp.sendmail(sender, receivers, message.as_bytes())

if __name__ == '__main__':
    sender = 'zhangzhigang79@qq.com'
    receivers = ['zhangzhigang79@qq.com', 'zhangzhigang79@163.com']
    body = "Hello World!\n"
    subject = "py mail"
    host = 'smtp.qq.com'
    passwd = getpass.getpass()
    inet_mail(body, sender, receivers, subject, host, passwd)
