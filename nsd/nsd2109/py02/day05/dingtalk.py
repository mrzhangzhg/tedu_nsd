import requests
import json

url = ''
headers = {'Content-Type': 'application/json; charset=UTF-8'}
# data = {
#     "at": {
#         "atMobiles": [
#             # "180xxxxxx"
#         ],
#         "atUserIds": [
#             # "user123"
#         ],
#         "isAtAll": False
#     },
#     "text": {
#         "content": "好好学习天天向上，我就是我, @180xxxxxx 是不一样的烟火"
#     },
#     "msgtype": "text"
# }
# data = {
#     "msgtype": "markdown",
#     "markdown": {
#         "title": "元旦放假通知",
#         "text": """### 元旦放假通知
# ![](https://p9.itc.cn/images01/20211114/3ff73fa1dbc04ee29f1deca251439af1.png)
# 好好学习天天向上[TMOOC](http://tmooc.cn)"""
#     },
#     "at": {
#         "atMobiles": [
#             # "150XXXXXXXX"
#         ],
#         "atUserIds": [
#             # "user123"
#         ],
#         "isAtAll": False
#     }
# }
# data = {
#     "msgtype": "actionCard",
#     "actionCard": {
#         "title": "打造一间咖啡厅",
#         "text": "好好学习天天向上![screenshot](https://img.alicdn.com/tfs/TB1NwmBEL9TBuNjy1zbXXXpepXa-2400-1218.png) \n #### 乔布斯 20 年前想打造的苹果咖啡厅 \n\n Apple Store 的设计正从原来满满的科技感走向生活化，而其生活化的走向其实可以追溯到 20 年前苹果一个建立咖啡馆的计划",
#         "singleTitle" : "阅读全文",
#         "singleURL" : "https://fanyi.sogou.com"
#     }
# }
# data = {
#     "msgtype": "actionCard",
#     "actionCard": {
#         "title": "乔布斯 20 年前想打造一间苹果咖啡厅，而它正是 Apple Store 的前身",
#         "text": "好好学习天天向上![screenshot](https://img.alicdn.com/tfs/TB1NwmBEL9TBuNjy1zbXXXpepXa-2400-1218.png) \n\n #### 乔布斯 20 年前想打造的苹果咖啡厅 \n\n Apple Store 的设计正从原来满满的科技感走向生活化，而其生活化的走向其实可以追溯到 20 年前苹果一个建立咖啡馆的计划",
#         "btnOrientation": "0",
#         "btns": [
#             {
#                 "title": "内容不错",
#                 "actionURL": "http://tmooc.cn/"
#             },
#             {
#                 "title": "不感兴趣",
#                 "actionURL": "https://fanyi.sogou.com/"
#             }
#         ]
#     }
# }
data = {
    "msgtype": "feedCard",
    "feedCard": {
        "links": [
            {
                "title": "好好学习天天向上",
                "messageURL": "https://www.dingtalk.com/",
                "picURL": "https://img.alicdn.com/tfs/TB1NwmBEL9TBuNjy1zbXXXpepXa-2400-1218.png"
            },
            {
                "title": "时代的火车向前开2",
                "messageURL": "https://www.dingtalk.com/",
                "picURL": "https://p9.itc.cn/images01/20211114/3ff73fa1dbc04ee29f1deca251439af1.png"
            }
        ]
    }
}
r = requests.post(url, headers=headers, data=json.dumps(data))
print(r.json())
