import subprocess

def get_fname():
    '用于获取一个系统中不存在的文件名，它的返回值是一个文件名'
    while 1:
        fname = input('文件名: ')
        result = subprocess.run(
            f'ls {fname}',  # 字符串前加f，字符串中可以在{}中写变量
            shell=True,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE
        )
        if result.returncode != 0:  # 文件不存在则结束循环
            break
        print('文件已存在，请重试。')

    return fname

def get_content():
    '用于获取用户输入的多行文本，作为文件内容，它的返回值是列表'
    content = []  # 定义列表，保存用户输入的行

    print('请输入内容，在单独的一行上输入end结束！')
    while 1:
        line = input('(输入end结束)> ')
        if line == 'end':
            break
        content.append(line + '\n')  # 用户输入的字符串拼接回车再追加到列表

    return content

def wfile(fname, content):
    '把内容content写入到文件fname中'
    with open(fname, 'w') as fobj:
        fobj.writelines(content)

if __name__ == '__main__':
    fname = get_fname()
    content = get_content()
    wfile(fname, content)
