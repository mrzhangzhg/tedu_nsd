src_fname = '/bin/ls'
dst_fname = '/root/list'
src_fobj = open(src_fname, 'rb')
dst_fobj = open(dst_fname, 'wb')

while 1:
    data = src_fobj.read(4096)  # 一次最多读4096字节
    if data == b'':   # 以rb方式读出来的数据是bytes类型
        break
    else:
        dst_fobj.write(data)

src_fobj.close()
dst_fobj.close()
