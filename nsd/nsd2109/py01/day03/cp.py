# 以读方式找开源文件，以写方式打开目录文件
f1 = open('/bin/ls', 'rb')
f2 = open('/root/ls', 'wb')

# 把数据从源文件中取出，并写入目标文件
data = f1.read()
f2.write(data)

# 关闭文件
f1.close()
f2.close()
