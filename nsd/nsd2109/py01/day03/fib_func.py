def mkfib(n):
    fib = [0, 1]

    for i in range(n - 2):
        fib.append(fib[-1] + fib[-2])

    return fib

a = mkfib(10)   # 调用函数
print(a)
b = int(input('长度：'))
c = mkfib(b)
print(c)
