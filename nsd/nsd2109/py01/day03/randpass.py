from random import choice

# 定义可用字符
all_chs = '0123456789qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM'

# 定义用于保存结果的变量
result = ''

# 随机选择一个字符，放到结果变量中，选8次
for i in range(8):
    ch = choice(all_chs)
    result += ch

# 打印结果
print(result)
