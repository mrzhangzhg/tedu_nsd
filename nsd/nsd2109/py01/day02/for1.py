s1 = 'abc'
for i in s1:
    print(i)

users = ['tom', 'jerry']
for name in users:
    print(name)

nums = (10, 20, 30)
for i in nums:
    print(i)

d1 = {'name': 'nb', 'age': 20}
for key in d1:
    print(key, d1[key])

