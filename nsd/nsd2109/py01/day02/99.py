# 打印一个hello
# print('hello')

# 打印3个hello
# for i in range(1, 4):
#     print('hello')   # print默认在结尾打印回车

# 打印3个hello，打印在一行
# for i in range(1, 4):
#     print('hello', end=' ')
# print()   # print默认打印回车

# 把上面代码作为一个整体，执行3遍
# for i in range(1, 4):
#     for j in range(1, 4):
#         print('hello', end=' ')
#     print()

# 改为第1行1个hello，第2行2个hello... ...第i行i个hello
# for i in range(1, 4):
#     for j in range(1, i + 1):
#         print('hello', end=' ')
#     print()

# 将输出的hello改为算式
for i in range(1, 10):
    for j in range(1, i + 1):
        print(j, 'x', i, '=', i * j, sep='', end=' ')
    print()
