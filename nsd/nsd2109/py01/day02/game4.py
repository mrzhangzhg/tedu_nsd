import random

win_list = ['石头剪刀', '剪刀布', '布石头']
all_choice = ['石头', '剪刀', '布']
prompt = '''(0) 石头
(1) 剪刀
(2) 布
请选择(0/1/2): '''
pwin = 0  # 人的计分牌
cwin = 0  # 计算机的计分牌

while pwin < 2 and cwin < 2:   # 人机都没有赢够2次，就继续
    computer = random.choice(all_choice)
    i = int(input(prompt))   # 获取用户输入的下标
    player = all_choice[i]   # 根据下标取出石头剪刀布

    print('你选择:', player, '，计算机选择:', computer)
    if player == computer:
        print('\033[32;1m平局\033[0m')
    elif player + computer in win_list:
        print('\033[31;1mYou WIN!!!\033[0m')
        pwin += 1
    else:
        print('\033[31;1mYou LOSE!!!\033[0m')
        cwin += 1
