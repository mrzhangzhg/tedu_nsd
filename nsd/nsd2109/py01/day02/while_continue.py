# 计算100以内所有偶数之和
sum100 = 0   # 定义用于存储结果的变量
i = 0

while i < 100:
    i += 1
    if i % 2 == 1:
        continue
    else:
        sum100 += i

print(sum100)
