import random

# 定义人获胜的情况
win_list = ['石头剪刀', '剪刀布', '布石头']
# 人机出拳
all_choice = ['石头', '剪刀', '布']
computer = random.choice(all_choice)
player = input('请出拳(石头/剪刀/布): ')

# 输出人机选择
print('你选择:', player, '，计算机选择:', computer)
# 判断胜负
if player == computer:
    print('\033[32;1m平局\033[0m')
elif player + computer in win_list:
    print('\033[31;1mYou WIN!!!\033[0m')
else:
    print('\033[31;1mYou LOSE!!!\033[0m')
