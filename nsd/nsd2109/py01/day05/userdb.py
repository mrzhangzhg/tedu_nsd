import getpass

userdb = {}

def register():
    '用于注册'
    username = input('用户名: ').strip()
    if len(username) == 0:
        print('用户名不能为空')
    elif username in userdb:
        print('用户已存在。')
    else:
        password = input('密码: ')
        userdb[username] = password

def login():
    '用于登陆'
    username = input('用户名: ').strip()
    password = getpass.getpass('密码: ')
    if userdb.get(username) == password:
        print('登陆成功')
    else:
        print('登陆失败')

def show_menu():
    '用于显示菜单'
    prompt = '''(0) 注册
(1) 登陆
(2) 退出
请选择(0/1/2): '''
    while 1:
        choice = input(prompt).strip()
        if choice not in ['0', '1', '2']:
            print('无效的输入，请重试。')
            continue

        if choice == '2':
            print('Bye-bye')
            break

        if choice == '0':
            register()
        else:
            login()

if __name__ == '__main__':
    show_menu()
