stack = []

def push_it():
    '用于压栈'
    data = input('数据: ').strip()
    if len(data) == 0:
        print('没有获取到数据')
    else:
        stack.append(data)

def pop_it():
    '用于出栈'
    if len(stack) == 0:
        print('栈已经为空')
    else:
        data = stack.pop()
        print(f'从栈中弹出了: {data}')

def view_it():
    '用于查询'
    print(stack)

def show_menu():
    '用于显示菜单，根据用户选择调用相关函数'
    prompt = '''(0) 压栈
(1) 出栈
(2) 查询
(3) 退出
请选择(0/1/2/3): '''
    while 1:
        choice = input(prompt).strip()  # 去除输入字符串两端的空格
        if choice not in ['0', '1', '2', '3']:
            print('无效的输入，请重试。')
            continue

        if choice == '3':
            print('Bye-bye')
            break

        if choice == '0':
            push_it()
        elif choice == '1':
            pop_it()
        else:
            view_it()

if __name__ == '__main__':
    show_menu()
