# NOSQL day03

<p align="right">
  By <a href="https://www.jianshu.com/u/665c84e77f9c">Mr.张志刚</a>
</p>

[toc]

## redis主从复制

### 一主一从结构

```mermaid
graph LR
redis1(redis1:192.168.1.11)-->redis2(redis2:192.168.1.12)
```



- 恢复redis1和redis2的redis默认配置

```shell
[root@redis1 ~]# vim /etc/redis/6379.conf 
# cluster-enabled yes
# cluster-config-file nodes-6379.conf
# cluster-node-timeout 5000

[root@redis1 ~]# service redis_6379 stop
[root@redis1 ~]# rm -f /var/lib/redis/6379/*
[root@redis1 ~]# service redis_6379 start

[root@redis2 ~]# vim /etc/redis/6379.conf 
# cluster-enabled yes
# cluster-config-file nodes-6379.conf
# cluster-node-timeout 5000

[root@redis2 ~]# service redis_6379 stop
[root@redis2 ~]# rm -f /var/lib/redis/6379/*
[root@redis2 ~]# service redis_6379 start
```

- 默认redis都是主服务器，所以无需配置

```shell
[root@redis1 ~]# redis-cli 
127.0.0.1:6379> INFO replication
# Replication
role:master
connected_slaves:0
master_replid:a839f0060f5e8d9894a1fafeeea973c3c52e5e71
master_replid2:0000000000000000000000000000000000000000
master_repl_offset:0
second_repl_offset:-1
repl_backlog_active:0
repl_backlog_size:1048576
repl_backlog_first_byte_offset:0
repl_backlog_histlen:0
```

- 配置redis2(192.168.1.12)为redis1的从服务器

  - 通过命令进行配置（临时生效）

  ```shell
  [root@redis2 ~]# redis-cli 
  127.0.0.1:6379> SLAVEOF 192.168.1.11 6379
  OK
  
  127.0.0.1:6379> INFO replication
  # Replication
  role:slave
  master_host:192.168.1.11
  master_port:6379
  master_link_status:up
  master_last_io_seconds_ago:3
  master_sync_in_progress:0
  slave_repl_offset:14
  slave_priority:100
  slave_read_only:1
  connected_slaves:0
  master_replid:7a4326738b63379fc5def4177a1ba6fff33d0a98
  master_replid2:0000000000000000000000000000000000000000
  master_repl_offset:14
  second_repl_offset:-1
  repl_backlog_active:1
  repl_backlog_size:1048576
  repl_backlog_first_byte_offset:1
  repl_backlog_histlen:14
  ```

  - 永久生效

  ```shell
  [root@redis2 ~]# vim /etc/redis/6379.conf 
  slaveof 192.168.1.11 6379
  ```

- 测试配置

```shell
# 在主服务器上添加数据
[root@redis1 ~]# redis-cli 
127.0.0.1:6379> SET name tom
OK
127.0.0.1:6379> SET email tom@tedu.cn
OK

# 在从服务器上查看同步的数据
[root@redis2 ~]# redis-cli 
127.0.0.1:6379> KEYS *
1) "name"
2) "email"

127.0.0.1:6379> MGET name email
1) "tom"
2) "tom@tedu.cn"
```

### 配置带验证的主从复制

基于以上示例【一主一从结构】，配置认证

```mermaid
graph LR
redis1(redis1:192.168.1.11)-->redis2(redis2:192.168.1.12)
```



- 配置主服务器redis1的连接密码为`tedu.cn`

```mysql
# 设置密码
[root@redis1 ~]# vim +501 /etc/redis/6379.conf
requirepass tedu.cn

# 修改服务脚本
[root@redis1 ~]# vim +43  /etc/init.d/redis_6379
            $CLIEXEC -p $REDISPORT -a tedu.cn shutdown
            
# 重启服务
[root@redis1 ~]# service redis_6379 restart
Stopping ...
Redis stopped
Starting Redis server...
```

- 配置从服务器

```mysql
# 修改配置文件，设置主服务器连接密码
[root@redis2 ~]# vim +289 /etc/redis/6379.conf
masterauth tedu.cn

# 重启服务
[root@redis2 ~]# service redis_6379 restart
Stopping ...
Redis stopped
Starting Redis server...
```

- 在从服务器本机连接服务，查看复制信息

```mysql
[root@redis2 ~]# redis-cli
127.0.0.1:6379> info replication
# Replication
role:slave             				# 角色为从服务器
master_host:192.168.1.11			# 主服务器地址
master_port:6379						 # 主服务器端口
master_link_status:up					# 到主服务器的连接状态
master_last_io_seconds_ago:2
master_sync_in_progress:0
slave_repl_offset:70
slave_priority:100
slave_read_only:1
connected_slaves:0
master_replid:94e2bea7fc81fa71b1193df29b0984190c01bacc
master_replid2:0000000000000000000000000000000000000000
master_repl_offset:70
second_repl_offset:-1
repl_backlog_active:1
repl_backlog_size:1048576
repl_backlog_first_byte_offset:1
repl_backlog_histlen:70
```

## 哨兵服务

- 监视master服务器

- 发现master宕机后，将从服务器升级为主服务器

### 配置哨兵服务

```mermaid
graph LR
sentinel1(192.168.1.19)-->redis1
subgraph cluster
redis1(redis1:192.168.1.11)-->redis2(redis2:192.168.1.12)
end
```



基于以上示例【配置带验证的主从复制】，配置哨兵服务

- 配置哨兵服务

```mysql
# 在redis服务器(如redis1)上，拷贝哨兵程序到哨兵服务器
[root@redis1 ~]# scp -r /usr/local/redis 192.168.1.19:/usr/local

# 在sentinel1上，将redis命令目录添加至PATH环境变量
[root@sentinel1 ~]# echo 'export PATH=$PATH:/usr/local/redis/bin' >> /etc/bashrc
[root@sentinel1 ~]# source /etc/bashrc

# 创建哨兵配置文件
[root@sentinel1 ~]# vim /etc/sentinel.conf
sentinel monitor redis1 192.168.1.11 6379 1   # 监视主服务器
bind 0.0.0.0    													  # 哨兵服务运行地址
sentinel auth-pass redis1 tedu.cn						 # 连接主服务器的密码
```

- 启动哨兵服务

```mysql
[root@sentinel1 ~]# redis-sentinel /etc/sentinel.conf
30557:X 12 May 10:12:44.026 # oO0OoO0OoO0Oo Redis is starting oO0OoO0OoO0Oo
30557:X 12 May 10:12:44.026 # Redis version=4.0.8, bits=64, commit=00000000, modified=0, pid=30557, just started
30557:X 12 May 10:12:44.026 # Configuration loaded
30557:X 12 May 10:12:44.028 * Increased maximum number of open files to 10032 (it was originally set to 1024).
                _._
           _.-``__ ''-._
      _.-``    `.  `_.  ''-._           Redis 4.0.8 (00000000/0) 64 bit
  .-`` .-```.  ```\/    _.,_ ''-._
 (    '      ,       .-`  | `,    )     Running in sentinel mode
 |`-._`-...-` __...-.``-._|'` _.-'|     Port: 26379
 |    `-._   `._    /     _.-'    |     PID: 30557
  `-._    `-._  `-./  _.-'    _.-'
 |`-._`-._    `-.__.-'    _.-'_.-'|
 |    `-._`-._        _.-'_.-'    |           http://redis.io
  `-._    `-._`-.__.-'_.-'    _.-'
 |`-._`-._    `-.__.-'    _.-'_.-'|
 |    `-._`-._        _.-'_.-'    |
  `-._    `-._`-.__.-'_.-'    _.-'
      `-._    `-.__.-'    _.-'
          `-._        _.-'
              `-.__.-'

30557:X 12 May 10:12:44.033 # WARNING: The TCP backlog setting of 511 cannot be enforced because /proc/sys/net/core/somaxconn is set to the lower value of 128.
30557:X 12 May 10:12:44.049 # Sentinel ID is 40460aaa4df5543a000bf8f464c6698a712d2697
30557:X 12 May 10:12:44.049 # +monitor master redis1 192.168.1.11 6379 quorum 1
30557:X 12 May 10:12:44.051 * +slave slave 192.168.1.12:6379 192.168.1.12 6379 @ redis1 192.168.1.11 6379
```

- 测试配置

```mysql
# 停止主服务器redis1的redis服务
[root@redis1 ~]# service redis_6379 stop
Stopping ...
Redis stopped

# 在redis2上查看redis服务状态
[root@redis2 ~]# redis-cli
127.0.0.1:6379> info replication
# Replication
role:master              # 已经变为主服务器
connected_slaves:0
master_replid:da33c07e16dc1b90d14a20004e9f5d9b3a7b9081
master_replid2:94e2bea7fc81fa71b1193df29b0984190c01bacc
master_repl_offset:10054
second_repl_offset:8936
repl_backlog_active:1
repl_backlog_size:1048576
repl_backlog_first_byte_offset:1
repl_backlog_histlen:10054
```

- 启动redis1，查看redis1信息

```mysql
[root@redis1 ~]# service redis_6379 start
Starting Redis server...

[root@redis1 ~]# redis-cli -a tedu.cn
127.0.0.1:6379> info replication
# Replication
role:slave             				# 角色为从服务器
master_host:192.168.1.12       # 主服务器地址
master_port:6379               # 主服务器端口
master_link_status:up          # 到主服务器的连接状态
master_last_io_seconds_ago:1
master_sync_in_progress:0
slave_repl_offset:62014
slave_priority:100
slave_read_only:1
connected_slaves:0
master_replid:da33c07e16dc1b90d14a20004e9f5d9b3a7b9081
master_replid2:0000000000000000000000000000000000000000
master_repl_offset:62014
second_repl_offset:-1
repl_backlog_active:1
repl_backlog_size:1048576
repl_backlog_first_byte_offset:61430
repl_backlog_histlen:585
```



## 数据持久化

### RedisDataBase(RDB)

- 数据持久化方式之一

- 数据持久化默认方式

- 按照指定时间间隔，将内存中的数据集快照写入硬盘

- 通过RDB进行备份还原，只要拷贝RDB文件即可
- 数据从内存保存到磁盘的频率设置

```mysql
save 900 1            		//15分钟内有1个key改变即存盘  
save 300 10          		  //5分钟内有10个key改变即存盘
save 60 10000      		    //1分钟内有10000个key改变即存盘
```

- 也可以进行手动存盘

```mysql
save       	   //阻塞写存盘
bgsave  			//不阻塞写存盘
```

#### RDB的优缺点

- 优点：
  - 高性能的持久化实现：创建一个子进程来执行持久化，先将数据写入临时文件，持久化过程结束后，再用这个临时文件替换上次持久化好的文件
  - 过程中主进程不做任何IO操作
  - 比较适合大规模数据恢复，且对数据完整性要求不是非常高的场合

- 缺点：
  - 意外宕机时，丢失最后一次持久化的所有数据

### 配置RDB

- 配置RDB参数

```mysql
# [root@redis7 ~]# vim /etc/redis/6379.conf
dbfilename dump.rdb
save 900 1
#save 300 10
save 120 10
save 60 10000
```

- 清空rdb数据

```mysql
# 停止redis服务
[root@redis7 ~]# service redis_6379 stop
Stopping ...
Redis stopped

# 删除数据
[root@redis7 ~]# rm -rf /var/lib/redis/6379/*

# 启动服务
[root@redis7 ~]# service redis_6379 start
Starting Redis server...

# 查看数据文件，因为没有存储过任何数据，所以不存在RDB文件
[root@redis7 ~]# ls /var/lib/redis/6379/
```

- 存储数据

```mysql
# 120秒内添加10个数据
[root@redis7 ~]# redis-cli
127.0.0.1:6379> MSET k1 v1 k2 v2 k3 v3 k4 v4 k5 v5
OK
127.0.0.1:6379> MSET k6 v6 k7 v7 k8 v8 k9 v9 k10 v10
OK

127.0.0.1:6379> KEYS *
 1) "k10"
 2) "k2"
 3) "k3"
 4) "k9"
 5) "k8"
 6) "k4"
 7) "k5"
 8) "k7"
 9) "k6"
10) "k1"
127.0.0.1:6379> exit

[root@redis7 ~]# ls /var/lib/redis/6379/
dump.rdb
```

- 验证备份还原

```mysql
# 备份数据文件
[root@redis7 ~]# cp /var/lib/redis/6379/dump.rdb ~/

# 模拟误删除数据
[root@redis7 ~]# redis-cli
127.0.0.1:6379> FLUSHALL
OK
127.0.0.1:6379> KEYS *
(empty list or set)
127.0.0.1:6379> exit

# 恢复数据
[root@redis7 ~]# service redis_6379 stop
Stopping ...
Redis stopped

[root@redis7 ~]# cp dump.rdb /var/lib/redis/6379/   # 用备份文件覆盖目标文件
cp: overwrite ‘/var/lib/redis/6379/dump.rdb’? y

# 启动服务并验证
[root@redis7 ~]# service redis_6379 start
Starting Redis server...
[root@redis7 ~]# redis-cli
127.0.0.1:6379> KEYS *
 1) "k6"
 2) "k3"
 3) "k4"
 4) "k1"
 5) "k8"
 6) "k7"
 7) "k9"
 8) "k5"
 9) "k10"
10) "k2"
```

### Append Only File(AOF)

- 追加方式记录写操作的文件
- 记录redis服务所有写操作
- 不断的将新的写操作，追加到文件的末尾
- 默认没有启用
- 使用cat命令可以查看文件内容

#### AOF文件记录写操作的方式

- appendfsync always：时时记录，并完成磁盘同步
- appendfsync everysec ：每秒记录一次，并完成磁盘同步
- appendfsync no：写入aof ，不执行磁盘同步

#### AOF优点与缺点

- 优点：
  - 可以灵活设置持久化方式
  - 出现意外宕机时，仅可能丢失1秒的数据

- 缺点：
  - 持久化文件的体积通常会大于RDB方式
  - 执行fsync策略时的速度可能会比RDB方式慢

### 配置AOF

- 修改配置文件

```mysql
[root@redis7 ~]# redis-cli
127.0.0.1:6379> CONFIG SET appendonly yes
OK
127.0.0.1:6379> CONFIG REWRITE
OK
127.0.0.1:6379> SAVE
OK
127.0.0.1:6379> exit

# 查看AOF文件
[root@redis7 ~]# ls /var/lib/redis/6379/
appendonly.aof  dump.rdb
```

- 验证备份还原

```mysql
# 备份AOF文件
[root@redis7 ~]# cp /var/lib/redis/6379/appendonly.aof ~/

# 删除数据
[root@redis7 ~]# redis-cli
127.0.0.1:6379> KEYS *
 1) "k6"
 2) "k3"
 3) "k4"
 4) "k1"
 5) "k8"
 6) "k7"
 7) "k9"
 8) "k5"
 9) "k10"
10) "k2"
127.0.0.1:6379> FLUSHALL
OK
127.0.0.1:6379> KEYS *
(empty list or set)
127.0.0.1:6379> exit

# 恢复数据
[root@redis7 ~]# service redis_6379 stop
Stopping ...
Redis stopped
[root@redis7 ~]# rm -rf /var/lib/redis/6379/*
[root@redis7 ~]# cp appendonly.aof /var/lib/redis/6379/

# 启动服务并验证
[root@redis7 ~]# service redis_6379 start
Starting Redis server...
[root@redis7 ~]# redis-cli
127.0.0.1:6379> KEYS *
 1) "k10"
 2) "k3"
 3) "k2"
 4) "k9"
 5) "k1"
 6) "k6"
 7) "k5"
 8) "k8"
 9) "k7"
10) "k4"
```

