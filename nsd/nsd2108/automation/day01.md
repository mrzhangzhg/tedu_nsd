# day01

[toc]

## git

- 就是为目录打快照。将来可以回到某一个历史记录点。
- 是一个分布式的代码管理工具
- 可以是C/S架构，也就是Client/Server
- 只要有适当的权限，每个客户端都可以下载或上传数据到服务器

## git的工作原理

- git可以在单机环境下运行，用于为目录打快照。
- git重要的三个工作区域：
  - 工作区：写代码的目录。就是项目代码存放的目录。
  - 暂存区：工作区与版本库之间的缓冲地带。位置是`.git/index`
  - 版本库：工作区快照存放的目录。在工作区下，名为`.git`的目录

## git环境准备

- automation部分的环境都采用RHEL8系统

```shell
[root@zzgrhel8 ~]# clone-vm8     # 创建RHEL8的脚本
Enter VM number: 1               # 输入节点的编号
[root@zzgrhel8 ~]# clone-vm8
Enter VM number: 2
[root@zzgrhel8 ~]# virsh list --all    # 查看所有的KVM虚拟机
 Id   名称           状态
------------------------------
 -    tedu8_node01   关闭
 -    tedu8_node02   关闭
[root@zzgrhel8 ~]# virt-manager  # 打开图形控制台


# 配置第一台机器为程序员所使用的机器
hostnamectl set-hostname develop   # 改主机名
nmcli connection modify eth0 ipv4.addresses 192.168.4.10/24  # 改IP
nmcli connection modify eth0 ipv4.method manual   # 改手工配置地址
nmcli connection down eth0     # 禁用网卡
nmcli connection up eth0       # 激活网卡
echo a | passwd --stdin root   # 改root密码为a
sed -i '/^SELINUX=/s/enforcing/disabled/' /etc/selinux/config  # 禁用selinux
setenforce 0      # 立即禁用selinux
systemctl stop firewalld.service       # 关防火墙
systemctl disable firewalld.service

# 配置第二台机器为git服务器
hostnamectl set-hostname git
nmcli connection modify eth0 ipv4.addresses 192.168.4.20/24
nmcli connection modify eth0 ipv4.method manual
nmcli connection down eth0 
nmcli connection up eth0
echo a | passwd --stdin root
sed -i '/^SELINUX=/s/enforcing/disabled/' /etc/selinux/config
setenforce 0
systemctl stop firewalld.service
systemctl disable firewalld.service
```

### 服务拓扑

```mermaid
graph LR
client(client)--git clone/push/pull-->gitserver(gitserver)
```

### 配置服务器

- 在192.168.4.20上安装软件

```shell
# 配置yum
[root@git ~]# vim /etc/yum.repos.d/rhel8.repo 
[app]
name=app
baseurl=ftp://192.168.4.254/AppStream
enabled=1
gpgcheck=0
[base]
name=base
baseurl=ftp://192.168.4.254/BaseOS
enabled=1
gpgcheck=0

[root@git ~]# yum repolist

# 安装git
[root@git ~]# yum install -y git

# 查看git版本
[root@git ~]# git --version
git version 2.18.2
```

> 附：`alt + 数字 `可在多个终端标签页切换。创建新标签页是`ctrl+shift+t`，文字放大是`ctrl+shift+ +`

- 在服务器上创建项目目录

```shell
# 在服务器上创建一个空仓库
[root@git ~]# mkdir -p /var/lib/git    # 创建git的数据目录，用于存储所有项目

# git init用于初始化项目目录，项目叫project，该目录自动生成。--bare表示空仓库
[root@git ~]# git init /var/lib/git/project --bare
[root@git ~]# ls /var/lib/git/project/   # 查看项目目录
HEAD      config       hooks  objects
branches  description  info   refs
```

## git客户端

- 环境准备[192.168.4.10]

```shell
# 安装git
[root@develop ~]# yum install -y git
```

- 下载git项目
  - 下载可以使用ssh协议或Http协议或https协议，取决于服务器配置

```shell
# 以ssh的方式，下载服务器的project项目
[root@develop ~]# git clone root@192.168.4.20:/var/lib/git/project
[root@develop ~]# ls    # 本地多了一个project目录
anaconda-ks.cfg  project
[root@develop ~]# ls -A project/   # 因为该项目是空的，所以只有一个版本库目录
.git
```

- git相关子命令

```shell
# 查看远程仓库信息，远程仓库名默认是origin
[root@develop project]# git remote -v
origin	root@192.168.4.20:/var/lib/git/project (fetch)   # 下载地址
origin	root@192.168.4.20:/var/lib/git/project (push)    # 上传地址

# 配置信息，如名字、email等
[root@develop project]# git config --global user.name Zhangzhg
[root@develop project]# git config --global user.email zhangzg@tedu.cn
# 查看配置信息
[root@develop project]# git config --list
[root@develop project]# cat ~/.gitconfig 
[user]
	name = Zhangzhg
	email = zhangzg@tedu.cn
```

### git操作

- git操作流程

```mermaid
graph LR
work(工作区)--git add-->stage(暂存区)--git commit-->git(版本库)
```

```shell
# 在工作区创建文件
[root@develop project]# echo "init data" > init.txt
[root@develop project]# mkdir demo
[root@develop project]# cp /etc/hosts demo/
[root@develop project]# ls
demo  init.txt

# 查看状态。
[root@develop project]# git status  # 查看状态，将会发现有两个未跟踪的文件
```

- git工作区下文件的状态：
  - 未跟踪：工作区下出现的新文件，没有被存放到暂存区或版本库中的状态
  - 已跟踪：
    - 已暂存：工作区的文件被复制一份到暂存区
    - 未修改：暂存区的数据被放至版本库，且内容与工作区中的文件相同
    - 已修改：文件被快照至版本库中之后，工作区中的文件做了改动

```shell
# 将工作区中的文件保存至暂存区
[root@develop project]# git add .   # 将当前工作区中的所有文件保存至暂存区
[root@develop project]# git status  # 将会看到两个新被跟踪的文件

# 将暂存区的数据确认至版本库。-m后面写入说明信息
[root@develop project]# git commit -m "init data"
[root@develop project]# git status   # 将显示干净的工作区
```

- 上传项目到git仓库服务器

```shell
# 配置推送到服务器的方法是simple。simple表示推送代码时，把本地分支推送到服务器同名的分支。
[root@develop project]# git config --global push.default simple

# 推送本地数据到服务器
[root@develop project]# git push
root@192.168.4.20's password:     # 此处输入root密码
```

## 阶段练习

- 本练习不涉及服务器

1. 在家目录下创建一个项目目录，名为myproject

```shell
[root@develop project]# cd
[root@develop ~]# git init myproject 
Initialized empty Git repository in /root/myproject/.git/
[root@develop ~]# ls -A myproject/
.git
```

2. 在项目目录下创建几个文件，自己定义

```shell
[root@develop ~]# cd myproject/
[root@develop myproject]# cp /etc/hosts /etc/passwd .
[root@develop myproject]# ls
hosts  passwd
```

3. 将工作区的文件保存到暂存区

```shell
[root@develop myproject]# git add .
```

4. 将暂存区内容，确认至版本库

```shell
[root@develop myproject]# git commit -m "myproject 1.0"
```

- 安装中文支持

```shell
# 安装中文支持的包即可
[root@develop ~]# yum install -y glibc-langpack-zh-2.28-101.el8.x86_64

# 如果仍然是英文，则：
[root@develop ~]# localectl set-locale zh_CN.utf8
```

## git操作（续）

- 查看日志

```shell
[root@develop ~]# cd project/
[root@develop project]# git log   # 查看历史日志
commit dbbda9af26f321d5e05012fcd5ae71658f933368 (HEAD -> master, origin/master)
Author: Zhangzhg <zhangzg@tedu.cn>
Date:   Fri Oct 22 14:24:37 2021 +0800

    init data

[root@develop project]# git log --pretty=oneline   # 在一行显示简要日志信息
dbbda9af26f321d5e05012fcd5ae71658f933368 (HEAD -> master, origin/master) init data

[root@develop project]# git log --oneline    # 更简短的日志
dbbda9a (HEAD -> master, origin/master) init data

[root@develop project]# git reflog    # 显示日志指针
dbbda9a (HEAD -> master, origin/master) HEAD@{0}: commit (initial): init data
```

## windows下的git软件

- git官方地址：https://www.git-scm.com/
- windows下的git：https://www.git-scm.com/download/win
- 安装完成后，命令与linux下一样

## HEAD指针

- HEAD指针是一个可以在任何分支或版本之间移动的指针
- HEAD指针指向哪，就可以看到当时的数据
- 验证HEAD指针

```shell
# 1. 打多个快照
[root@develop project]# echo "new file" > new.txt
[root@develop project]# git add .
[root@develop project]# git commit -m "add new.txt"

[root@develop project]# echo "first" > 1.txt
[root@develop project]# git add .
[root@develop project]# git commit -m "add 1.txt"

[root@develop project]# echo "second" > 2.txt
[root@develop project]# git add .
[root@develop project]# git commit -m "add 2.txt"

[root@develop project]# echo "third" > 3.txt
[root@develop project]# git add .
[root@develop project]# git commit -m "add 3.txt"

[root@develop project]# echo "123" > 4.txt
[root@develop project]# git add .
[root@develop project]# git commit -m "add 4.txt"

[root@develop project]# echo "456" > 5.txt
[root@develop project]# git add .
[root@develop project]# git commit -m "add 5.txt"

[root@develop project]# echo "789" > 6.txt
[root@develop project]# git add .
[root@develop project]# git commit -m "add 6.txt"

[root@develop project]# ls
1.txt  3.txt  5.txt  demo      new.txt
2.txt  4.txt  6.txt  init.txt

# 2. 查看git版本信息（每一次commit都叫产生一个版本）
# HEAD@{0}表示当HEAD指针位置，HEAD@{1}是上一次HEAD指针所在位置
[root@develop project]# git reflog 
af29414 (HEAD -> master) HEAD@{0}: commit: add 6.txt
6aa744c HEAD@{1}: commit: add 5.txt
bc80bfa HEAD@{2}: commit: add 4.txt
5ca23ce HEAD@{3}: commit: add 3.txt
1cffecb HEAD@{4}: commit: add 2.txt
8a7d795 HEAD@{5}: commit: add 1.txt
2f34350 HEAD@{6}: commit: add new.txt
dbbda9a (origin/master) HEAD@{7}: commit (initial): init data

# 3. 移动HEAD指针到 5ca23ce add 3.txt
[root@develop project]# git reset --hard 5ca23ce
HEAD 现在位于 5ca23ce add 3.txt
[root@develop project]# ls
1.txt  2.txt  3.txt  demo  init.txt  new.txt

# 4. 切换到最新状态
[root@develop project]# git reflog 
5ca23ce (HEAD -> master) HEAD@{0}: reset: moving to 5ca23ce
af29414 HEAD@{1}: commit: add 6.txt
6aa744c HEAD@{2}: commit: add 5.txt
bc80bfa HEAD@{3}: commit: add 4.txt
5ca23ce (HEAD -> master) HEAD@{4}: commit: add 3.txt
1cffecb HEAD@{5}: commit: add 2.txt
8a7d795 HEAD@{6}: commit: add 1.txt
2f34350 HEAD@{7}: commit: add new.txt
dbbda9a (origin/master) HEAD@{8}: commit (initial): init data

[root@develop project]# git reset --hard af29414
HEAD 现在位于 af29414 add 6.txt
[root@develop project]# ls
1.txt  3.txt  5.txt  demo      new.txt
2.txt  4.txt  6.txt  init.txt
```

## 分支管理

- 分支可以允许开发分为多条主线进行，每条主线互不影响
- git默认有一个名为master的分支。该分支与其他手工建立的分支，没有什么不同。
- 在使用的时候，开发时，建立分支，写代码；分支代码功能完善后，将其汇入主干master分支，进行版本发布。

### 分支操作

```shell
# 查看分支
[root@develop project]# git branch    # 查看分支
* master    # 只有一个默认分支

# 创建名为news的新分支
[root@develop project]# git branch news
[root@develop project]# git branch     # 查看分支
* master           # *号标识当前所在分支是master
  news

[root@develop project]# git branch bbs
[root@develop project]# git branch 
  bbs
* master
  news

# 切换到分支news，在新分支中看到的内容，此时与master一致
[root@develop project]# git checkout news
切换到分支 'news'
[root@develop project]# git branch 
  bbs
  master
* news
[root@develop project]# ls
1.txt  3.txt  5.txt  demo      new.txt
2.txt  4.txt  6.txt  init.txt

# 在news分支中继续编写代码
[root@develop project]# echo "my test" > 7.txt
[root@develop project]# echo "new file" > 8.txt
[root@develop project]# git add .
[root@develop project]# git commit -m "add 7.txt 8.txt"
[root@develop project]# ls
1.txt  3.txt  5.txt  7.txt  demo      new.txt
2.txt  4.txt  6.txt  8.txt  init.txt

# 在其他分支上，看不到news分支上的两个新文件
[root@develop project]# git checkout master
[root@develop project]# ls    # 没有7.txt 8.txt
1.txt  3.txt  5.txt  demo      new.txt
2.txt  4.txt  6.txt  init.txt
[root@develop project]# git checkout bbs
[root@develop project]# ls    # 没有7.txt 8.txt
1.txt  3.txt  5.txt  demo      new.txt
2.txt  4.txt  6.txt  init.txt


# 合并news分支到主干master
[root@develop project]# git checkout master   # 必须切换到master分支
[root@develop project]# git merge news -m "merge news branch"
[root@develop project]# ls   # master分支中已经可以看到7.txt 和8.txt
1.txt  3.txt  5.txt  7.txt  demo      new.txt
2.txt  4.txt  6.txt  8.txt  init.txt

# 如果分支确实不需要了，可以删除。
[root@develop project]# git branch -d news   # 删除news分支
[root@develop project]# git branch 
  bbs
* master
```

- 解决分支冲突。在不同的分支中，相同文件的内容不一样，合并时无法自动合并。则需要手工解决冲突。

```shell
# 在bbs分支中创建a.txt
[root@develop project]# git checkout bbs
[root@develop project]# echo AAA > a.txt
[root@develop project]# git add .
[root@develop project]# git commit -m "add AAA to a.txt"

# 在master分支中创建a.txt，内容与bbs分支中的不同
[root@develop project]# git checkout master
[root@develop project]# echo BBB > a.txt
[root@develop project]# git add .
[root@develop project]# git commit -m "add BBB to a.txt"

# 因为a.txt中内容不一样，所以合并bbs到master时，将会出现冲突
[root@develop project]# git merge bbs -m "merge bbs"
自动合并 a.txt
冲突（添加/添加）：合并冲突于 a.txt
自动合并失败，修正冲突然后提交修正的结果。

# 手工修正a.txt的冲突，然后提交
[root@develop project]# vim a.txt    # 内容自己决定
BBB
AAA
[root@develop project]# git add .
[root@develop project]# git commit -m "resolve a.txt"
[root@develop project]# git status    # 此时为干净的工作区，合并成功
```









