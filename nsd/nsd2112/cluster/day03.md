# day03

[toc]

## ceph

- ceph被称作面向未来的存储
- 中文手册：
  - https://access.redhat.com/documentation/zh-cn/red_hat_ceph_storage/5/html/architecture_guide/index
  - http://docs.ceph.org.cn/

- ceph可以实现的存储方式：
  - 块存储：提供像普通硬盘一样的存储，为使用者提供“硬盘”
  - 文件系统存储：类似于NFS的共享方式，为使用者提供共享文件夹
  - 对象存储：像百度云盘一样，需要使用单独的客户端

- ceph还是一个分布式的存储系统，非常灵活。如果需要扩容，只要向ceph集中增加服务器即可。
- ceph存储数据时采用多副本的方式进行存储，生产环境下，一个文件至少要存3份。ceph默认也是三副本存储。

### ceph的构成

- **Ceph OSD 守护进程：** Ceph OSD 用于存储数据。此外，Ceph OSD 利用 Ceph 节点的 CPU、内存和网络来执行数据复制、纠删代码、重新平衡、恢复、监控和报告功能。存储节点有几块硬盘用于存储，该节点就会有几个osd进程。
- **Ceph Mon监控器：** Ceph Mon维护 Ceph 存储集群映射的主副本和 Ceph 存储群集的当前状态。监控器需要高度一致性，确保对Ceph 存储集群状态达成一致。维护着展示集群状态的各种图表，包括监视器图、 OSD 图、归置组（ PG ）图、和 CRUSH 图。
- **MDSs**: Ceph 元数据服务器（ MDS ）为 Ceph 文件系统存储元数据。
- RGW：对象存储网关。主要为访问ceph的软件提供API接口。

### 搭建ceph集群

- 节点准备

| 主机名  | IP地址          |
| ------- | --------------- |
| node1   | 192.168.4.11/24 |
| node2   | 192.168.4.12/24 |
| node3   | 192.168.4.13/24 |
| client1 | 192.168.4.10/24 |

```shell
# 创建4台虚拟机
[root@zzgrhel8 ~]# clone-vm7
```

- 关机，为node1-node3各额外再添加2块20GB的硬盘

```shell
# 查看添加的硬盘，注意硬盘名字
[root@node1 ~]# lsblk 
NAME   MAJ:MIN RM SIZE RO TYPE MOUNTPOINT
vda    253:0    0  30G  0 disk 
└─vda1 253:1    0  30G  0 part /
vdb    253:16   0  20G  0 disk 
vdc    253:32   0  20G  0 disk 
```

- 配置yum源。除了系统yum源以外，还需要配置ceph的yum源

```shell
# 在真机上提供yum源。真机务必关掉防火墙
[root@zzgrhel8 ~]# yum install -y vsftpd
[root@zzgrhel8 ~]# systemctl start vsftpd
[root@zzgrhel8 ~]# mkdir /var/ftp/ceph
[root@zzgrhel8 ~]# cp /linux-soft/2/ceph10.iso /iso/
[root@zzgrhel8 ~]# vim /etc/fstab   # 追加一行如下
/iso/ceph10.iso /var/ftp/ceph   iso9660 defaults,loop   0 0
[root@zzgrhel8 ~]# mount -a
[root@zzgrhel8 ~]# df -h /var/ftp/ceph/
文件系统        容量  已用  可用 已用% 挂载点
/dev/loop3      284M  284M     0  100% /var/ftp/ceph
[root@zzgrhel8 ~]# ls /var/ftp/ceph/
EULA  MON  README                      Tools
GPL   OSD  RPM-GPG-KEY-redhat-release  TRANS.TBL


# 在node1-3节点上配置yum
[root@node1 ~]# cat /etc/yum.repos.d/local.repo 
[local_repo]
name=CentOS-$releasever - Base
baseurl=ftp://192.168.4.254/centos-1804
enabled=1
gpgcheck=0

[root@node1 ~]# vim /etc/yum.repos.d/ceph.repo
[osd]
name=ceph osd
baseurl=ftp://192.168.4.254/ceph/OSD
enabled=1
gpgcheck=0

[mon]
name=ceph mon
baseurl=ftp://192.168.4.254/ceph/MON
enabled=1
gpgcheck=0

[tools]
name=ceph tools
baseurl=ftp://192.168.4.254/ceph/Tools
enabled=1
gpgcheck=0

[root@node1 ~]# yum repolist
... ...
repolist: 10,013

[root@node1 ~]# scp /etc/yum.repos.d/ceph.repo 192.168.4.12:/etc/yum.repos.d/
[root@node1 ~]# ^12^13
[root@node1 ~]# ^13^10
```

- 各节点务必关闭selinux和防火墙

- 集群安装前的准备工作

```shell
# ceph为我们提供了一个ceph-deploy工具，可以在某一节点上统一操作全部节点
# 将Node1作为部署节点，将来的操作都在node1上进行。这样，需要node1能够免密操作其他主机
[root@node1 ~]# ssh-keygen    # 生成密钥对
[root@node1 ~]# for i in {10..13}
> do
> ssh-copy-id 192.168.4.$i
> done


# 在所有的主机上配置名称解析。注意，解析的名字必须是该机器的主机名
[root@node1 ~]# vim /etc/hosts   # 增加4行
... ...
192.168.4.10    client1
192.168.4.11    node1
192.168.4.12    node2
192.168.4.13    node3
[root@node1 ~]# for i in 10 12 13
> do
> scp /etc/hosts 192.168.4.$i:/etc/
> done
```

- 安装集群

```shell
# 在3个节点上安装软件包
[root@node1 ~]# for i in node{1..3}
> do
> ssh $i yum install -y ceph-mon ceph-osd ceph-mds ceph-radosgw
> done


# 配置client1为ntp服务器
[root@client1 ~]# yum install -y chrony
[root@client1 ~]# vim /etc/chrony.conf 
 29 allow 192.168.4.0/24    # 授权192.168.4.0/24可以时钟同步
 33 local stratum 10   # 即使没有从一个源同步时钟，也为其他主机提供时间
[root@client1 ~]# systemctl restart chronyd

# 配置node1-3成为client1的NTP客户端
[root@node1 ~]# for i in node{1..3}
> do
> ssh $i yum install -y chrony
> done
[root@node1 ~]# vim /etc/chrony.conf  # 只改第7行
  7 server 192.168.4.10 iburst   # 替换gateway
[root@node1 ~]# for i in node{2..3}
> do
> scp /etc/chrony.conf $i:/etc/
> done
[root@node1 ~]# for i in node{1..3}
> do
> ssh $i systemctl restart chronyd
> done

# 验证时间是否同步  client1前面有^*表示同步成功
[root@node1 ~]# chronyc sources -v
... ...
^* client1                      10   6    17    40  -4385ns[-1241us] +/-  162us



# 在node1上安装ceph-deploy部署工具
[root@node1 ~]# yum install -y ceph-deploy
# 查看使用帮助
[root@node1 ~]# ceph-deploy --help
[root@node1 ~]# ceph-deploy mon --help   # 查看mon子命令的帮助

# 创建ceph-deploy工作目录
[root@node1 ~]# mkdir ceph-cluster
[root@node1 ~]# cd ceph-cluster

# 创建一个新的集群。
[root@node1 ceph-cluster]# ceph-deploy new node{1..3}
[root@node1 ceph-cluster]# ls
ceph.conf  ceph-deploy-ceph.log  ceph.mon.keyring
[root@node1 ceph-cluster]# tree .
.
├── ceph.conf               # 集群配置文件
├── ceph-deploy-ceph.log    # 日志文件
└── ceph.mon.keyring        # 共享密钥

# 开启分层快照功能。
[root@node1 ceph-cluster]# vim ceph.conf   # 尾部追加一行如下
rbd_default_features = 1

# 初始化monitor
[root@node1 ceph-cluster]# ceph-deploy mon create-initial

# 如果安装过程中出现keyring...这种报错，可以试着执行以下命令：
[root@node1 ceph-cluster]# ceph-deploy gatherkeys node{1..3}

[root@node1 ceph-cluster]# systemctl status ceph-mon*
● ceph-mon@node1.service .. ..
[root@node2 ~]# systemctl status ceph*
● ceph-mon@node2.service ... ...
[root@node3 ~]# systemctl status ceph*
● ceph-mon@node3.service ... ...
# 注意：这些服务在30分钟之内只能启动3次，超过报错。

# 查看集群状态
[root@node1 ceph-cluster]# ceph -s
     health HEALTH_ERR   # 因为还没有硬盘，所以状态是HEALTH_ERR


# 创建OSD
[root@node1 ceph-cluster]# ceph-deploy disk --help
# 初始化各主机的硬盘。vmware应该是sdb和sdc
[root@node1 ceph-cluster]# ceph-deploy disk zap node1:vdb node1:vdc 
[root@node1 ceph-cluster]# ceph-deploy disk zap node2:vdb node2:vdc 
[root@node1 ceph-cluster]# ceph-deploy disk zap node3:vdb node3:vdc 

# 创建存储空间。ceph会硬盘分为两个分区，一个分区大小为5GB，用于保存ceph的内部资源；另一个分区是剩余全部空间
[root@node1 ceph-cluster]# ceph-deploy osd --help
[root@node1 ceph-cluster]# ceph-deploy osd create node1:vd{b,c}
[root@node1 ceph-cluster]# lsblk 
NAME   MAJ:MIN RM SIZE RO TYPE MOUNTPOINT
vda    253:0    0  30G  0 disk 
└─vda1 253:1    0  30G  0 part /
vdb    253:16   0  20G  0 disk 
├─vdb1 253:17   0  15G  0 part /var/lib/ceph/osd/ceph-0
└─vdb2 253:18   0   5G  0 part 
vdc    253:32   0  20G  0 disk 
├─vdc1 253:33   0  15G  0 part /var/lib/ceph/osd/ceph-1
└─vdc2 253:34   0   5G  0 part 
# 将会出现2个osd进程，因为有两块硬盘用于ceph
[root@node1 ceph-cluster]# systemctl status ceph-osd*

# 继续初始化其他节点的OSD
[root@node1 ceph-cluster]# ceph-deploy osd create node2:vd{b,c}
[root@node1 ceph-cluster]# ceph-deploy osd create node3:vd{b,c}

# 查看集群状态
[root@node1 ceph-cluster]# ceph -s
     health HEALTH_OK     # 状态是HEALTH_OK表示一切正常

```

### 实现块存储

- 块设备存取数据时，可以一次存取很多。字符设备只能是字符流

```shell
[root@node1 ceph-cluster]# ll /dev/vda
brw-rw---- 1 root disk 253, 0 11月  4 10:15 /dev/vda
# b表示block，块设备
[root@node1 ceph-cluster]# ll /dev/tty
crw-rw-rw- 1 root tty 5, 0 11月  4 10:54 /dev/tty
# c表示character，字符设备
```

- 块存储，就是可以提供像硬盘一样的设备。使用块存储的节点，第一次连接块设备，需要对块设备进行分区、格式化，然后挂载使用。
- ceph提供存储时，需要使用存储池。为了给客户端提供存储资源，需要创建名为存储池的容器。存储池类似于逻辑卷管理中的卷组。卷组中包含很多硬盘和分区；存储池中包含各节点上的硬盘。

```shell
# ceph默认有一个名为rbd的存储池，其编号为0
[root@node1 ceph-cluster]# ceph osd lspools 
0 rbd,

# 查看存储池大小
[root@node1 ceph-cluster]# ceph df
GLOBAL:
    SIZE       AVAIL      RAW USED     %RAW USED 
    92093M     91889M         203M          0.22 
POOLS:
    NAME     ID     USED     %USED     MAX AVAIL     OBJECTS 
    rbd      0        16         0        30629M           0 

# 查看存储池rbd存储数据时，保存的副本数量
[root@node1 ceph-cluster]# ceph osd pool get rbd size
size: 3

# 在默认存储池中，创建一个名为demo-image大小为10G的镜像，提供给客户端使用
# 镜像相当于逻辑卷管理中的lv
[root@node1 ceph-cluster]# rbd create demo-image --size 10G
# 查看默认存储池中的镜像
[root@node1 ceph-cluster]# rbd list
demo-image
# 查看demo-image的详细信息
[root@node1 ceph-cluster]# rbd info demo-image
rbd image 'demo-image':
	size 10240 MB in 2560 objects
	order 22 (4096 kB objects)
	block_name_prefix: rbd_data.1035238e1f29
	format: 2
	features: layering
	flags: 

# 扩容
[root@node1 ceph-cluster]# rbd resize --size 15G demo-image
Resizing image: 100% complete...done.
[root@node1 ceph-cluster]# rbd info demo-image
rbd image 'demo-image':
	size 15360 MB in 3840 objects

# 缩减
[root@node1 ceph-cluster]# rbd resize --size 7G demo-image --allow-shrink
[root@node1 ceph-cluster]# rbd info demo-image
rbd image 'demo-image':
	size 7168 MB in 1792 objects
```

#### 客户端使用块设备

- 怎么用？装软件
- ceph集群在哪？通过配置文件说明集群地址
- 权限。keyring文件

```shell
# 安装ceph客户端软件
[root@client1 ~]# yum install -y ceph-common

# 将配置文件和密钥keyring文件拷贝给客户端
[root@node1 ceph-cluster]# scp /etc/ceph/ceph.conf 192.168.4.10:/etc/ceph/
[root@node1 ceph-cluster]# scp /etc/ceph/ceph.client.admin.keyring 192.168.4.10:/etc/ceph/

# 客户端查看镜像
[root@client1 ~]# rbd list
demo-image

# 将ceph提供的镜像映射到本地
[root@client1 ~]# rbd map demo-image
/dev/rbd0
[root@client1 ~]# lsblk 
NAME   MAJ:MIN RM SIZE RO TYPE MOUNTPOINT
vda    253:0    0  30G  0 disk 
└─vda1 253:1    0  30G  0 part /
rbd0   252:0    0   7G  0 disk    # 多了一块7GB的硬盘
[root@client1 ~]# ls /dev/rbd0 
/dev/rbd0

# 查看映射
[root@client1 ~]# rbd showmapped
id pool image      snap device    
0  rbd  demo-image -    /dev/rbd0 

# 使用
[root@client1 ~]# mkfs.xfs /dev/rbd0
[root@client1 ~]# mount /dev/rbd0 /mnt/
[root@client1 ~]# df -h /mnt/
文件系统        容量  已用  可用 已用% 挂载点
/dev/rbd0       7.0G   33M  7.0G    1% /mnt
```

