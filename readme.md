- 本笔记采用Typora编写，Typora下载地址：https://typora.io/#download

- 笔记下载方式：

课上笔记如果本地没有任何资料，第一次下载：

```
git clone https://gitee.com/mrzhangzhg/tedu_nsd.git
```
后续同步：
```
# cd tedu_nsd
# git pull
```

- 云计算学院2021年课程全阶段软件：
  链接：https://pan.baidu.com/s/1H9BCY_dgnmgVKgpQceDKDQ 
  提取码：Va2M
- 云计算脱产学员（VMware版本虚拟机实验环境）:
  链接：https://pan.baidu.com/s/1HtAb-73_OzwqwpNMOFMsMw 
  提取码：5wyg 

- 我的Python专辑：《Python百例》
  - https://www.jianshu.com/c/00c61372c46a
  - https://cloud.tencent.com/developer/column/5283

市面上“从入门到精通”的书很多，但是很时候反而成了“从入门到放弃”。我并不打算以“从入门到精通”的名号，把大家搞到放弃。所以写了一本“从入门到菜鸟”的书。

本书注重基础和编程思路的讲解，通过一系列示例帮助读者顺利入门。

书名为《例解Python》，已由电子工业出版社出版，在京东等各大电商平台有售。[京东链接](https://item.jd.com/13054450.html)
![](imgs/py.png)