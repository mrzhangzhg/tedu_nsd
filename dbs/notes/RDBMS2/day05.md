# RDBMS2 day05

<p align="right">
  By <a href="https://www.jianshu.com/u/665c84e77f9c">Mr.张志刚</a>
</p>

[toc]

## PXC（Percona XtraDB Cluster）

### 概述

#### PXC介绍

- 是基于Galera的MySQL高可用集群解决方案
- Galera Cluster是Codership公司开发的一套免费开源的高可用方案
- PXC集群主要由两部分组成：Percona Server with XtraDB和Write Set Replication patches（同步、多主复制插件）
- 官网http://galeracluster.com

#### PXC特点

- 数据强一致性、无同步延迟
- 没有主从切换操作，无需使用虚拟IP
- 支持InnoDB存储引擎
- 多线程复制
- 部署使用简单
- 支持节点自动加入，无需手动拷贝数据

#### 相应端口

- 3306：数据库服务端口
- 4444：SST端口
- 4567：集群通信端口
- 4568：IST端口
- SST：State Snapshot Transfer 全量同步
- IST：Incremental State Transfer 增量同步

## 部署PXC

### 服务器角色

- 三台相互独立的mysql服务器：192.168.4.20、192.168.4.30、192.168.4.40

### 初始环境准备

- 配置服务器的名称解析

```mysql
[root@node20 ~]# for i in 20 30 40
> do
> echo -e "192.168.4.$i\tnode$i" >> /etc/hosts
> done

[root@node30 ~]# for i in 20 30 40
> do
> echo -e "192.168.4.$i\tnode$i" >> /etc/hosts
> done

[root@node40 ~]# for i in 20 30 40
> do
> echo -e "192.168.4.$i\tnode$i" >> /etc/hosts
> done
```

- 准备yum源

```mysql
[root@room8pc16 mysql_soft]# cd pxc
[root@room8pc16 pxc]# tar xf Percona-XtraDB-Cluster-5.7.25-31.35-r463-el7-x86_64-bundle.tar
[root@room8pc16 pxc]# cp *.rpm /var/ftp/mysql5.7/
[root@room8pc16 pxc]# cd /var/ftp/mysql5.7/
[root@room8pc16 mysql5.7]# createrepo -v .
```

- 安装软件包

```mysql
[root@node20 ~]# yum clean all
[root@node20 ~]# yum remove -y mysql-community-*
[root@node20 ~]# yum install -y qpress-1.1-14.11 Percona-XtraDB-Cluster-*

[root@node30 ~]# yum clean all
[root@node30 ~]# yum remove -y mysql-community-*
[root@node30 ~]# yum install -y qpress-1.1-14.11 Percona-XtraDB-Cluster-*

[root@node40 ~]# yum clean all
[root@node40 ~]# yum remove -y mysql-community-*
[root@node40 ~]# yum install -y qpress-1.1-14.11 Percona-XtraDB-Cluster-*
```

### 配置服务

- 分别修改3台服务器的mysqld.cnf文件

```mysql
[root@node20 ~]# vim /etc/percona-xtradb-cluster.conf.d/mysqld.cnf
... ...
[mysqld]
server-id=20
datadir=/var/lib/mysql
socket=/var/lib/mysql/mysql.sock
log-error=/var/log/mysqld.log
pid-file=/var/run/mysqld/mysqld.pid
log-bin
log_slave_updates
expire_logs_days=7
... ...

[root@node30 ~]# vim /etc/percona-xtradb-cluster.conf.d/mysqld.cnf
... ...
[mysqld]
server-id=30
datadir=/var/lib/mysql
socket=/var/lib/mysql/mysql.sock
log-error=/var/log/mysqld.log
pid-file=/var/run/mysqld/mysqld.pid
log-bin
log_slave_updates
expire_logs_days=7
... ...

[root@node40 ~]# vim /etc/percona-xtradb-cluster.conf.d/mysqld.cnf
... ...
[mysqld]
server-id=40
datadir=/var/lib/mysql
socket=/var/lib/mysql/mysql.sock
log-error=/var/log/mysqld.log
pid-file=/var/run/mysqld/mysqld.pid
log-bin
log_slave_updates
expire_logs_days=7
... ...
```

- 分别修改3台服务器的mysqld_safe.cnf （使用默认配置即可）

```mysql
[root@node20 ~]# vim /etc/percona-xtradb-cluster.conf.d/mysqld_safe.cnf
[mysqld_safe]
pid-file = /var/run/mysqld/mysqld.pid
socket   = /var/lib/mysql/mysql.sock
nice     = 0

[root@node30 ~]# vim /etc/percona-xtradb-cluster.conf.d/mysqld_safe.cnf
[mysqld_safe]
pid-file = /var/run/mysqld/mysqld.pid
socket   = /var/lib/mysql/mysql.sock
nice     = 0

[root@node40 ~]# vim /etc/percona-xtradb-cluster.conf.d/mysqld_safe.cnf
[mysqld_safe]
pid-file = /var/run/mysqld/mysqld.pid
socket   = /var/lib/mysql/mysql.sock
nice     = 0
```

- 分别修改3台服务器的wsrep.cnf

```mysql
[root@node20 ~]# vim /etc/percona-xtradb-cluster.conf.d/wsrep.cnf
# 修改以下内容：
wsrep_cluster_address=gcomm://192.168.4.20,192.168.4.30,192.168.4.40  # 集群成员
wsrep_node_address=192.168.4.20   # 本节点IP地址
wsrep_cluster_name=pxc-cluster    # 集群名
wsrep_node_name=node20            # 本节点名
wsrep_sst_auth="sstuser:NSD2021@tedu.cn"   # SST数据同步授权用户及密码

[root@node30 ~]# vim /etc/percona-xtradb-cluster.conf.d/wsrep.cnf
# 修改以下内容：
wsrep_cluster_address=gcomm://192.168.4.20,192.168.4.30,192.168.4.40  # 集群成员
wsrep_node_address=192.168.4.30   # 本节点IP地址
wsrep_cluster_name=pxc-cluster    # 集群名
wsrep_node_name=node30            # 本节点名
wsrep_sst_auth="sstuser:NSD2021@tedu.cn"   # SST数据同步授权用户及密码

[root@node40 ~]# vim /etc/percona-xtradb-cluster.conf.d/wsrep.cnf
# 修改以下内容：
wsrep_cluster_address=gcomm://192.168.4.20,192.168.4.30,192.168.4.40  # 集群成员
wsrep_node_address=192.168.4.40   # 本节点IP地址
wsrep_cluster_name=pxc-cluster    # 集群名
wsrep_node_name=node40            # 本节点名
wsrep_sst_auth="sstuser:NSD2021@tedu.cn"   # SST数据同步授权用户及密码
```

### 启动服务

- 启动集群服务

```mysql
# 首次启动服务时间比较长
[root@node20 ~]# systemctl  start mysql@bootstrap.service
[root@node20 ~]# grep password /var/log/mysqld.log
[root@node20 ~]# mysql -uroot -p'!Yo.e(qv:0pH'
mysql> alter user  root@'localhost' identified by 'NSD2021@tedu.cn';
mysql> grant reload, lock tables,replication client,process on *.*  to
    -> sstuser@'localhost' identified by 'NSD2021@tedu.cn';
Query OK, 0 rows affected, 1 warning (0.20 sec)
```

- 启动其他节点，其他节点将会自动同步第一台服务器的数据（授权用户等）

```mysql
[root@node30 ~]# systemctl start mysql
[root@node30 ~]# netstat -utnlp  | grep :3306
tcp6       0      0 :::3306                 :::*                    LISTEN      18875/mysqld
[root@node30 ~]# netstat -utnlp  | grep :4567
tcp        0      0 0.0.0.0:4567            0.0.0.0:*               LISTEN      18875/mysqld

[root@node40 ~]# systemctl  start mysql
[root@node40 ~]# netstat -utnlp  | grep :3306
tcp6       0      0 :::3306                 :::*                    LISTEN      18465/mysqld
[root@node40 ~]# netstat -utnlp  | grep :4567
tcp        0      0 0.0.0.0:4567            0.0.0.0:*               LISTEN      18465/mysqld
```

### 测试配置

#### 查看集群信息

- 在任意数据库服务器查看信息

```mysql
[root@node30 ~]# mysql -uroot -pNSD2021@tedu.cn
mysql> show status like "%wsrep%";  # 找到以下信息
| wsrep_incoming_addresses         | 192.168.4.20:3306,192.168.4.30:3306,192.168.4.40:3306 |
| wsrep_cluster_weight             | 3                                                     |
| wsrep_cluster_status             | Primary                                               |
| wsrep_connected                  | ON                                                    |
| wsrep_ready                      | ON                                                    |
```

- 访问集群，在任意数据库服务器存取数据

```mysql
mysql> grant all on db1.* to dbuser1@'%' identified by 'NSD2021@tedu.cn';
Query OK, 0 rows affected, 1 warning (0.48 sec)

mysql> show grants for dbuser1;
+--------------------------------------------------+
| Grants for dbuser1@%                             |
+--------------------------------------------------+
| GRANT USAGE ON *.* TO 'dbuser1'@'%'              |
| GRANT ALL PRIVILEGES ON `db1`.* TO 'dbuser1'@'%' |
+--------------------------------------------------+
2 rows in set (0.00 sec)
```

- 客户端连接集群任意数据库服务器存取数据

```mysql
[root@node10 ~]# mysql -h192.168.4.20 -udbuser1 -pNSD2021@tedu.cn
mysql> create database db1 default charset utf8mb4;
Query OK, 1 row affected (0.57 sec)

mysql> create table db1.students(id int primary key auto_increment, name varchar(20));
Query OK, 0 rows affected (0.92 sec)

mysql> insert into db1.students(name) values ('tom');
Query OK, 1 row affected (0.15 sec)
```

- 客户端连接各数据库服务器查看数据

```mysql
[root@node10 ~]# mysql -h192.168.4.30 -udbuser1 -pNSD2021@tedu.cn
mysql> select * from db1.students;
+----+------+
| id | name |
+----+------+
|  1 | tom  |
+----+------+
1 row in set (0.00 sec)

[root@node10 ~]# mysql -h192.168.4.40 -udbuser1 -pNSD2021@tedu.cn
mysql> select * from db1.students;
+----+------+
| id | name |
+----+------+
|  1 | tom  |
+----+------+
1 row in set (0.00 sec)
```

#### 测试故障自动恢复

- 停止3台服务器的任意一台主机的数据库服务都不会影响数据的存取。

```mysql
[root@node20 ~]# systemctl stop mysql@bootstrap.service
[root@node30 ~]# mysql -uroot -pNSD2021@tedu.cn
mysql> insert into db1.students(name) values('jerry');
Query OK, 1 row affected (0.20 sec)

mysql> select * from db1.students;
+----+-------+
| id | name  |
+----+-------+
|  1 | tom   |
|  5 | jerry |
+----+-------+
2 rows in set (0.00 sec)
```

- 启动停止的mysql服务器，数据将会自动同步

```mysql
[root@node20 ~]# mysql -uroot -pNSD2021@tedu.cn
mysql> select * from db1.students;
+----+-------+
| id | name  |
+----+-------+
|  1 | tom   |
|  5 | jerry |
+----+-------+
2 rows in set (0.00 sec)
```

## 存储引擎

### 概述

- 数据库存储引擎是数据库底层软件组织，数据库管理系统（DBMS）使用数据引擎进行创建、查询、更新和删除数据。

- 不同的存储引擎提供不同的存储机制、索引技巧、锁定水平等功能，使用不同的存储引擎，还可以获得特定的功能。
- 现在许多不同的数据库管理系统都支持多种不同的数据引擎。
- MySQL支持很多存储引擎，包括MyISAM、InnoDB、BDB、MEMORY、MERGE、EXAMPLE、NDB Cluster、ARCHIVE等，其中InnoDB和BDB支持事务安全。
- 查看数据库使用的、支持的引擎

```mysql
mysql> show engines;  # DEFAULT表示默认使用的引擎，
```

- 查看当前表使用的存储引擎

```mysql
mysql> show create table nsd2021.departments\G
*************************** 1. row ***************************
       Table: departments
Create Table: CREATE TABLE `departments` (
  `dept_id` int(4) NOT NULL AUTO_INCREMENT,
  `dept_name` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`dept_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4
1 row in set (0.00 sec)

# 或

mysql> use nsd2021;
mysql> show table status\G
*************************** 1. row ***************************
           Name: departments
         Engine: InnoDB
        Version: 10
     Row_format: Dynamic
           Rows: 8
 Avg_row_length: 2048
    Data_length: 16384
Max_data_length: 0
   Index_length: 0
      Data_free: 0
 Auto_increment: 9
    Create_time: 2021-04-19 20:41:46
    Update_time: 2021-04-19 20:41:46
     Check_time: NULL
      Collation: utf8mb4_general_ci
       Checksum: NULL
 Create_options:
        Comment:
*************************** 2. row ***************************
           Name: employees
         Engine: InnoDB
        Version: 10
     Row_format: Dynamic
           Rows: 133
 Avg_row_length: 123
    Data_length: 16384
Max_data_length: 0
   Index_length: 16384
      Data_free: 0
 Auto_increment: 134
    Create_time: 2021-04-19 20:41:46
    Update_time: 2021-04-19 20:41:47
     Check_time: NULL
      Collation: utf8mb4_general_ci
       Checksum: NULL
 Create_options:
        Comment:
*************************** 3. row ***************************
           Name: salary
         Engine: InnoDB
        Version: 10
     Row_format: Dynamic
           Rows: 8066
 Avg_row_length: 44
    Data_length: 360448
Max_data_length: 0
   Index_length: 163840
      Data_free: 0
 Auto_increment: 9577
    Create_time: 2021-04-19 20:41:47
    Update_time: 2021-04-19 20:41:49
     Check_time: NULL
      Collation: utf8mb4_general_ci
       Checksum: NULL
 Create_options:
        Comment:
3 rows in set (0.01 sec)
```

- 创建表时指定使用的存储引擎

```mysql
mysql> create table nsd2021.students(id int primary key auto_increment, name varchar(20)) engine=myisam;
Query OK, 0 rows affected (0.03 sec)

mysql> show create table nsd2021.students \G                                    *************************** 1. row ***************************
       Table: students
Create Table: CREATE TABLE `students` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4
1 row in set (0.00 sec)
```

### 读锁和写锁

- 无论何时，只要有多个SQL需要同一时刻修改数据，都会产生并发控制的问题。
- 解决这类经典问题的方法就是并发控制，即在处理并发读或者写时，可以通过实现一个由两种类型的锁组成的锁系统来解决问题。
- 这两种锁就是共享锁和排他锁，也叫读锁和写锁。
- **读锁**是**共享**的，即相互不阻塞的，多个客户在同一时刻可以读取同一资源，互不干扰。
- 写锁是排他的，即一个写锁会阻塞其它的写锁和读锁，只有这样，才能确保给定时间内，只有一个用户能执行写入，防止其它用户读取正在写入的同一资源。
- 写锁优先级高于读锁。

### 行锁和表锁

- 实际数据库系统中每时每刻都在发生锁定，锁也是有粒度的，提高共享资源并发行的方式就是让锁更有选择性，尽量只锁定需要修改的部分数据，而不是所有的资源，因此要进行精确的锁定。
- 由于加锁也需要消耗资源，包括获得锁、检查锁是否解除、释放锁等，都会增加系统的开销
- 所谓的锁策略就是要在锁的开销和数据的安全性之间寻求平衡，这种平衡也会影响性能。
- 每种MySQL存储引擎都有自己的锁策略和锁粒度，最常用的两种重要的锁策略分别是表锁和行锁。
  - 表锁是开销最小的策略，会锁定整张表，用户对表做写操作时，要先获得写锁，这会阻塞其它用户对该表的所有读写操作。没有写锁时，其它读取的用户才能获得读锁，读锁之间是不相互阻塞的。
  - 行锁可以最大成都支持并发处理，但也带来了最大的锁开销，它只对指定的记录加锁，其它进程还是可以对同一表中的其它记录进行操作。
  - 表级锁速度快，但冲突多，行级锁冲突少，但速度慢。

### MyISAM

- 它是MySQL5.5之前的默认存储引擎
- 优势：访问速度快
- 适用场景：对事务的完整性没有要求，或以select、insert为主的应用基本都可以选用MYISAM。在Web、数据仓库中应用广泛。
- 特点：
  - 不支持事务、外键
  - 每个myisam在磁盘上存储为3个文件，文件名和表名相同，扩展名分别是
    - .frm：存储表定义
    - .myd：MYData，存储数据
    - .myi：MYIndex，存储索引

### InnoDB

- MySQL5.5之后的默认存储引擎
- 应用场景：如果应用对事务的完整性有较高的要求，在并发条件下要求数据的一致性，数据操作中包含读、插入、删除、更新，那InnoDB是最好的选择。在计费系统、财务系统等对数据的准确性要求较高的系统中被广泛应用。
- 优点：提供了具有提交（Commit）、回滚（Rollback）、崩溃恢复能力的事务安全，支持外键。
- 缺点：相比较于MyISAM，写的处理效率差一点，并且会占用更多的磁盘空间来存储数据和索引。
- 特点：
  - 自动增长列：innoDB表的自动增长列必须是索引，如果是组合索引，也必须是组合索引的第一列。MyISAM表的自动增长列可以是组合索引的其他列
  - 外键约束：MySQL的存储引擎中只有innoDB支持外键约束。注意：当某个表被其它表创建了外键参照，那么该表对应的索引和主键禁止被删除。

### MEMORY

- MEMORY存储引擎是用保存在内存中的数据来创建表，每个memory表对应一个磁盘文件。格式是.frm。
- 适用场景：内容变化不频繁的代码表，作为统计操作的中间结果表，便于利用它速率快的优势高效的对中间结果进分析。
- 特点：由于它的数据是存放在内存中的，并且默认使用HASH索引，所以它的访问速度特别快，同时也造成了他的缺点，就是数据库服务一旦关闭，数据就会丢失，另外对表的大小有限制。



