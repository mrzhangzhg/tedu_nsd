#!/bin/bash


# 配置pip使用国内镜像
[ ! -d ~/.pip ] && mkdir ~/.pip
cat <<EOF > ~/.pip/pip.conf
[global]
index-url = http://mirrors.aliyun.com/pypi/simple/
[install]
trusted-host=mirrors.aliyun.com
EOF

# 安装所需的python包
pip3 install faker pypinyin pymysql

# 建立数据库
mysql -uroot -p'NSD2021@tedu.cn' < tedu_db.sql
# 生成数据
python3 gen_data.py

