DROP DATABASE IF EXISTS tedu_db;

CREATE DATABASE /*IF NOT EXISTS*/  tedu_db DEFAULT CHARSET UTF8MB4;

USE `tedu_db`

DROP TABLE IF EXISTS `departments`;

CREATE TABLE `departments` (
    `dept_id` INT(4) NOT NULL AUTO_INCREMENT,
    `dept_name` VARCHAR(10) DEFAULT NULL,
    PRIMARY KEY (`dept_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `departments` VALUES (1, '人事部'), (2, '财务部'), (3, '运维部'), (4, '开发部'),
(5, '测试部'), (6, '市场部'), (7, '销售部'), (8, '法务部');

DROP TABLE IF EXISTS `employees`;
CREATE TABLE `employees` (
    `employee_id` INT(6) NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(10) DEFAULT NULL,
    `hire_date` DATE DEFAULT NULL,
    `birth_date` DATE DEFAULT NULL,
    `email` VARCHAR(25) DEFAULT NULL,
    `phone_number` CHAR(11) DEFAULT NULL,
    `dept_id` INT(4) DEFAULT NULL,
    PRIMARY KEY (`employee_id`),
    KEY `dept_id_fk` (`dept_id`),
    CONSTRAINT `dept_id_fk` FOREIGN KEY (`dept_id`) REFERENCES `departments` (`dept_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `salary` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `date` DATE DEFAULT NULL,
    `employee_id` INT(6) DEFAULT NULL,
    `basic` int(6) DEFAULT NULL,
    `bonus` int(6) DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `emp_id_fk` (`employee_id`),
    CONSTRAINT `emp_id_fk` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`employee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
