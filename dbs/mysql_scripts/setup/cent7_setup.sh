#!/bin/bash
# 时间：2021年2月28日
# 作者：张志刚

# 安装python3和mariadb，并启动mariadb服务、修改密码为tedu.cn
wget -O /etc/yum.repos.d/CentOS-Base.repo https://mirrors.aliyun.com/repo/Centos-7.repo
yum -y install python3 python3-devel mariadb-server
systemctl start mariadb
systemctl enable mariadb
mysqladmin password tedu.cn

# 配置pip使用国内镜像
[ ! -d ~/.pip ] && mkdir ~/.pip
cat <<EOF > ~/.pip/pip.conf
[global]
index-url = http://mirrors.aliyun.com/pypi/simple/
[install]
trusted-host=mirrors.aliyun.com
EOF

# 安装所需的python包
pip3 install faker pypinyin pymysql

# 建立数据库
mysql -uroot -ptedu.cn < tedu_db.sql
# 生成数据
python3 gen_data.py
