# 数据库设计

## 表设计

departments表

| 字段            | 类型        | 说明   |
| --------------- | ----------- | ------ |
| dept_id         | int(4)      | 部门号 |
| department_name | varchar(20) | 部门名 |
|                 |             |        |

employees表

| 字段         | 类型        | 约束         |
| :----------- | ----------- | ------------ |
| employee_id  | int         | 员工号       |
| name         | varchar()   | 姓名         |
| birth_date   | date        | 生日         |
| hire_date    | date        | 入职日期     |
| phone_number | char(11)    | 电话号码     |
| email        | varchar(30) | email地址    |
| dept_id      | int         | 所在部门编号 |
|              |             |              |

salary表

| 字段        | 类型 | 说明       |
| ----------- | ---- | ---------- |
| id          | int  | 行号       |
| date        | date | 发工资日期 |
| employee_id | int  | 员工编号   |
| basic       | int  | 基本工资   |
| bonus       | int  | 奖金       |

## 脚本运行

- centos7系统
  - 在保证联网的情况下，运行cent7_setup.sh。
  - 将安装python3、mariadb-server、启动mariadb服务，配置mariadb密码为tedu.cn

- centos8系统
  - 在保证联网和yum可用的情况下，运行cent8_setup.sh。
  - 将安装mariadb-server、启动mariadb服务，配置mariadb密码为tedu.cn
- 脚本将会创建名为tedu_db的数据库，并创建上述3张表
  - departments表将会插入8个固定的部门
  - employees表将会随机生成133个用户的信息
  - salary表将会生成20年的工资数据。起始基本工资从5000到20000不等，每年涨5%；奖金为1000到10000的随机值。